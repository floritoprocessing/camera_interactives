static int ADDING = 0;

/*
* Slowly adapting image
*/
class AdaptingImage extends PImage {
   int[] r, g, b;
   
   int adaptStyle = ADDING;
   int adaptAdd = 50;
  
  AdaptingImage(int width, int height) {
    super(width,height);
    r = new int[width*height];
    g = new int[r.length];
    b = new int[r.length];
  }
  
  AdaptingImage(int width, int height, int[] pixels) {
    this(width,height);
    if (pixels.length!=r.length) {
      throw new RuntimeException("Supplied array not the right size!");
    }
    set(pixels);
    updatePixels();
  }
  
  public void setAdaption(int style, float arg0) {
    if (style==ADDING) {
      adaptAdd = (int)arg0;
    } else {
      throw new RuntimeException("Unknown adaption style");
    }
  }
  
  public void set(int[] pixels) {
    for (int i=0;i<pixels.length;i++) {
      r[i] = (pixels[i]>>8&0xff00);
      g[i] = (pixels[i]&0xff00);
      b[i] = (pixels[i]&0xff)<<8;
    }
    System.arraycopy(pixels,0,this.pixels,0,pixels.length);
    this.updatePixels();
  }
  
  
  
  public void adaptFrom(int[] pixels) {
    if (adaptStyle==ADDING) {
      int val=0, rr=0, gg=0, bb=0;
      for (int i=0;i<pixels.length;i++) {
        val = (pixels[i]>>8&0xff00);
        if (val<r[i]) {
          r[i]-=adaptAdd;
        } else if (val>r[i]) {
          r[i]+=adaptAdd;
        }
        
        val = (pixels[i]&0xff00);
        if (val<g[i]) {
          g[i]-=adaptAdd;
        } else if (val>g[i]) {
          g[i]+=adaptAdd;
        }
        
        val = (pixels[i]&0xff)<<8;
        if (val<b[i]) {
          b[i]-=adaptAdd;
        } else if (val>b[i]) {
          b[i]+=adaptAdd;
        }
        
        rr = r[i]<<8&0xff0000;
        gg = g[i]&0xff00;
        bb = b[i]>>8&0xff;
          
        this.pixels[i] = 0xff000000|rr|gg|bb;
      }
    }
    this.updatePixels();
  }
  
  public void adaptFrom(int col) {
    if (adaptStyle==ADDING) {
      int val=0, rr=0, gg=0, bb=0;
      for (int i=0;i<this.pixels.length;i++) {
        val = (col>>8&0xff00);
        if (val<r[i]) {
          r[i]-=adaptAdd;
        } else if (val>r[i]) {
          r[i]+=adaptAdd;
        }
        
        val = (col&0xff00);
        if (val<g[i]) {
          g[i]-=adaptAdd;
        } else if (val>g[i]) {
          g[i]+=adaptAdd;
        }
        
        val = (col&0xff)<<8;
        if (val<b[i]) {
          b[i]-=adaptAdd;
        } else if (val>b[i]) {
          b[i]+=adaptAdd;
        }
        
        rr = r[i]<<8&0xff0000;
        gg = g[i]&0xff00;
        bb = b[i]>>8&0xff;
          
        this.pixels[i] = 0xff000000|rr|gg|bb;
      }
    }
    this.updatePixels();
  }
  
  
}
