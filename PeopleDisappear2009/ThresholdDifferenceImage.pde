class ThresholdDifferenceImage extends PImage {
  
  int threshold;
  
  ThresholdDifferenceImage(int width, int height, int threshold) {
    super(width,height);
    setThreshold(threshold);
  }
  
  void setThreshold(int threshold) {
    this.threshold = threshold;
  }
  
  int getThreshold() {
    return threshold;
  }
  
  
  public void create(int[] in0, int[] in1) {
    if (in0.length!=in1.length) {
      throw new RuntimeException("in0 and in1 need to be same length");
    }
    if (in0.length!=pixels.length) {
      throw new RuntimeException("in and pixels of this image need to be same length");
    }
    
    int diff;
    for (int i=0;i<pixels.length;i++) {
      diff = Math.abs( (in0[i]>>16&0xff) - (in1[i]>>16&0xff) );
      diff += Math.abs( (in0[i]>>8&0xff) - (in1[i]>>8&0xff) );
      diff += Math.abs( (in0[i]&0xff) - (in1[i]&0xff) );
      if (diff<threshold) {
        pixels[i]=0xffffffff;
      } else {
        pixels[i]=0xff000000;
      }
    }
    
  }
  
}
