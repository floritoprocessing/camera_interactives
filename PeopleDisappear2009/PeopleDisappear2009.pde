import processing.video.*;

int WEBCAM_WIDTH = 320;
int WEBCAM_HEIGHT = 240;
int WEBCAM_FPS = 30;

float WEBCAM_ADAPTION = 1;
int DIFFERENCE_THRESHOLD = 90;

Capture webcam;
boolean setSlomo = false;

AdaptingImage slomo1 = null;
AdaptingImage slomo2 = null;

ThresholdDifferenceImage thresholdDifference;

PImage combine;

void setup() {
  //size(WEBCAM_WIDTH*3,WEBCAM_HEIGHT*2,P2D);
  size(960,480,P2D);
  
  /*
  *  Init Webcam
  */
  
  webcam = new Capture(this,WEBCAM_WIDTH,WEBCAM_HEIGHT,WEBCAM_FPS);
  webcam.start();
  
  /*
  *  Init slomo webcam adapter
  */
  slomo1 = new AdaptingImage(webcam.width,webcam.height);
  slomo1.setAdaption(ADDING, WEBCAM_ADAPTION);
  
  thresholdDifference = new ThresholdDifferenceImage(webcam.width, webcam.height, DIFFERENCE_THRESHOLD);
  
  slomo2 = new AdaptingImage(webcam.width,webcam.height);
  slomo2.setAdaption(ADDING, 100);
  
  combine = new PImage(webcam.width, webcam.height);
  combine.format = ARGB;
}

void draw() {
  background(255,0,0);
  
  /*
  * Read webcam
  */
  if (webcam.available()) {
    webcam.read();
    webcam.updatePixels();
  }
  
  /*
  * Set on command
  */
  if (setSlomo) {
    setSlomo = false;
    slomo1.set(webcam.pixels);
    thresholdDifference.create(webcam.pixels, slomo1.pixels);
    slomo2.set(thresholdDifference.pixels);
  }
  
  slomo1.adaptFrom(webcam.pixels);
  thresholdDifference.create(webcam.pixels, slomo1.pixels);
  slomo2.adaptFrom(thresholdDifference.pixels);
  
  combineAlphaAndRGB(slomo2.pixels,webcam.pixels,combine.pixels);
  
  image(webcam,0,0);
  image(slomo1,WEBCAM_WIDTH,0);
  image(thresholdDifference,0,WEBCAM_HEIGHT);
  image(slomo2,WEBCAM_WIDTH,WEBCAM_HEIGHT);
  
  image(slomo1,WEBCAM_WIDTH*2,WEBCAM_HEIGHT/2);
  image(combine,WEBCAM_WIDTH*2,WEBCAM_HEIGHT/2);
}

void combineAlphaAndRGB(int[] a, int[] rgb, int[] out) {
  for (int i=0;i<a.length;i++) {
    out[i] = (a[i]&0xff)<<24 | (rgb[i]&0xffffff);
  }
}

void keyPressed() {
  if (key==' ') {
    setSlomo = true;
    println("SLOMO");
  }
  
  if (key==CODED) {
    if (keyCode==UP) {
      thresholdDifference.setThreshold(thresholdDifference.getThreshold()+10);
      println(thresholdDifference.getThreshold());
    } else if (keyCode==DOWN) {
      thresholdDifference.setThreshold(thresholdDifference.getThreshold()-10);
      println(thresholdDifference.getThreshold());
    }
  }
}
