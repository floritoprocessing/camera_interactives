/**
 * 
 */
package florito.movietolongimage;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import processing.core.PApplet;
import processing.core.PImage;

/**
 * @author Marcus
 *
 */
public class MovieToLongImage02 extends PApplet {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"florito.movietolongimage.MovieToLongImage02"});
	}
	
	//final String inFramesPath = "D:\\_PROJECTS\\Video Effects Dez\\frames\\";
	//final String inFramesPath = "D:\\_PROJECTS\\Iball\\_SIMULATION\\VideoWithStickersShorterFrames\\";
	//final String inFramesPath = "D:\\_PROJECTS\\Iball\\_SIMULATION\\TodaysArt03\\";
	final String inFramesPath = "C:\\tempCruiseToL2\\";
	String[] inFrames;
	
	final String outImagePath = "D:\\Processing\\_output\\MovieToLongImage02_05_CruiseToL2\\";
	int outSubimageWidth = 1000;
	
	ArrayList<String> outImages = new ArrayList<String>();
	
	int[] sourcePixels;
	PImage sourceImage;
	int sourceWidth, sourceHeight;
	boolean showSource = false;
	
	PImage drawImage;
	
	int currentColumn = 0;
	
	public void setup() {
		
		
		/*
		 * Get all filenames in directory and sort by name
		 */
		File[] allFiles = new File(inFramesPath).listFiles();
		int fileCount = 0;
		for (int i=0;i<allFiles.length;i++) {
			if (allFiles[i].isFile()) fileCount++;
		}
		inFrames = new String[fileCount];
		fileCount=0;
		for (int i=0;i<allFiles.length;i++) {
			if (allFiles[i].isFile()) {
				inFrames[fileCount++] = allFiles[i].getAbsolutePath();
			}
		}
		Arrays.sort(inFrames);
		
		PImage temp = loadImage(inFrames[0]);
		sourceWidth = temp.width;
		sourceHeight = temp.height;
		println("Images are "+sourceWidth+"x"+sourceHeight);
		
		
		size(outSubimageWidth,sourceHeight,P3D);
		
		frameRate(1000);
		
		sourcePixels = new int[sourceWidth*sourceHeight];
		sourceImage = new PImage(sourceWidth,sourceHeight);
		drawImage = new PImage(outSubimageWidth,sourceHeight);

	}
	
	
	private void sampleSource() {
		long r=0, g=0, b=0;
		int x, y, i=0;
		for (y=0;y<sourceHeight;y++) {
			r=0;
			g=0;
			b=0;
			for (x=0;x<sourceWidth;x++) {
				r += sourcePixels[i]>>16&0xff;
				g += sourcePixels[i]>>8&0xff;
				b += sourcePixels[i]&0xff;
				i++;
			}
			r /= sourceImage.width;
			g /= sourceImage.width;
			b /= sourceImage.width;
			drawImage.set(currentColumn, y, color(r,g,b));
		}
	}
	
	/**
	 * Returns true when column is 0
	 * @return
	 */
	private boolean nextColumn() {
		currentColumn++;
		if (currentColumn==drawImage.width) {
			currentColumn=0;
			return true;
		}
		return false;
	}
	
	public void keyPressed() {
		if (key=='s') showSource = !showSource;
	}
	
	public void draw() {
		
		
		int frameNumber = (frameCount-1);//%framesFilenames.length;
		println("frame: "+frameNumber+" / "+(inFrames.length-1));
		
			if (frameNumber<inFrames.length) {
			
			sourceImage = loadImage(inFrames[frameNumber]);
			sourcePixels = sourceImage.pixels;
			
			sampleSource();
			
			background(0);
			
			image(drawImage,0,0);
			
			if (showSource) {
				image(sourceImage,currentColumn,0);
				if (currentColumn+sourceImage.width>outSubimageWidth) {
					image(sourceImage,currentColumn-outSubimageWidth,0);
				}
			}
			
			
			if (nextColumn()) {
				String filename = outImagePath+"frame_"+nf(outImages.size(),5)+".bmp";
				outImages.add(filename);
				File out = new File(filename);
				out.mkdirs();
				drawImage.save(filename);
				drawImage = new PImage(outSubimageWidth,sourceHeight);
			}
			
		} else {
			/*
			 * Save last image
			 */
			String filename = outImagePath+"frame_"+nf(outImages.size(),5)+".bmp";
			outImages.add(filename);
			PImage lastImage = new PImage(currentColumn,sourceHeight);
			lastImage.copy(drawImage, 0, 0, currentColumn, drawImage.height, 0, 0, currentColumn, drawImage.height);
			File out = new File(filename);
			out.mkdirs();
			lastImage.save(filename);
			
			/*
			 * Combine
			 */
			int totalWidth = (outImages.size()-1)*outSubimageWidth + currentColumn;
			PImage total = new PImage(totalWidth, sourceHeight);
			int x=0;
			for (String name:outImages) {
				PImage img = loadImage(name);
				total.copy(img, 0, 0, img.width, img.height, x, 0, img.width, img.height);
				x+=img.width;
			}
			total.save(outImagePath+"FINAL.bmp");
			
			
			System.exit(0);
		}
		
	}
}
