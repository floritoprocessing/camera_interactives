/**
 * 
 */
package florito.movietolongimage;

import processing.core.PApplet;
import processing.core.PImage;
import processing.video.Capture;
import processing.video.Movie;

/**
 * @author Marcus
 *
 */
public class MovieToLongImage extends PApplet {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"florito.movietolongimage.MovieToLongImage"});
	}
	
	final int CAMERA = 0;
	final int VIDEO = 1;
	final int FRAMES_FOLDER = 2;
	
	int source = CAMERA;
	
	String videoPath = "d:\\FluidMirror_Demo1.mov";
	int videoWidth = 640;
	int videoHeight = 480;
	int videoFrameRate = 25;
	float videoDuration;
	int videoCurrentFrame;
	int videoFrames;
	
	String framesPath = "D:\\_PROJECTS\\Video Effects Dez\\frames50pBW\\";
	String[] framesFilenames;
	
	
	Movie video;
	Capture cam;
	
	int[] sourcePixels;
	PImage sourceImage;
	int sourceWidth, sourceHeight;
	boolean showSource = true;
	
	PImage drawImage;
	int targetWidth = 800;
	int currentColumn = 0;
	
	public void settings() {
		size(targetWidth,180,P3D);
	}
	
	public void setup() {
		
		if (source==CAMERA) {
			cam = new Capture(this,320,240);
			cam.start();
			sourceWidth = cam.width;
			sourceHeight = cam.height;
			println("starting camera at "+sourceWidth+"x"+sourceHeight);
		} else if (source==VIDEO) {
			video = new Movie(this, videoPath);
			video.speed(0);
			sourceWidth = videoWidth;//video.width;
			sourceHeight = videoHeight;//video.height;
			videoDuration = video.duration();
			videoFrames = (int)(videoDuration*videoFrameRate);
			println("starting video at "+sourceWidth+"x"+sourceHeight);
			println("frames: "+videoFrames);
		}
		
		sourcePixels = new int[sourceWidth*sourceHeight];
		sourceImage = new PImage(sourceWidth,sourceHeight);
		drawImage = new PImage(targetWidth,sourceHeight);

	}
	
	/*public void movieEvent(Movie m) {
		
		//m.play();
		m.read();
		//m.pause();
		println("reading movie at time "+m.time());
		
		//m.pause();
		//m.play();
		//m.speed(1.0f/videoFrameRate);
		
		//videoCurrentFrame++;
		//m.jump((float)videoCurrentFrame/videoFrameRate);
	}*/
	
	private void sampleSource() {
		long r=0, g=0, b=0;
		int x, y, i=0;
		for (y=0;y<sourceHeight;y++) {
			r=0;
			g=0;
			b=0;
			for (x=0;x<sourceWidth;x++) {
				r += sourcePixels[i]>>16&0xff;
				g += sourcePixels[i]>>8&0xff;
				b += sourcePixels[i]&0xff;
				i++;
			}
			r /= sourceImage.width;
			g /= sourceImage.width;
			b /= sourceImage.width;
			drawImage.set(currentColumn, y, color(r,g,b));
		}
		drawImage.updatePixels();
	}
	
	private void nextColumn() {
		currentColumn++;
		if (currentColumn==drawImage.width) {
			currentColumn=0;
		}
	}
	
	public void draw() {
		if (source==CAMERA) {
			if (cam.available() ) {
				cam.read();
				cam.loadPixels();
			}
//			cam.update();
			sourcePixels = cam.pixels;//cameraImage();
			
		} else {
			video.jump((float)videoCurrentFrame/videoFrameRate);
			videoCurrentFrame++;
			if (videoCurrentFrame>=videoFrames) {
				videoCurrentFrame=0;
			}
			video.read();
			sourcePixels = video.pixels;
		}
		sampleSource();
		nextColumn();
		background(0);
		
		image(drawImage,0,0);
		
		if (showSource) {
			sourceImage.pixels = sourcePixels;
			sourceImage.updatePixels();
			image(sourceImage,0,0,sourceImage.width/2,sourceImage.height/2);
			
		}
	}
}
