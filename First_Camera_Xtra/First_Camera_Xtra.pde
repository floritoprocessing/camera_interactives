import JMyron.*;

JMyron m;//a camera object
/*
  point the camera at some bright objects in the room and the boxes will follow.
*/
 
void setup(){
  size(320,240);
  m = new JMyron();//make a new instance of the object
  m.start(width,height);//start a capture at 320x240
  m.trackColor(255,255,255,100);
  m.minDensity(100);
  
  println("Myron " + m.version());
  noFill();
}

void draw(){
  m.update();//update the camera view

  int[] img = m.image(); //get the normal image of the camera
  for(int i=0;i<width*height;i++){ //loop through all the pixels
    pixels[i] = img[i]; //draw each pixel to the screen
  }

  
  m.trackColor(255,0,255,120);
  int[][] b = m.globBoxes();//get the center points
  //draw the boxes
  for(int i=0;i<b.length;i++){
    stroke(255,0,255);
    rect(b[i][0],b[i][1],b[i][2],b[i][3]);
  }
  
  m.trackColor(255,255,0,120);
  int[][] c = m.globBoxes();//get the center points
  //draw the boxes
  for(int i=0;i<c.length;i++){
    stroke(255,255,0);
    rect(c[i][0],c[i][1],c[i][2],c[i][3]);
  }
 
}

void mousePressed(){
  //m.settings();//click the window to get the settings
}

public void stop(){
  m.stop();//stop the object
}

