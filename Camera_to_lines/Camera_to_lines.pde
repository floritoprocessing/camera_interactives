import processing.video.*;
Capture video;

color[] imgbuffer, bgbuffer; // imgbuffer holds camera, bgbuffer holds background
color[][] tempbgbuffer;      // buffers used for averaging background buffer
int nrOfBgBuf, bgBufToTake;
boolean getBgBuf, gotBgBuf;
int cameraW, cameraH, cameraPix, cameraFPS;
float camToScreen;
boolean newFrame, showCamera, mirrorCamera, showNoBuffer;

int nrOfActivePixels, activePixelThreshhold;  float focusX, focusY; Pixel[] activePixel; int maxActivePixels;
Spraydot[] spraydot; int nrOfSpraydots, maxSpraydots, spraydotindex;

PImage fx;
int fc;
 
void setup() {	 
  // setup screen
  size(768, 576, P3D); 
  ellipseMode(DIAMETER); 
  rectMode(DIAMETER); 
  noStroke(); 
  //framerate(25); 
  background(255);

  // setup camera
  cameraW=100; cameraH=75; cameraPix=cameraW*cameraH; cameraFPS=25;
//  beginVideo(cameraW,cameraH,cameraFPS);
  video = new Capture(this,cameraW,cameraH,cameraFPS);
  video.start();
  
  // setup camera buffer
  imgbuffer=new color[cameraPix]; 
  
  // setup background buffer
  nrOfBgBuf=5; getBgBuf=false; gotBgBuf=false;
  bgbuffer=new color[cameraPix]; tempbgbuffer=new color[nrOfBgBuf][cameraPix];
//  setupBackground();
  
  // setup active pixels;
  nrOfActivePixels=0; activePixel=new Pixel[cameraPix]; activePixelThreshhold=500;
  
  // setup spraydots
  maxSpraydots=20000; spraydot=new Spraydot[maxSpraydots]; nrOfSpraydots=maxSpraydots; spraydotindex=0;
  for (int i=0;i<nrOfSpraydots;i++) {
    spraydot[i]=new Spraydot(new Vec(0,0),new Vec(0,0));
    spraydot[i].alive=false;
  }
  
  // setup values
  newFrame=false; 
  showCamera=true;  // if false-> show background buffer
  mirrorCamera=true;
  showNoBuffer=false;
  camToScreen=width/(float)cameraW;
  fc=0;
} 
 

 
 
void draw() {
  background(255);
  
  //if (video.available()) {
  //  videoEvent();
  //  video.read();
  //  video.loadPixels();
  //}
  video.read();
  video.loadPixels();
  
  // get new background buffer if requested
  if (getBgBuf&&newFrame) {
    getNewBackgroundBuffer();
  }
  
  // show image buffer!
  if (!showNoBuffer) {showImageBuffer();}
  
  // get nrOfActivePixels, focusX, focusY, activePixel:
  getDifferenceImage(); 
  
  // show focusX/focusY
//  if (nrOfActivePixels>20) {
//    fill(255,255,255,128);
//    ellipse(focusX*camToScreen,focusY*camToScreen,100,100);
//  }
  
  // create sprayparticles
  if (nrOfActivePixels>activePixelThreshhold&&gotBgBuf) {
    for (int i=0;i<nrOfActivePixels;i++) {
      if (random(1)<0.6) {
        float x=activePixel[i].x, y=activePixel[i].y;
        float xm=0.03*(x-focusX), ym=0.03*(y-focusY);
        spraydot[spraydotindex].init(new Vec(x*camToScreen,y*camToScreen),new Vec(xm*camToScreen,ym*camToScreen));
        spraydotindex++; if (spraydotindex==nrOfSpraydots) {spraydotindex=0;}
      }    
    }
  }
  
  if (gotBgBuf) {
    for (int i=0;i<nrOfSpraydots;i++) {
      spraydot[i].update();
    }
  }
} 



void getDifferenceImage() {
  nrOfActivePixels=0; focusX=0; focusY=0;
  for (int x=0;x<cameraW;x++) {
    for (int y=0;y<cameraH;y++) {
      int i=xyToIndex(x,y);
      if (colorDifference(imgbuffer[i],bgbuffer[i])>0.2) {
        activePixel[nrOfActivePixels]=new Pixel(x,y,imgbuffer[i]);
        focusX+=x; focusY+=y; nrOfActivePixels++;
      }
    }
  }
  if (nrOfActivePixels!=0) {
    focusX/=nrOfActivePixels;
    focusY/=nrOfActivePixels;
  }
}

void showImageBuffer() {
  for (int x=0;x<cameraW;x++) {
    for (int y=0;y<cameraH;y++) {
      int i=xyToIndex(x,y);
      if (showCamera) {
        fill(imgbuffer[i]);
      } else {
        fill(bgbuffer[i]);
      }
      rect((x+.5)*camToScreen,(y+.5)*camToScreen,camToScreen,camToScreen);
    }
  }
}

void getNewBackgroundBuffer() {
  if (bgBufToTake<nrOfBgBuf) {
    // get another bg into a buffer each frame
    for (int i=0;i<cameraPix;i++) {
      tempbgbuffer[bgBufToTake][i]=imgbuffer[i];
    }
    bgBufToTake++;
  } else {
    // create an average bg buffer from prevoously taken bg buffers
    for (int i=0;i<cameraPix;i++) {
      float rr=0,gg=0,bb=0;
      for (int j=0;j<nrOfBgBuf;j++) {
        color cc=tempbgbuffer[j][i];
        rr+=red(cc); gg+=green(cc); bb+=blue(cc);
      }
      rr/=(float)nrOfBgBuf; gg/=(float)nrOfBgBuf; bb/=(float)nrOfBgBuf;
      bgbuffer[i]=color(rr,gg,bb);
    }
    getBgBuf=false; gotBgBuf=true;
  }
}


void videoEvent() { 
  // refresh camera buffer
  video.read();
  for (int x=0;x<cameraW;x++) {
    for (int y=0;y<cameraH;y++) {
      if (mirrorCamera) {
        imgbuffer[xyToIndex(x,y)]=video.pixels[xyToIndex(cameraW-1-x,y)];
      } else {
        imgbuffer[xyToIndex(x,y)]=video.pixels[xyToIndex(x,y)];
      }
    }
  }
  newFrame=true;
}

void keyPressed() {
  if (key==' ') { setupBackground(); }
  if (key=='n'||key=='N') { showNoBuffer=!showNoBuffer; }
  if (key=='c'||key=='C') { showCamera=true; }
  if (key=='b'||key=='B') { showCamera=false; }
}

void setupBackground() {
  bgBufToTake=0; getBgBuf=true;
}


// classes
// -------

class Pixel {
  int x,y;
  color c;
  Pixel(int ix, int iy, color ic) {
    x=ix; y=iy; c=ic;
  }
}

class Spraydot {
  Vec pos,mov;
  Vec floatvec;
  boolean alive;
  color c; float cmix;
  Spraydot(Vec p, Vec m) {
    init(p,m);
  }
  
  void init(Vec p,Vec m) {
    alive=true;
    pos=p; mov=m;
    pos=VecAdd(pos,new Vec(random(-camToScreen/2.0,camToScreen/2.0),random(-camToScreen/2.0,camToScreen/2.0)));
    floatvec=new Vec(0,-.3);
    c=color(random(5),random(5),random(5));
    cmix=1.0;
  }
  
  void update() {
    if (alive) {
      mov=VecAdd(mov,floatvec);
      mov=VecAdd(mov,new Vec(noise(pos.x,pos.y)*2.0-1.0,noise(pos.y,pos.x)*2.0-1.0));
      mov=VecMul(0.8,mov);
      pos=VecAdd(pos,mov);
      cmix*=0.985;
      int sx=int(pos.x), sy=int(pos.y);
      if (sx>=1&&sx<=width&&sy>=1&&sy<=height) {
        set(sx,sy,colorMix(cmix,c,color(255,255,255)));
      }
      if (pos.y<0) {alive=false;}
    }
  }
}


// functions
// ---------

int xyToIndex(int x, int y) {
  return (y*video.width+x);
}

float colorDifference(color a, color b) {
  float r1=red(a), g1=green(a), b1=blue(a);
  float r2=red(b), g2=green(b), b2=blue(b);
  float rd=abs(r1-r2)/255.0, gd=abs(g1-g2)/255.0, bd=abs(b1-b2)/255.0;
  return ((rd+gd+bd)/1.0);
}

color colorMul(float n,color a) {
  float r1=n*red(a), g1=n*green(a), b1=n*blue(a);
  return color(r1,g1,b1);
}

color colorAdd(color a, color b) {
  float r1=red(a), g1=green(a), b1=blue(a);
  float r2=red(b), g2=green(b), b2=blue(b);
  float ro=r1+r2, go=g1+g2, bo=b1+b2;
  return color(ro,go,bo);
}

color colorMix(float percA, color a, color b) {
  float percB=1.0-percA;
  float r1=red(a), g1=green(a), b1=blue(a);
  float r2=red(b), g2=green(b), b2=blue(b);
  float ro=percA*r1+percB*r2, go=percA*g1+percB*g2, bo=percA*b1+percB*b2;
  return color(ro,go,bo);
}


// VECTOR CLASS AND MATHEMATICS --------------------------------
// -------------------------------------------------------------
class Vec {
  float x,y;
  Vec(float x,float y) {
    this.x=x; this.y=y;
  }
}

Vec VecAdd(Vec a, Vec b) {
  Vec z=new Vec(0,0);
  z.x=a.x+b.x; z.y=a.y+b.y;
  return z;
}

Vec VecSub(Vec a, Vec b) {
  Vec z=new Vec(0,0);
  z.x=a.x-b.x; z.y=a.y-b.y;
  return z;
}

Vec VecMul(float a, Vec b) {
  Vec z=new Vec(0,0);
  z.x=a*b.x; z.y=a*b.y;
  return z;
}

float VecSkalar(Vec a, Vec b) { // (len(a)*len(b)*cos(angle between a and b)
  float z;
  z=a.x*b.x+a.y*b.y;
  return z;
}

float VecLen(Vec a) {
  float z=sqrt(a.x*a.x+a.y*a.y);
  return z;
}

Vec VecInScreen(Vec a) {
  Vec z=a;
  while (z.x>width) {z.x-=width;} while (z.x<1) {z.x+=width;}
  return z;
}
// -------------------------------------------------------------
// (end vector class and mathematics) --------------------------
