import processing.video.*;
Capture cap;

boolean newFrame;
float lastFrame, fps;
color vidbuf[];
Dot[] dot; int nrOfDots;
float mmovx, mmovy;
boolean firstSetup;

void setup() {
  size(400,300,P3D); 
  stroke(0); 
  fill(255); 
  rectMode(DIAMETER); 
  //framerate(25);
  
  newFrame=false;  
  //beginVideo(400,300,25);  
  cap = new Capture(this,400,300,25);
  cap.start();
  lastFrame=0.0; fps=0.0;
  nrOfDots=400*300;  dot=new Dot[nrOfDots];
  nrOfDots=0;
  for (int x=0;x<width;x++) {
    for (int y=0;y<height;y++) {
      dot[nrOfDots]=new Dot(x,y,0);
      nrOfDots++;
    }
  }
  mmovx=0; mmovy=0;
}

void draw() {
  //background(0);
  if (cap.available()) {
    videoEvent();
  }
  if (newFrame) {
    cap.loadPixels();
    // update original color
    for (int i=0;i<nrOfDots;i++) {
      color tc=cap.pixels[dot[i].sy*width+dot[i].sx];
      dot[i].updateColor(tc);
    }
  }
  mmovx=mouseX-pmouseX; mmovy=mouseY-pmouseY;
  
  for (int i=0;i<nrOfDots;i++) {
    dot[i].update();
  }
  
  newFrame=false;
}

void displayFramerate() {
  for (int i=1;i<=fps;i++) {
    if (i%10==0) {fill(255,0,0);} else {fill(255,255,255);}
    rect(i*4,4,3,3);
  }
}

void videoEvent() {
  cap.read();
  fps=1000/(float)(millis()-lastFrame);
  lastFrame=millis();
  //image(video,0,0);
  displayFramerate();
  newFrame=true;
}

class Dot {
  int sx,sy;
  float x,y,d,ms;
  color c;

  float easeA=250, easeB=easeA+1.0;
  float mouseinflu=1.2;
  
  Dot(int ix, int iy, color ic) {
    c=ic;
    sx=ix; sy=iy; x=sx; y=sy;
  }
  
  void update() {
    if (mousePressed) {
      d=dist(x,y,mouseX,mouseY);
      if (d<30) {
        ms=pow((1.0-(d/30.0)),2);
        x+=ms*mouseinflu*mmovx; y+=ms*mouseinflu*mmovy;
      }
    }
    x=(easeA*x+sx)/easeB; y=(easeA*y+sy)/easeB;
    x=constrain(x,0,width-1); y=constrain(y,0,height-1);
    set(int(x),int(y),c);
  }
  
  void updateColor(color ic) {
    c=colorFade(c,ic,0.8);
  }
  
}


color colorFade(color a, color b, float perc1) {
  float perc2=1.0-perc1;
  float rr=perc1*red(a)+perc2*red(b), gg=perc1*green(a)+perc2*green(b), bb=perc1*blue(a)+perc2*blue(b);
  return color(rr,gg,bb);
}
