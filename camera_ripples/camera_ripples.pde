import processing.video.*;

Capture cap;

color black=#000000;

int cameraW, cameraH, cameraPix;
color[] imgbuffer;
boolean newFrame, mirrorCamera;

PImage img; 
int image_w,image_h;
float av_brightness;
DistImage distimage;

void setup() {
  size(800,600,P3D); 
  noStroke(); 
  noFill(); 
  ellipseMode(DIAMETER);//CENTER_DIAMETER);
  
  cameraW=160; 
  cameraH=120; 
  cameraPix=cameraW*cameraH;
  
  imgbuffer=new color[cameraPix]; 
  
  //beginVideo(cameraW,cameraH,25);
  cap = new Capture(this,cameraW,cameraH,25);
  cap.start();
  
  newFrame=false; mirrorCamera=true;
  
  img=loadImage("400x300.jpg"); 
  image_w=img.width; 
  image_h=img.height;
  av_brightness=getAverageBrightness(img);
  distimage=new DistImage(img);
}



void draw() {
  background(black);
  
  newFrame = true;//cap.available();
  cap.read();
  cap.loadPixels();
  if (newFrame) {
    for (int x=0;x<image_w;x++) {
      for (int y=0;y<image_h;y++) {
        int cx=cameraW-int((float)cameraW*x/image_w)-1;
        int cy=int((float)cameraH*y/image_h);
        //img.set(x,y,video.pixels[cy*cameraW+cx]);
        //cap.loadPixels();
        img.set(x,y,cap.pixels[cy*cameraW+cx]);
      }
    }
    av_brightness=getAverageBrightness(img);
    distimage.getImage(img);
  }
  
  image(img,100,200,img.width*1.5,img.height*2/3.0);
  distimage.update();
  showDistImage(100,400,false,1.5,0);
  showDistImage(100,200,true,1.5,0);
  
  newFrame=false;
}



class DistImage {
  color[] c;
  int w,h;
  float[] x,y, sx,sy;
  float[] freq, amp, phase;
  float ti;
  
  DistImage(PImage in_img) {
    ti=0.0;
    w=in_img.width; h=in_img.height;
    int maxpix=h*w;
    c=new color[maxpix];
    x=new float[maxpix]; y=new float[maxpix]; sx=new float[maxpix];
    amp=new float[maxpix]; phase=new float[maxpix];
    
    for (int iy=0;iy<h;iy++) {
      for (int ix=0;ix<w;ix++) {
        int i=iy*w+ix;
       
        c[i]=in_img.pixels[i];
        x[i]=ix;
        sx[i]=w/2.0 + (1+0.5*(h-iy)/((float)h))*(ix-w/2.0);
        y[i]=h-iy;
        
        amp[i]=2+20*(h-iy)/((float)h);
        phase[i]=10*TWO_PI*pow(iy/(float)h,2);
      }
    }
  }
  
  void update() {
    ti+=0.45;
    for (int iy=0;iy<h;iy++) {
      for (int ix=0;ix<w;ix++) {
        int i=iy*w+ix;
        x[i]=sx[i]+amp[i]*sin(ti + phase[i]);
      }
    }
  }
  
  void getImage(PImage in_img) {
    for (int iy=0;iy<h;iy++) {
      for (int ix=0;ix<w;ix++) {
        int i=iy*w+ix;
        c[i]=in_img.pixels[i];
      }
    }
  }
}

void showDistImage(int xo, int yo, boolean ySwap, float xScale, float br) {
  int sw; color c;
  if (ySwap) {sw=-1;} else {sw=1;}
  for (int y=0;y<image_h;y++) {
    for (int x=0;x<image_w;x++) {
      int i=y*image_w+x;
      if (ySwap) {
        c=distimage.c[(image_h-y-1)*image_w+x];
      } else {
        c=distimage.c[i];
      }
      if (brightness(c)>br) {
        set(int(xScale*distimage.x[i])+xo,yo+int(sw*distimage.y[i]),c);
      }
    }
  }
}

float getAverageBrightness(PImage img) {
  int n=img.width*img.height;
  float br=0.0;
  for (int i=0;i<n;i++) {
    br+=brightness(img.pixels[i]);
  } 
  br/=(float)n;
  return br;
}

void videoEvent() { 
  newFrame=true;
}
