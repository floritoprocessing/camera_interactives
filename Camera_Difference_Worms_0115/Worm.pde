class Worm {
  float x=0, y=0;
  float xm=0, ym=0;
  float speed=1.0;
  float x_max, y_max;
  int col=0xFFFFFF;
  float age=0;
  float AGE_MAXIMUM=150.0;
  
  Worm(int mW,int mH, float spd) {
    x_max=mW;
    y_max=mH;
    speed=spd;
    float rndDir=random(TWO_PI);
    x=random(x_max);
    y=random(y_max);
    xm=speed*cos(rndDir);
    ym=speed*sin(rndDir);
  }
  
  void setPos(int _x, int _y) {
    x=_x;
    y=_y;
  }
  
  void setColor(int _c) {
    col=_c;
  }
  
  void move() {
    xm+=random(-0.05,0.05);
    ym+=random(-0.05,0.05);
    x+=xm;
    y+=ym;
    if (x<0) x+=x_max;
    if (x>=x_max) x-=x_max;
    if (y<0) y+=y_max;
    if (y>=y_max) y-=y_max;
  }
  
  void age() {
    age++;
  }
  
  void setAgePerc(float p) {
    age=p*AGE_MAXIMUM;
  }
  
  boolean isDead() {
    return (age>AGE_MAXIMUM);
  }
  
  void draw() {
    float p=age/AGE_MAXIMUM;
    set(int(x),int(y),mixColor(col,get(int(x),int(y)),1.0-p,p));
  }
  
}
