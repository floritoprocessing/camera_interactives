PImage createDiffImage(PImage img1, PImage img2) {
  PImage out=new PImage(img1.width,img1.height);
  for (int i=0;i<out.pixels.length;i++) {
    int px1=img1.pixels[i];
    int px2=img2.pixels[i];
    int r1=(px1>>16)&0xff;
    int r2=(px2>>16)&0xff;
    int g1=(px1>>8)&0xff;
    int g2=(px2>>8)&0xff;
    int b1=px1&0xff;
    int b2=px2&0xff;
    int rr=abs(r1-r2);
    int gg=abs(g1-g2);
    int bb=abs(b1-b2);
    out.pixels[i]=rr<<16|gg<<8|bb;
  }
  return out;
}

int mixColor(int c1, int c2, float p1, float p2) {
  int out=0;
  int r1=(c1>>16)&0xff;
  int r2=(c2>>16)&0xff;
  int g1=(c1>>8)&0xff;
  int g2=(c2>>8)&0xff;
  int b1=c1&0xff;
  int b2=c2&0xff;
  int rr=int(r1*p1+r2*p2);
  int gg=int(g1*p1+g2*p2);
  int bb=int(b1*p1+b2*p2);
  out=255<<24|rr<<16|gg<<8|bb;
  return out;
}

int addColor(int c1, int c2, float p1, float p2) {
  int out=0;
  int r1=(c1>>16)&0xff;
  int r2=(c2>>16)&0xff;
  int g1=(c1>>8)&0xff;
  int g2=(c2>>8)&0xff;
  int b1=c1&0xff;
  int b2=c2&0xff;
  int rr=int(constrain(r1*p1+r2*p2,0,255));
  int gg=int(constrain(g1*p1+g2*p2,0,255));
  int bb=int(constrain(b1*p1+b2*p2,0,255));
  out=255<<24|rr<<16|gg<<8|bb;
  return out;
}

int fadeColor(int c1, float p1) {
  int out=mixColor(c1,0x000000,p1,1.0-p1);
  return out;
}


PImage mixImage(PImage img1, PImage img2, float p1, float p2) {
  PImage out=new PImage(img1.width,img1.height);
  for (int i=0;i<img1.pixels.length;i++) out.pixels[i]=mixColor(img1.pixels[i],img2.pixels[i],p1,p2);
  out.updatePixels();
  return out;
}

PImage addImage(PImage img1, PImage img2, float p1, float p2) {
  PImage out=new PImage(img1.width,img1.height);
  for (int i=0;i<img1.pixels.length;i++) out.pixels[i]=addColor(img1.pixels[i],img2.pixels[i],p1,p2);
  out.updatePixels();
  return out;
}

PImage fadeImage(PImage img1, float p1) {
  PImage out=new PImage(img1.width,img1.height);
  for (int i=0;i<img1.pixels.length;i++) out.pixels[i]=fadeColor(img1.pixels[i],p1);
  out.updatePixels();
  return out;
}
