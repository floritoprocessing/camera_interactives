import processing.video.*; 
import java.util.Vector;

int CAPTURE_WID=320;
int CAPTURE_HEI=240;
int WORM_CREATION_THRESHOLD=30; // 0..255

WormWorld wormWorld;

Capture cap;
PImage capImage, lastImage, procImage;

boolean showCap=false;
boolean showProc=false;

void keyPressed() {
  if (key=='1') showCap=!showCap;
  if (key=='2') showProc=!showProc;
  if (key=='c') background(255);
}

void setup() {
  size(400,300,P3D);
  cap=new Capture(this,CAPTURE_WID,CAPTURE_HEI,25);
  cap.start();
  capImage=new PImage(CAPTURE_WID,CAPTURE_HEI);
  lastImage=new PImage(CAPTURE_WID,CAPTURE_HEI);
  procImage=new PImage(CAPTURE_WID,CAPTURE_HEI);
  wormWorld=new WormWorld(width,height);
}

void draw() {
  boolean newFrame=cap.available();
  if(newFrame) {
    cap.read();
    cap.loadPixels();
    for (int y=0;y<cap.height;y++) {
      for (int x=0;x<cap.width;x++) {
        capImage.pixels[x+cap.width*y]=cap.pixels[cap.width-x-1+cap.width*y];
      }
    }
    capImage.updatePixels();
    procImage=mixImage(procImage,createDiffImage(capImage,lastImage),0.8,0.2);
  }
  
  
  //
  if (showCap&&!showProc) image(capImage,0,0,width,height);
  if (showProc&&!showCap) image(procImage,0,0,width,height);
  if (showProc&&showCap) {
    image(capImage,0,0,width/2,height);
    image(procImage,width/2,0,width/2,height);
  }
  
  for (int i=0;i<procImage.pixels.length;i++) {
    float br=brightness(procImage.pixels[i]);
    if (br>WORM_CREATION_THRESHOLD) {
      int sx=int(i%procImage.width/(float)CAPTURE_WID*(float)width);
      int sy=int(i/procImage.width/(float)CAPTURE_HEI*(float)height);
      wormWorld.newWorm(sx,sy,capImage.pixels[i]);
    }
  }
  
  wormWorld.update();
  wormWorld.draw();
  // create Worms
  
  if(newFrame) for (int i=0;i<lastImage.pixels.length;i++) lastImage.pixels[i]=capImage.pixels[i];
}
