class WormWorld {
  int width=0;
  int height=0;
  int MAXIMUM_AMOUNT_WORMS=40000;
  
  Vector worms;
  
  WormWorld(int w, int h) {
    width=w;
    height=h;
    worms=new Vector();
  }
  
  void newWorm(int x, int y, color c) {
    if (worms.size()<MAXIMUM_AMOUNT_WORMS) {
    Worm w=new Worm(width,height,0.3);
    w.setPos(x,y);
    w.setColor(c);
    worms.add(w);
    }
  }
  
  void update() {
    for (int i=0;i<worms.size();i++) {
      Worm w=(Worm)(worms.elementAt(i));
      w.move();
      w.age();
      if (w.isDead()) worms.remove(w);
    }
  }
  
  void draw() {
    for (int i=0;i<worms.size();i++) ((Worm)(worms.elementAt(i))).draw();
  }
}
