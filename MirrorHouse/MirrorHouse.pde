boolean newVideo=false;
float xscale, yscale;

void setup() {
  size(350,1000); // CRAZY BIGHEAD MIRROR
//  size(640,480);
  beginVideo(320,240,18);
  xscale=width/(float)video.width;
  yscale=height/(float)video.height;
}

void loop() {
  color c;
  int sx,sy;
  float cx,cy;
  if (newVideo) {
    for (sy=0;sy<height;sy++) { 
      float dxp=sy/(float)height;  // CRAZY BIGHEAD MIRROR
      
      for (sx=0;sx<width;sx++) {
      
//      float dxp=0.7+0.3*sin(PI*sy/(float)height);
//      float dyp=0.7+0.3*sin(PI*sx/(float)width);
      
      cx=video.width-sx/xscale-1;
      cx=(video.width/2.0) + dxp*(cx-video.width/2.0);
      
      cy=sy/yscale;
//      cy=(video.height/2.0) + dyp*(cy-video.height/2.0);
      
      c=video.pixels[int(cy)*video.width+int(cx)];
      set(sx,sy,c);
      } 
    }
  }
  newVideo=false;
}

void videoEvent() {
  newVideo=true;
}
