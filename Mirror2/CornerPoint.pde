class CornerPoint {
  float x, y, z;
  float u, v;
  float zmov;
  float zmovNeighbours=0;
  
  CornerPoint(float _x, float _y, float _z, float _u, float _v, float _zmov) {
    x=_x;
    y=_y;
    z=_z;
    u=_u;
    v=_v;
    zmov=_zmov;
  }
  
  void makeVertex() {
    vertex(x,y,z);
  }
  
  void makeUVVertex() {
    vertex(x,y,z,u,v);
  }
  
  float getZmov() {
    return zmov;
  }
  
  void addToZmov(float p) {
    zmov+=p;
  }
  
  void setZmovNeighbours(float zn) {
    zmovNeighbours=zn;
  }
  
  void updateZByZmovAndNeighbours() {
    zmov+=0.01*zmovNeighbours;
    zmov-=0.1*z;
    zmov*=0.7;
    z+=zmov;
  }
}
