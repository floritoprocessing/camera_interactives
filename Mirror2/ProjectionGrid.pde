class ProjectionGrid {
  int width, height;
  int pointsX, pointsY;
  CornerPoint[][] cornerPoint;
  PImage tex;
  int drawMode=2;
  
  public static final int DRAW_MODE_GRID=0;
  public static final int DRAW_MODE_GRID_AND_IMAGE=1;
  public static final int DRAW_MODE_IMAGE=2;
  
  ProjectionGrid(int w, int h, int divX, int divY) {
    this.width=w;
    this.height=h;
    pointsX=divX+1;
    pointsY=divY+1;
    cornerPoint=new CornerPoint[pointsX][pointsY];
    for (int x=0;x<pointsX;x++) {
      for (int y=0;y<pointsY;y++) {
        float[] myPoint=new float[6];
        float xperc=x/(float)divX;
        float yperc=y/(float)divY;
        float xpos=w*xperc;  //x
        float ypos=h*yperc;  //y
        float zpos=0;     //z
        float u=xperc;    //u
        float v=yperc;    //v
        float zmov=4-random(8);        //zmov
        cornerPoint[x][y]=new CornerPoint(xpos,ypos,zpos,u,v,zmov);
      }
    }
    tex=new PImage(w,h);
  }
  
  void setImage(PImage img) {
    tex.copy(img,0,0,img.width,img.height,0,0,tex.width,tex.height);
  }
  
  void updateZByZmovAndNeighbours() {
    for (int x=0;x<pointsX;x++) {
      for (int y=0;y<pointsY;y++) {
        // get all movements around this point:
        float nrOfNeighbours=0;
        float zmovNeighbours=0;
        for (int xo=-1;xo<=1;xo++) for (int yo=-1;yo<=1;yo++) {
          int nx=x+xo;
          int ny=y+yo;
          if (nx>=0&&nx<pointsX&&ny>=0&&ny<pointsY&&(!(xo==0&&yo==0))) {
            zmovNeighbours+=cornerPoint[nx][ny].getZmov();
            nrOfNeighbours++;
          }
        }
        // average movement of neighbours:
        zmovNeighbours/=nrOfNeighbours;
        cornerPoint[x][y].setZmovNeighbours(zmovNeighbours);
      }
    }
    
    
    for (int x=0;x<pointsX;x++) {
      for (int y=0;y<pointsY;y++) {
        cornerPoint[x][y].updateZByZmovAndNeighbours();
      }
    }
  }
  
  void pinchAt(int sx, int sy, float pressure) {
    // convert screen points to grid points:
    int x=(int)((pointsX-1)*sx/(float)this.width);
    int y=(int)((pointsY-1)*sy/(float)this.height);
    if (cornerPoint[x][y].getZmov()<0) cornerPoint[x][y].addToZmov(-pressure);
  }
  
  
  void addZmovByDeformationGrid(DeformationGrid dfgrid,float amount) {
    for (int x=0;x<pointsX;x++) for (int y=0;y<pointsY;y++) {
      cornerPoint[x][y].addToZmov(amount*dfgrid.getValueAt(x,y));
    }
  }
  
  
  void draw() {
    if (drawMode==DRAW_MODE_GRID) {
      stroke(255);
      noFill();
      beginShape(QUADS);
      for (int x=0;x<pointsX-1;x++) {
        for (int y=0;y<pointsY-1;y++) {
          cornerPoint[x][y].makeVertex();
          cornerPoint[x+1][y].makeVertex();
          cornerPoint[x+1][y+1].makeVertex();
          cornerPoint[x][y+1].makeVertex();
        }
      }
      endShape();
    }
    
    if (drawMode== DRAW_MODE_GRID_AND_IMAGE||drawMode==DRAW_MODE_IMAGE) {
      if (drawMode==DRAW_MODE_GRID_AND_IMAGE) stroke(255); else noStroke();
      fill(128);
      textureMode(NORMAL);
      beginShape(QUADS);
      texture(tex);
      for (int x=0;x<pointsX-1;x++) {
        for (int y=0;y<pointsY-1;y++) {
          cornerPoint[x][y].makeUVVertex();
          cornerPoint[x+1][y].makeUVVertex();
          cornerPoint[x+1][y+1].makeUVVertex();
          cornerPoint[x][y+1].makeUVVertex();
        }
      }
      endShape();
    }
  }
  
  
}
