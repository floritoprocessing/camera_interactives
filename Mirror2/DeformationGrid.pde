class DeformationGrid {
  
  int pointsX, pointsY;
  float[][] deform;
  
  DeformationGrid(int divX, int divY) {
    pointsX=divX+1;
    pointsY=divY+1;
    deform=new float[pointsX][pointsY];
    for (int x=0;x<pointsX;x++) for (int y=0;y<pointsY;y++) {
      deform[x][y]=0;
    }
  }
  
  void createBolb() {
    float maxX=pointsX-1;
    float maxY=pointsY-1;
    for (int x=0;x<pointsX;x++) for (int y=0;y<pointsY;y++) {
      float percX=x/maxX;
      float percY=y/maxY;
      deform[x][y]=sin(PI*percX)*sin(PI*percY);
    }
  }
  
  float getValueAt(int x,int y) {
    return deform[x][y];
  }
  
}
