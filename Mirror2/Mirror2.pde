import processing.video.*;

int SCREEN_WIDTH=640, SCREEN_HEIGHT=480;
int CAPTURE_WIDTH=320, CAPTURE_HEIGHT=240;
int GRID_WIDTH=16, GRID_HEIGHT=12;

Capture cap;
PImage capImage;
ProjectionGrid projectionGrid;
DeformationGrid deformationGrid;

void setup() {
  size(640,480,P3D);
  cap=new Capture(this,CAPTURE_WIDTH,CAPTURE_HEIGHT,25);
  cap.start();
  capImage=new PImage(SCREEN_WIDTH,SCREEN_HEIGHT);
  
  projectionGrid=new ProjectionGrid(SCREEN_WIDTH,SCREEN_HEIGHT,GRID_WIDTH,GRID_HEIGHT);
  deformationGrid=new DeformationGrid(GRID_WIDTH,GRID_HEIGHT);
  deformationGrid.createBolb();
  println("CLICK AND DRAG YOUR MOUSE HORIZONTAL!");
}


void captureEvent(Capture cap) {
  if (cap.available()) {
    cap.read();
    cap.loadPixels();
    capImage=flipHorizontal(cap);
    projectionGrid.setImage(capImage);
  }
}


void draw() {  
  background(0);
  /*
  // PINCH AT ONE POINT:
  if (mousePressed) projectionGrid.pinchAt(mouseX,mouseY,5);
  */
  
  if (mousePressed) projectionGrid.addZmovByDeformationGrid(deformationGrid,20*(mouseX-width/2.0)/(width/2.0));
  projectionGrid.updateZByZmovAndNeighbours();
  
  projectionGrid.draw();
}

void keyPressed() {
  saveFrame("Mirror2_preview###.jpg");
}


// simple camera processing:

PImage flipHorizontal(PImage img) {
  PImage out=new PImage(img.width,img.height);
  for (int y=0;y<img.height;y++) {
    for (int x=0;x<img.width;x++) {
      out.pixels[x+y*img.width]=img.pixels[img.width-1-x+y*img.width];
    }
  }
  out.updatePixels();
  return out;
}



//
