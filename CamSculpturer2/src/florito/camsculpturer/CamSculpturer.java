/**
 * 
 */
package florito.camsculpturer;

import processing.core.PApplet;
import processing.core.PImage;

/**
 * @author Marcus
 *
 */
public class CamSculpturer extends PApplet {
	
	public static boolean DEBUG;


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"florito.camsculpturer.CamSculpturer"});
	}
	
	private Webcam webcam;
	
	int smallScale = 4;
	PImage small, smallColor;
	
	int layerAmount = 50;
	int firstLayer = 0;
	PImage[] layer = new PImage[layerAmount];
	float layerTotalDepth = -800;
	
	boolean median = true;
	
	public void settings() {
		size(1024,768,P3D);
	}
	public void setup() {
		
		webcam = new Webcam(this,640,480);
		webcam.start();
//		webcam.start(640, 480);
//		webcam.adaptivity(0);
		small = new PImage(webcam.width/smallScale, webcam.height/smallScale);
		smallColor = new PImage(webcam.width/smallScale, webcam.height/smallScale);
		smooth();
		
		for (int i=0;i<layer.length;i++) {
			layer[i] = new PImage(small.width, small.height);
		}
		println(layer);
	}
	
	public void draw() {
//		if (frameCount<100) {
//			return;
//		}
		background(64);
		webcam.update(Webcam.CAMERA_IMAGE);
		
		/*PImage img = webcam.getCameraImage();
		img.filter(THRESHOLD,0.8f);
		image(img,0,0,width,height);*/
		
		small = new PImage(small.width,small.height);
		small.loadPixels();
		small.copy(webcam.getCameraImage(), 0, 0, webcam.width, webcam.height, 0, 0, small.width, small.height);
		small.updatePixels();
		smallColor = new PImage(small.width, small.height);//new PImage(small.getImage());
		smallColor.copy(small, 0, 0, small.width, small.height, 0, 0, small.width, small.height);
		
		small.filter(GRAY);
		if (median) {
			Median.filterBW(small);
			Median.filterBW(small);
		}
		small.filter(THRESHOLD,0.7f);
		small.filter(BLUR,2);
		small.format = ARGB;
		small.updatePixels();
		ImageToMask.filter(small, smallColor);
		
		layer[firstLayer] = small;
		
		//small.filter(BLUR,1);
		//image(smallColor,0,0,width,height);
		//image(small,0,0,small.width*2,small.height*2);
		
		int imgWidth = small.width*4;
		int imgHeight = small.height*4;
		
		translate(0,0,-500);
		for (int layerNr=firstLayer+layerAmount-1;layerNr>=firstLayer;layerNr--) {
			int layerIndex = layerNr%layerAmount;
			if (layer[layerIndex]!=null) {
				image(layer[layerIndex],(width-imgWidth)/2,(height-imgHeight)/2,imgWidth,imgHeight);
			}
			translate(0,0,10);
		}
				
		
		firstLayer--;
		if (firstLayer==-1) {
			firstLayer=layerAmount-1;
		}
	}
	
	public void keyPressed() {
		if (key=='m') median=!median;
//		else if (key=='s') webcam.settings();
	}
}
