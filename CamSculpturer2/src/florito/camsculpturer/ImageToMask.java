/**
 * 
 */
package florito.camsculpturer;

import processing.core.PImage;

/**
 * @author Marcus
 *
 */
public class ImageToMask {
		
	/**
	 * @param small
	 */
	public static void filter(PImage img, PImage colorImage) {
		for (int i=0;i<img.pixels.length;i++) {
			img.pixels[i] = (img.pixels[i]&0x00ff0000)<<8 | (colorImage.pixels[i]&0xffffff);
		}
	}
}
