/**
 * 
 */
package florito.camsculpturer;

import java.util.Arrays;

import processing.core.PImage;

/**
 * @author Marcus
 *
 */
public class Median {
	public static boolean DEBUG;

	public Median() {
	}

	public static void filterBW(PImage img) {
		
		int[] bright = new int[img.pixels.length];
		for (int i=0;i<bright.length;i++) {
			bright[i]=img.pixels[i]&0xff;
		}
		
		int width = img.width;
		int height = img.height;
		int x, y, i=0;//1+width;
		int[] px = new int[9];
		
		boolean corner;
		int[] pxCorner = new int[3];
		boolean side;
		int[] pxSide = new int[6];
		
		for (y=0;y<height;y++) {
			
			
			for (x=0;x<width;x++) {
				
				corner = false;
				side = false;
				if (x==0&&y==0) {
					pxCorner[0] = bright[0];
					pxCorner[1] = bright[1];
					pxCorner[2] = bright[width];
					corner=true;
				} else if (x==0&&y==height-1) {
					pxCorner[0] = bright[bright.length-width];
					pxCorner[1] = bright[bright.length-width+1];
					pxCorner[2] = bright[bright.length-width-width];
					corner=true;
				} else if (x==width-1&&y==0) {
					pxCorner[0] = bright[width-1];
					pxCorner[1] = bright[width-2];
					pxCorner[2] = bright[width-1+width];
					corner=true;
				} else if (x==width-1&&y==height-1) {
					pxCorner[0] = bright[bright.length-1];
					pxCorner[1] = bright[bright.length-2];
					pxCorner[2] = bright[bright.length-1-width];
					corner=true;
				} else if (x==0) {
					pxSide[0] = bright[i-width];
					pxSide[1] = bright[i-width+1];
					pxSide[2] = bright[i];
					pxSide[3] = bright[i+1];
					pxSide[4] = bright[i+width];
					pxSide[5] = bright[i+width+1];
					side=true;
				} else if (x==width-1) {
					pxSide[0] = bright[i-width];
					pxSide[1] = bright[i-width-1];
					pxSide[2] = bright[i];
					pxSide[3] = bright[i-1];
					pxSide[4] = bright[i+width];
					pxSide[5] = bright[i+width-1];
					side=true;
				} else if (y==0) {
					pxSide[0] = bright[i-1];
					pxSide[1] = bright[i];
					pxSide[2] = bright[i+1];
					pxSide[3] = bright[i+width-1];
					pxSide[4] = bright[i+width];
					pxSide[5] = bright[i+width+1];
					side=true;
				} else if (y==height-1) {
					pxSide[0] = bright[i-1];
					pxSide[1] = bright[i];
					pxSide[2] = bright[i+1];
					pxSide[3] = bright[i-width-1];
					pxSide[4] = bright[i-width];
					pxSide[5] = bright[i-width+1];
					side=true;
				} 
				
				if (corner) {
					Arrays.sort(pxCorner);
					img.pixels[i] = 0xff000000 | pxCorner[1]<<16 | pxCorner[1]<<8 | pxCorner[1];
				} else if (side) {
					Arrays.sort(pxSide);
					img.pixels[i] = 0xff000000 | pxSide[2]<<16 | pxSide[2]<<8 | pxSide[2];
				} else {		
					px[0] = bright[i-width-1];
					px[1] = bright[i-width];
					px[2] = bright[i-width+1];
					px[3] = bright[i-1];
					px[4] = bright[i];
					px[5] = bright[i+1];
					px[6] = bright[i+width-1];
					px[7] = bright[i+width];
					px[8] = bright[i+width+1];
					Arrays.sort(px);
					img.pixels[i] = 0xff000000 | px[4]<<16 | px[4]<<8 | px[4];
				}
				
				i++;
			}
			//i+=2;
		}
		img.updatePixels();
		
	}
}
