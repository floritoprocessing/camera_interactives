import processing.video.*;


//import processing.opengl.*;

Webcam webcam;
FlowField flowField;
Mosaic mosaic;

boolean SHOW_FLOWFIELD = false;
int FLOWFIELD_AVERAGE_PASSES = 4;//5;
float FLOWFIELD_THRESHOLD = 4.0;//5;

float avWebcamGetTime = 0;
float avFlowFieldUpdateTime = 0;
float avFlowFieldAveragingTime = 0;
float avFlowFieldThresholdTime = 0;
float avFlowFieldDrawTime = 0;

float avFac = 0.95, avFac1=1.0-avFac;

void setup() {
  size(800, 600, P3D);
  noSmooth();
  frameRate(10000);

  webcam = new Webcam(this);
  flowField = new FlowField(this, webcam, 4);
  mosaic = new Mosaic(this, webcam, flowField);
}


void keyPressed() {
  if (key=='f') { 
    SHOW_FLOWFIELD = !SHOW_FLOWFIELD; 
    avFlowFieldDrawTime=0;
  } else if (key==CODED) {
    if (keyCode==RIGHT) FLOWFIELD_AVERAGE_PASSES++;
    if (keyCode==LEFT&&FLOWFIELD_AVERAGE_PASSES>=1) FLOWFIELD_AVERAGE_PASSES--;
    if (keyCode==UP) FLOWFIELD_THRESHOLD+=0.1;
    if (keyCode==DOWN&&FLOWFIELD_THRESHOLD>=0.1) FLOWFIELD_THRESHOLD-=0.1;
  }
}

void draw() {
  background(0);

  smooth();
  frameRate(25);




  // GET WEBCAM
  avWebcamGetTime = avFac*avWebcamGetTime + avFac1*webcam.updateCameraImage();

  // UPDATE FLOWFIELD
  avFlowFieldUpdateTime = avFac*avFlowFieldUpdateTime + avFac1*flowField.update();

  // AVERAGE FLOWFIELD
  float tmp=0;
  for (int i=0; i<FLOWFIELD_AVERAGE_PASSES; i++) 
    tmp += flowField.average();
  avFlowFieldAveragingTime = avFac*avFlowFieldAveragingTime + avFac1*tmp;

  // THRESHOLD FLOWFIELD
  //avFlowFieldThresholdTime = avFac*avFlowFieldThresholdTime + avFac1*flowField.threshold(FLOWFIELD_THRESHOLD);

  // MODULATE MOSAIC BY FLOWFIELD
  mosaic.modulate();

  // SHOW WEBCAM OR BACKGROUND
  mosaic.draw();

  // SHOW FLOWFIELD
  if (SHOW_FLOWFIELD) {
    
    avFlowFieldDrawTime = avFac*avFlowFieldDrawTime + avFac1*flowField.draw(0, 0, 800/320.0, 800/320.0, 1.0);
 
  } else
    avFlowFieldDrawTime = avFac*avFlowFieldDrawTime;




  // SHOW STATS
  print("frame");
  print("\twebcamGet");
  print("\tflowFieldMake");
  print("\tflowFieldAverage["+FLOWFIELD_AVERAGE_PASSES+"]");
  print("\tflowFieldThreshold["+nf(FLOWFIELD_THRESHOLD, 1, 2)+"]");
  println("\tflowFieldDraw["+(SHOW_FLOWFIELD?"on":"off")+"]");
  print(nf(frameCount, 5));
  print("\t"+nf(avWebcamGetTime, 1, 2)+" ms");
  print("\t"+nf(avFlowFieldUpdateTime, 1, 2)+" ms");
  print("\t\t"+nf(avFlowFieldAveragingTime, 1, 2)+" ms");
  print("\t\t"+nf(avFlowFieldThresholdTime, 1, 2)+" ms");//);
  println("\t\t"+nf(avFlowFieldDrawTime, 1, 2)+" ms");
}
