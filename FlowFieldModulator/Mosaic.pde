class Mosaic {
  
  PApplet pa;
  Webcam wc;
  FlowField ff;
  Point[][] polyVertex, texVertex;
  int width,height;
  
  
  Mosaic(PApplet pa, Webcam wc, FlowField ff) {
    //this.pa=pa;
    this.wc=wc;
    this.ff=ff;
    width=ff.width;
    height=ff.height;
    polyVertex = new Point[width+1][height+1];
    texVertex = new Point[width+1][height+1];
    
    for (int x=0;x<=width;x++) for (int y=0;y<=height;y++) {
      polyVertex[x][y] = new Point((float)pa.width*x/width,(float)pa.height*y/height);
      texVertex[x][y] = new Point((float)x/width,(float)y/height);
    }
  }
  
  // modulate all except border
  void modulate() {
    for (int x=1;x<width;x++) for (int y=1;y<height;y++) {
      polyVertex[x][y].addMovement(ff);
      polyVertex[x][y].move();
    }
  }
  
  void draw() {
    fill(255,255,255);
    stroke(128,128,255,64);
    noStroke();
    textureMode(NORMAL);
    beginShape(TRIANGLES);
    texture(wc.camImage);
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
      vertex(polyVertex[x][y].x-.8,polyVertex[x][y].y-.5,texVertex[x][y].x,texVertex[x][y].y);
      vertex(polyVertex[x+1][y].x+.5,polyVertex[x+1][y].y-.5,texVertex[x+1][y].x,texVertex[x+1][y].y);
      vertex(polyVertex[x+1][y+1].x+.5,polyVertex[x+1][y+1].y+.8,texVertex[x+1][y+1].x,texVertex[x+1][y+1].y);
      vertex(polyVertex[x+1][y+1].x+.8,polyVertex[x+1][y+1].y+.5,texVertex[x+1][y+1].x,texVertex[x+1][y+1].y);
      vertex(polyVertex[x][y+1].x-.5,polyVertex[x][y+1].y+.5,texVertex[x][y+1].x,texVertex[x][y+1].y);
      vertex(polyVertex[x][y].x-.5,polyVertex[x][y].y-.8,texVertex[x][y].x,texVertex[x][y].y);
    }
    endShape();
  }
  
}
