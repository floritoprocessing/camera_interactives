class Webcam {

  int width=320; //320
  int height=240;//240
  Capture cap;

  int[] cameraImage = new int[width*height];
  PImage camImage = new PImage(width,height);

  Webcam(PApplet pa) {
    cap = new Capture(pa,width,height);
    cap.start();//width,height);
    //println("New webcam created!");
    //println("Myron " + myron.version()); 
    //println("Forced Dimensions of webcam: " + myron.getForcedWidth() + " " + myron.getForcedHeight()); 
  }

  //void settings() {
  //  cap.settings();
  //}

  int updateCameraImage() {
    int ti=millis();
    if (cap.available()) {
      cap.read();
      cap.loadPixels();
      camImage.pixels = cap.pixels;//myron.cameraImage();
    camImage.updatePixels();
    }
    
    return (millis()-ti);
  }

  
  int getCamera(int x, int y) {
    //return cameraImage[y*width+x];
    return camImage.pixels[y*width+x];
  }
  
  
  PImage getCameraImage() {
    return camImage;
    //for (int i=0;i<width*height;i++)
    //  out.pixels[i]=cameraImage[i];
    //out.updatePixels();
    //return out;
  }
  
  

}
