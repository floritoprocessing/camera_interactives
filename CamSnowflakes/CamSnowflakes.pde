//import processing.opengl.*;
import java.util.Vector;

int MAINWIDTH=640;
int MAINHEIGHT=480;

int SNOWFLAKES_MAX_NR=2000;
boolean SHOWFLAKES_SHOW=true;

boolean WEBCAM_SHOW_INPUT=true;
boolean WEBCAM_SHOW_BUFFER=false;
boolean WEBCAM_SHOW_DIFFERENCE=false;

int WEBCAM_WIDTH=200;
int WEBCAM_HEIGHT=150;
int WEBCAM_FPS=25;
int WEBCAM_THRESHOLD=15;

Vector snowflakes;
Snowflake snowflake;
WebCam webcam;


void setup() {
  size(640,480);//,P2D);//,P3D);
  snowflakes=new Vector();
  webcam=new WebCam(this,MAINWIDTH,MAINHEIGHT,WEBCAM_WIDTH,WEBCAM_HEIGHT,WEBCAM_FPS);
  webcam.setThreshold(WEBCAM_THRESHOLD);
  background(0,0,0);
}

void draw() {
  background(0,0,0);
  
  webcam.update();
  
  if (WEBCAM_SHOW_INPUT) webcam.show();
  if (WEBCAM_SHOW_BUFFER) webcam.showBuffer();
  if (WEBCAM_SHOW_DIFFERENCE) webcam.showDifference();
  if (keyPressed&&key==' ') webcam.makeBuffer();
  
  // ADD FLAKES
  int nrFlakes=snowflakes.size();
  if (nrFlakes<SNOWFLAKES_MAX_NR) {
    for (int i=0;i<25;i++) {
      snowflake=new Snowflake(width,height);
      snowflakes.add(snowflake);
      nrFlakes++;
    }
  }
  
  // DRAW FLAKES
  fill(255);
  noStroke();
  ellipseMode(CENTER);
  for (int i=0;i<nrFlakes;i++) {
    snowflake=(Snowflake)(snowflakes.elementAt(i));
    snowflake.updateFrom(webcam);
    snowflake.toScreen();
  }
}

void keyPressed() {
  if (key=='b') {
    WEBCAM_SHOW_BUFFER=!WEBCAM_SHOW_BUFFER;
    WEBCAM_SHOW_INPUT=false;
    WEBCAM_SHOW_DIFFERENCE=false;
  }
  if (key=='c') {
    WEBCAM_SHOW_BUFFER=false;
    WEBCAM_SHOW_INPUT=!WEBCAM_SHOW_INPUT;
    WEBCAM_SHOW_DIFFERENCE=false;
  }
  if (key=='d') {
    WEBCAM_SHOW_BUFFER=false;
    WEBCAM_SHOW_INPUT=false;
    WEBCAM_SHOW_DIFFERENCE=!WEBCAM_SHOW_DIFFERENCE;
  }
  if (key=='s') SHOWFLAKES_SHOW=!SHOWFLAKES_SHOW;
}
