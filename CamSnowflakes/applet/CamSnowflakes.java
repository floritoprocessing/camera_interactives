import processing.core.*; import processing.video.*; import java.applet.*; import java.awt.*; import java.awt.image.*; import java.awt.event.*; import java.io.*; import java.net.*; import java.text.*; import java.util.*; import java.util.zip.*; public class CamSnowflakes extends PApplet {Vector snowflakes;
Snowflake snowflake;
int SNOWFLAKES_MAX_NR=2000;
boolean SHOWFLAKES_SHOW=true;

WebCam webcam;
boolean WEBCAM_SHOW_INPUT=false;
boolean WEBCAM_SHOW_BUFFER=false;
boolean WEBCAM_SHOW_DIFFERENCE=false;

public void setup() {
  size(640,480,P3D);
  snowflakes=new Vector();
  webcam=new WebCam(this,640,480,320,240,25);
}

public void draw() {
  background(0,0,0);
  
  webcam.update();
  if (WEBCAM_SHOW_INPUT) webcam.show();
  if (WEBCAM_SHOW_BUFFER) webcam.showBuffer();
  if (WEBCAM_SHOW_DIFFERENCE) webcam.showDifference();
  if (keyPressed&&key==' ') webcam.makeBuffer();
  
  // ADD FLAKES
  int nrFlakes=snowflakes.size();
  if (nrFlakes<SNOWFLAKES_MAX_NR) {
    for (int i=0;i<50;i++) {
      snowflake=new Snowflake(width,height);
      snowflakes.add(snowflake);
      nrFlakes++;
    }
  }
  
  // DRAW FLAKES
  fill(255);
  noStroke();
  for (int i=0;i<nrFlakes;i++) {
    snowflake=(Snowflake)(snowflakes.elementAt(i));
    snowflake.updateFrom(webcam);
    snowflake.toScreen();
  }
}

public void keyPressed() {
  if (key=='b') {
    WEBCAM_SHOW_BUFFER=!WEBCAM_SHOW_BUFFER;
    WEBCAM_SHOW_INPUT=false;
    WEBCAM_SHOW_DIFFERENCE=false;
  }
  if (key=='c') {
    WEBCAM_SHOW_BUFFER=false;
    WEBCAM_SHOW_INPUT=!WEBCAM_SHOW_INPUT;
    WEBCAM_SHOW_DIFFERENCE=false;
  }
  if (key=='d') {
    WEBCAM_SHOW_BUFFER=false;
    WEBCAM_SHOW_INPUT=false;
    WEBCAM_SHOW_DIFFERENCE=!WEBCAM_SHOW_DIFFERENCE;
  }
  if (key=='s') SHOWFLAKES_SHOW=!SHOWFLAKES_SHOW;
}
class Snowflake {
  float x=0, y=0;
  float xm=0, ym=0;
  int wid, hei;
  float radius;
  float speedDown, speedSide=0;
  float moveFac=1.0f;
  
  Snowflake(int w, int h) {
    wid=w;
    hei=h;
    reset(w);
  }
  
  public void reset(float w) {
    x=-0.1f*w+1.2f*random(w);
    y=-10;
    radius=0.8f*random(1.0f,3.0f);
    speedDown=2.0f*random(0.8f,1.2f);
  }
  
  public void updateFrom(WebCam wc) {
    if (x>=0&&x<wid&&y>=0&&y<hei) {
      if (wc.isDifferentAt(x/PApplet.toFloat(wid),y/PApplet.toFloat(hei))) moveFac=0; else moveFac=1;
    }
    update();
  }
  
  public void update() {
    ym=(3*ym+moveFac*radius*speedDown)/4.0f;
    y+=ym;
    if (y>height+radius) reset(width);
    
    speedSide=0.5f*(noise(0.5f*x/(float)width,0.5f*y/PApplet.toFloat(width),millis()/1000.0f)-0.5f);
    xm+=moveFac*speedSide*radius;
    xm*=0.9f;
    x+=xm;    
  }
  
  public void toScreen() {
    ellipseMode(CENTER_RADIUS);
    ellipse(x,y,radius,radius);
  }
}

 
 
class WebCam {
  Capture capture;
  int wid, hei;
  int camWid, camHei;
  int fps;
  int DIFF_THRESHOLD=25;
  PImage buffer, difference;
  
  WebCam(PApplet pa, int _w, int _h, int _wc, int _hc, int _fps) {
    wid=_w;
    hei=_h;
    camWid=_wc;
    camHei=_hc;
    fps=_fps;
    capture=new Capture(pa,camWid,camHei,fps);
    buffer=new PImage(camWid,camHei);
    difference=new PImage(camWid,camHei);
  }
  
  public void update() {
    capture.read();
    //capture.loadPixels();
  }
  
  public void makeBuffer() {
    println("pixels to buffer: "+capture.pixels.length);
    for (int i=0;i<capture.pixels.length;i++) {
      buffer.pixels[i]=capture.pixels[i];
    }
    buffer.updatePixels();
  }
  
  public void show() {  
    image(capture,0,0,wid,hei);
  }
  
  public void showBuffer() {
    image(buffer,0,0,wid,hei);
  }
  
  public void showDifference() {
    //capture.loadPixels();
    //buffer.loadPixels();
    for (int i=0;i<capture.pixels.length;i++) {
      float rr=abs(red(buffer.pixels[i])-red(capture.pixels[i]));
      float gg=abs(green(buffer.pixels[i])-green(capture.pixels[i]));
      float bb=abs(blue(buffer.pixels[i])-blue(capture.pixels[i]));
      float br=(rr+gg+bb)/3.0f;
      if (br<DIFF_THRESHOLD) br=0;
      difference.pixels[i]=color(br);
    }
    difference.updatePixels();
    image(difference,0,0,wid,hei);
  }
  
  public boolean isDifferentAt(float x, float y) {
    int bx=PApplet.toInt(x*camWid);
    int by=PApplet.toInt(y*camHei);
    int i=by*camWid+bx;
  
    float rr=abs(red(buffer.pixels[i])-red(capture.pixels[i]));
    float gg=abs(green(buffer.pixels[i])-green(capture.pixels[i]));
    float bb=abs(blue(buffer.pixels[i])-blue(capture.pixels[i]));
    float br=(rr+gg+bb)/3.0f;
    boolean out=true;
    if (br<DIFF_THRESHOLD) out=false;
    return out;
  }
}
static public void main(String args[]) {   PApplet.main(new String[] { "CamSnowflakes" });}}