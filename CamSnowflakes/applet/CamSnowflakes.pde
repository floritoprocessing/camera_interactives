Vector snowflakes;
Snowflake snowflake;
int SNOWFLAKES_MAX_NR=2000;
boolean SHOWFLAKES_SHOW=true;

WebCam webcam;
boolean WEBCAM_SHOW_INPUT=false;
boolean WEBCAM_SHOW_BUFFER=false;
boolean WEBCAM_SHOW_DIFFERENCE=false;

void setup() {
  size(640,480,P3D);
  snowflakes=new Vector();
  webcam=new WebCam(this,640,480,320,240,25);
}

void draw() {
  background(0,0,0);
  
  webcam.update();
  if (WEBCAM_SHOW_INPUT) webcam.show();
  if (WEBCAM_SHOW_BUFFER) webcam.showBuffer();
  if (WEBCAM_SHOW_DIFFERENCE) webcam.showDifference();
  if (keyPressed&&key==' ') webcam.makeBuffer();
  
  // ADD FLAKES
  int nrFlakes=snowflakes.size();
  if (nrFlakes<SNOWFLAKES_MAX_NR) {
    for (int i=0;i<50;i++) {
      snowflake=new Snowflake(width,height);
      snowflakes.add(snowflake);
      nrFlakes++;
    }
  }
  
  // DRAW FLAKES
  fill(255);
  noStroke();
  for (int i=0;i<nrFlakes;i++) {
    snowflake=(Snowflake)(snowflakes.elementAt(i));
    snowflake.updateFrom(webcam);
    snowflake.toScreen();
  }
}

void keyPressed() {
  if (key=='b') {
    WEBCAM_SHOW_BUFFER=!WEBCAM_SHOW_BUFFER;
    WEBCAM_SHOW_INPUT=false;
    WEBCAM_SHOW_DIFFERENCE=false;
  }
  if (key=='c') {
    WEBCAM_SHOW_BUFFER=false;
    WEBCAM_SHOW_INPUT=!WEBCAM_SHOW_INPUT;
    WEBCAM_SHOW_DIFFERENCE=false;
  }
  if (key=='d') {
    WEBCAM_SHOW_BUFFER=false;
    WEBCAM_SHOW_INPUT=false;
    WEBCAM_SHOW_DIFFERENCE=!WEBCAM_SHOW_DIFFERENCE;
  }
  if (key=='s') SHOWFLAKES_SHOW=!SHOWFLAKES_SHOW;
}
