class Snowflake {
  float x=0, y=0;
  float xm=0, ym=0;
  int wid, hei;
  float radius;
  float speedDown, speedSide=0;
  float moveFac=1.0;
  
  Snowflake(int w, int h) {
    wid=w;
    hei=h;
    reset(w);
  }
  
  void reset(float w) {
    x=-0.1*w+1.2*random(w);
    y=-10;
    radius=0.8*random(1.0,3.0);
    speedDown=2.0*random(0.8,1.2);
  }
  
  void updateFrom(WebCam wc) {
    if (x>=0&&x<wid&&y>=0&&y<hei) {
      if (wc.isDifferentAt(x/float(wid),y/float(hei))) moveFac=0; else moveFac=1;
    }
    update();
  }
  
  void update() {
    ym=(3*ym+moveFac*radius*speedDown)/4.0;
    y+=ym;
    if (y>height+radius) reset(width);
    
    speedSide=0.5*(noise(0.5*x/(float)width,0.5*y/float(width),millis()/1000.0)-0.5);
    xm+=moveFac*speedSide*radius;
    xm*=0.9;
    x+=xm;    
  }
  
  void toScreen() {
    ellipseMode(CENTER_RADIUS);
    ellipse(x,y,radius,radius);
  }
}
