import processing.video.*; 
 
class WebCam {
  Capture capture;
  int wid, hei;
  int camWid, camHei;
  int fps;
  int DIFF_THRESHOLD=25;
  PImage buffer, difference;
  
  WebCam(PApplet pa, int _w, int _h, int _wc, int _hc, int _fps) {
    wid=_w;
    hei=_h;
    camWid=_wc;
    camHei=_hc;
    fps=_fps;
    capture=new Capture(pa,camWid,camHei,fps);
    buffer=new PImage(camWid,camHei);
    difference=new PImage(camWid,camHei);
  }
  
  void update() {
    capture.read();
    //capture.loadPixels();
  }
  
  void makeBuffer() {
    println("pixels to buffer: "+capture.pixels.length);
    for (int i=0;i<capture.pixels.length;i++) {
      buffer.pixels[i]=capture.pixels[i];
    }
    buffer.updatePixels();
  }
  
  void show() {  
    image(capture,0,0,wid,hei);
  }
  
  void showBuffer() {
    image(buffer,0,0,wid,hei);
  }
  
  void showDifference() {
    //capture.loadPixels();
    //buffer.loadPixels();
    for (int i=0;i<capture.pixels.length;i++) {
      float rr=abs(red(buffer.pixels[i])-red(capture.pixels[i]));
      float gg=abs(green(buffer.pixels[i])-green(capture.pixels[i]));
      float bb=abs(blue(buffer.pixels[i])-blue(capture.pixels[i]));
      float br=(rr+gg+bb)/3.0;
      if (br<DIFF_THRESHOLD) br=0;
      difference.pixels[i]=color(br);
    }
    difference.updatePixels();
    image(difference,0,0,wid,hei);
  }
  
  boolean isDifferentAt(float x, float y) {
    int bx=int(x*camWid);
    int by=int(y*camHei);
    int i=by*camWid+bx;
  
    float rr=abs(red(buffer.pixels[i])-red(capture.pixels[i]));
    float gg=abs(green(buffer.pixels[i])-green(capture.pixels[i]));
    float bb=abs(blue(buffer.pixels[i])-blue(capture.pixels[i]));
    float br=(rr+gg+bb)/3.0;
    boolean out=true;
    if (br<DIFF_THRESHOLD) out=false;
    return out;
  }
}
