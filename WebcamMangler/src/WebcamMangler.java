import java.awt.Point;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;

import processing.core.PApplet;
import processing.core.PImage;
import sun.java2d.loops.DrawLine;
import de.humatic.dsj.DSCapture;
import de.humatic.dsj.DSCapture.CaptureDevice;
import de.humatic.dsj.DSFilter;
import de.humatic.dsj.DSFilter.DSPin;
import de.humatic.dsj.DSFilterInfo;
import de.humatic.dsj.DSFiltergraph;
import de.humatic.dsj.DSJUtils;
import de.humatic.dsj.DSSampleBuffer;


public class WebcamMangler extends PApplet implements PropertyChangeListener {

	DSCapture capture;
	
	CaptureDevice cdev;
	DSPin captureOut;
	DSSampleBuffer dssb;
	
	PImage capImage;
	PImage drawImage;
	
	boolean feedback = false;
	BufferedImage bimg;
	
	Vector<Point> drawPoints = new Vector<Point>();
	Vector<Point> drawDirection = new Vector<Point>();
	Vector<Integer> drawColor = new Vector<Integer>();
	Vector<Integer> drawLife = new Vector<Integer>();
	Vector<Integer> drawLongivety = new Vector<Integer>();
	
	public void setup() {
		size(640,480);
		registerDispose(this);
		
		DSFilterInfo[][] dsi = DSCapture.queryDevices();
		for (int i=0;i<dsi.length;i++) {
			println(i+" -------------------------------------------- ");
			println(dsi[i]);
		}
		
		println(dsi[0].length);
		capture = new DSCapture(DSFiltergraph.DD7, dsi[0][0], false, DSFilterInfo.doNotRender(), this);
		
		cdev= capture.getActiveVideoDevice();
		//captureOut = cdev.getDeviceOutput(DSCapture.CaptureDevice.PIN_CATEGORY_CAPTURE);
		DSFilter[] filters = capture.listFilters();
		println(filters);
	    //dssb = capture.insertSampleAccessFilter(filters[0], null, filters[1], 0);//DSSampleBuffer.DIRECT_ACCESS);
	    
	    
		
		capImage = new PImage(capture.getDisplaySize().width, capture.getDisplaySize().height);
		drawImage = new PImage(capImage.width, capImage.height);
		println(capImage.width+"x"+capImage.height);
		
		textFont(createFont("Verdana Bold",12));
	}
		
	public void draw() {
		//println(capture.getState());
		/*float fps = cdev.getFrameRate(captureOut);
		println(fps);*/
		//println(dssb.getDeltaTime());
		
		if (!feedback) {
			//int dataSize = capture.getDataSize();
			//println(dataSize);
			//println(capture.getFrameDropInfo());
			//capture.getActiveVideoDevice().
			bimg = capture.getImage();
			bimg.getRGB(0, 0, capImage.width, capImage.height, capImage.pixels, 0, capImage.width);
		} else {
			loadPixels();
			System.arraycopy(pixels, 0, capImage.pixels, 0, pixels.length);
		}
		capImage.updatePixels();
		
		//image(capImage,0,0);
		
		if (frameRate>30)
		for (int i=0;i<500;i++) {
			int x = (int)(Math.random()*capImage.width);
			int y = (int)(Math.random()*capImage.height);
			Point point = new Point(x, y);
			Integer color = capImage.get(x, y);
			int dr = (int)(Math.random()*4);
			Point dir = null;
			if (dr==0) dir = new Point(1,0);
			else if (dr==1) dir = new Point(0,1);
			else if (dr==2) dir = new Point(-1,0);
			else if (dr==3) dir = new Point(0,-1);
			
			Integer life = (int)(Math.random()*Math.max(capImage.width,capImage.height)*0.5);
			drawPoints.add(point);
			drawColor.add(color);
			drawDirection.add(dir);
			drawLongivety.add(life);
			drawLife.add(life);
		}
		
		for (int i=0;i<drawPoints.size();i++) {
			int x = drawPoints.get(i).x;
			int y = drawPoints.get(i).y;
			float opacity = (float)drawLife.get(i) / (float)drawLongivety.get(i);
			float invOpacity = 1-opacity;
			
			int dc = drawColor.get(i)&0xffffff;
			int bc = drawImage.get(x, y);
			int red = (int)(opacity*red(dc) + invOpacity*red(bc));
			int green = (int)(opacity*green(dc) + invOpacity*green(bc));
			int blue = (int)(opacity*blue(dc) + invOpacity*blue(bc));
			int color = red<<16 | green<<8 | blue;
			
			drawPoints.get(i).x += drawDirection.get(i).x;
			drawPoints.get(i).y += drawDirection.get(i).y;
			int life = drawLife.remove(i);
			life--;
			drawLife.add(i, life);
			if (drawPoints.get(i).x<0 || drawPoints.get(i).x>=capImage.width || 
					drawPoints.get(i).y<0 || drawPoints.get(i).y>=capImage.height ||
							drawLife.get(i)<=0) {
				drawPoints.remove(i);
				drawColor.remove(i);
				drawDirection.remove(i);
				drawLongivety.remove(i);
				drawLife.remove(i);
				i--;
			}
			drawImage.set(x, y, color);
		}
		
		image(drawImage,0,0);
		image(capImage,capImage.width-10-capImage.width*0.1f,10,capImage.width*0.1f,capImage.height*0.1f);
		
		fill(255);
		text("fps="+nf(frameRate,1,2),10,20);
		text("points="+drawPoints.size(),10,35);
		text("[f]eedback="+feedback,10,50);
		
	}
	
	public void keyPressed() {
		if (key=='f') feedback=!feedback;
	}
	
	public void dispose() {
		println("end");
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"WebcamMangler"});
	}

	public void propertyChange(PropertyChangeEvent arg0) {
		//println(arg0);
		//println(DSJUtils.getEventType(arg0));
	}

}
