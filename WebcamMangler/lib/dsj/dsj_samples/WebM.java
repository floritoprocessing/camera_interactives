/**
dsj demo code.
You may use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
copyright 2010
N.Peters
humatic GmbH
Berlin, Germany
**/

import de.humatic.dsj.*;
import de.humatic.dsj.sink.JSink;
import de.humatic.dsj.sink.MKVSink;
import de.humatic.dsj.sink.FileSink;
import de.humatic.dsj.src.MKVSource;

import de.humatic.vpj.*;

/**
Demo for WebM support in dsj 0_8_62. Before trying to run this, download and install the WebM DirectShow filters from
http://www.webmproject.org/tools and the Vorbis filters from http://xiph.org/dshow/. Also make sure you have a general purpose Vorbis
decoder installed (ffdshow will do) as Xiph's decoder only works with ogg streams (their encoder is more widely useable).
You may also want to download and install the vpj extension to dsj for the last part of this demo.
There are no 64bit versions of the WebM filters so far, until this changes this is 32bit only!
**/

public class WebM implements java.beans.PropertyChangeListener {

	private DSFiltergraph graph;

	private java.awt.FileDialog fd;

	private int mode = 1;

	public WebM() {}

	public void createGraph() throws Exception {

		javax.swing.JFrame f = new javax.swing.JFrame("dsj - WebM support");

		switch (mode) {

			case 0:

				/** play a WebM file using the WebM DirectShow filters **/

				fd = new java.awt.FileDialog(f, "select a WebM file", java.awt.FileDialog.LOAD);

				fd.setVisible(true);

				if (fd.getFile() == null) return;

				graph = new DSMovie(fd.getDirectory()+fd.getFile(), DSFiltergraph.DD7, this);

				break;

			case 1:

				/** play a WebM stream using dsj's MKVSource (some of these will not work with the WebM project's source filter so far) **/

				String[] paths = new String[]{ "http://www.opera.com/media/video/1060/introduction.webm",
											   "http://people.opera.com/patrickl/experiments/webm/videos/fridge.webm", // some Opera demo
											   "http://"+java.net.InetAddress.getLocalHost().getHostAddress()+":8080/video.webm" // the stream resulting from mode 3 of this demo
											   "http://195.10.10.75:8800/live.webm",  // Flumotion livestream demo (if up)
											   "http://184.72.239.149/webm/webmDemo/playlist.webm?"+String.valueOf(System.currentTimeMillis()) // Wowza Streaming Server sample
											   };

				MKVSource src = new MKVSource(new java.net.URL(paths[0]), 0, this);

				graph = src.createGraph(de.humatic.dsj.DSFiltergraph.DD7);

				break;

			case 2:

				/** Transcode some file to VP8 / Vorbis in a WebM (Matroska) container. FileSink.WEBM stands for a predined
				FileSink using the WebM VP8Encoder, Xiph Vorbis Encoder and WebM Muxer filters. You can define your own FileSink in
				case you prefer other filters. **/

				fd = new java.awt.FileDialog(f, "select a non-WebM file", java.awt.FileDialog.LOAD);

				fd.setVisible(true);

				if (fd.getFile() == null) return;

				graph = new DSMovie(fd.getDirectory()+fd.getFile(), DSFiltergraph.DD7, this);

				FileSink fs = FileSink.forType(FileSink.WEBM, "test.webm");

				graph.connectSink(fs);

				/* Leave our tag in the file. Outcomment if you did not install the vpj extension.*/

				VPJLink mlink = new VPJLink(graph);

				final WebmMux mux = mlink.getMultiplexer();

				mux.setWritingApp("WebM.java");

				break;

			case 3:

				/**
				Open a capture device and create a simple http server on port 8080. You can use another
				instance of this demo (with "mode" set to 1 and the last url selected) to play the stream or fire
				up a nightly build of Mozilla or Opera (Chrome lately was broken for livestreams due to some ffmpeg bug.
				Will probably be fixed by the time) and direct it to http://localhost:8080/video.htm.
				Make sure to not set camera framerates too high, the encoder may proove too slow.
				**/

				graph = DSCapture.fromUserDialog(f, DSFiltergraph.DD7, this);

				MKVSink sink = new MKVSink(graph,
										   8080,
										   DSFilterInfo.filterInfoForName("WebM VP8 Encoder Filter"),
										   DSFilterInfo.filterInfoForName("Xiph.org Vorbis Encoder"),
										   JSink.PREVIEW);

				/**
				This uses the new vpj extension to configure the encoder. With default settings - namely
				pure "auto" keyframe mode - VP8 is unusable for livestreaming (unless you really like macroblock
				mosaics or plan to shoot something with a lot of motion). The WebM project's VP8Encoder filter
				also offers a properties page for adjustments since version 0.9.10.
				**/

				VPJLink link = new VPJLink(graph);

				final VP8Encoder vp8Encoder = link.getEncoder();

				vp8Encoder.setErrorResilient(1);
				vp8Encoder.setTargetBitrate(500);
				vp8Encoder.setEndUsage(1);
				vp8Encoder.setDeadline(1);
				vp8Encoder.setKeyframeMode(-1);
				/** setting both min and max to the same value gives a constant keyframe rate **/
				vp8Encoder.setKeyframeMinInterval(20);
				vp8Encoder.setKeyframeMaxInterval(20);
				vp8Encoder.setUndershootPct(95);
				vp8Encoder.setDecoderBufferSize(6);
				vp8Encoder.setDecoderBufferInitialSize(4);
				vp8Encoder.setDecoderBufferOptimalSize(5);
				vp8Encoder.setMinQuantizer(4);
				vp8Encoder.setMinQuantizer(56);

				/** All the above methods work on the filter's cache. Make them active now.**/

				vp8Encoder.applySettings();

				break;

		}

		try{ f.getContentPane().add(java.awt.BorderLayout.CENTER, graph.asComponent()); } catch (Exception e){}

		f.getContentPane().add(java.awt.BorderLayout.SOUTH, new SwingMovieController(graph));

		f.pack();

		f.setVisible(true);

		/**
		Don't do this at home. This demo relies on dsj closing and disposing off filtergraphs when the JVM exits. This is
		OK for a "open graph, do something & exit" style demo, but real world applications should take care of calling
		dispose() on filtergraphs they're done with themselves.
		**/

		f.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

	}

	public void propertyChange(java.beans.PropertyChangeEvent pe) {

		switch (DSJUtils.getEventType(pe)) {

		}

	}

	public static void main(String[] args){

		try{ new WebM().createGraph(); } catch (Exception ex){ex.printStackTrace();}

	}

}