/**
dsj demo code.
You may use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
copyright 2008
N.Peters
humatic GmbH
Berlin, Germany
**/

import de.humatic.dsj.*;
import de.humatic.dsj.sink.*;

/**
Shows basic construction and use of FileSinks. Also see the dsj.xml file in the main download for an alternative way
to create FileSinks via XML.
**/

public class FileSinks implements java.beans.PropertyChangeListener {

	private DSFiltergraph graph;

	private static boolean exportDone;

	private int TYPE = DSFiltergraph.MOVIE,
				FORMAT = 0;

	public FileSinks() {

		if (TYPE == DSFiltergraph.CAPTURE) {

			DSFilterInfo[][] dsi = DSCapture.queryDevices(1);

			graph = new DSCapture(DSFiltergraph.DD7, dsi[0][0], false, dsi[1][0], null);

		} else {

			/** This sample uses the output of the AVCombine sample. Run that one before continueing or change the file here **/

        	graph = new DSMovie("media/mux.avi", DSFiltergraph.DD7 | DSMovie.INIT_PAUSED, this);

		}

		javax.swing.JFrame f = new javax.swing.JFrame("dsj - filesinks");

		f.getContentPane().add(java.awt.BorderLayout.CENTER, graph.asComponent());

		f.pack();

		f.setVisible(true);

		f.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		FileSink fileSink = null;

        switch (FORMAT) {

			case 0:

				/**
				This writes a Flash Video (FLV) file. There are certain requirements:
				FLV supports a limited set of video and audio codecs, same as a limited set of samplerates. The multiplexer
				used here further limits your codec choice as it only supports mp3 audio and FLV1 video. We are going to use
				ffdShow to encode the video part to FLV1 and the stock MP3 encoder for the audio part.
				The FLV Multiplexer is available from http://blog.monogram.sk/janos. If you are on Vista you will also need to
				install the LAME audio encoder, because the system does not bring an MP3 encoder.
				If your source audio has a samplerate that is not 44.1khz or an even divider thereof (the hardcoded samplefile
				is at 22.05 and thus will work), you will need to put something into the the graph to convert the samplerate
				before feeding it to the encoder.
				**/

				fileSink = new FileSink("media/flvsink.flv");

				fileSink.setVideoEncoder(DSFilterInfo.filterInfoForName("ffdshow video encoder"));

            	/**
				The MPEG Layer-3 filter is no longer available on Vista. Use LAME instead.
				(http://www.free-codecs.com/LAME_DirectShow_Filter_download.htm. Filter registration must be run as administrator)
				**/

				if (DSEnvironment.getOSVersion() < 6)
					fileSink.setAudioEncoder(DSFilterInfo.filterInfoForName("MPEG Layer-3"));
				else fileSink.setAudioEncoder(DSFilterInfo.filterInfoForName("LAME Audio Encoder"));

				fileSink.setMultiplexer(DSFilterInfo.filterInfoForName("MONOGRAM FLV Mux"));

				break;

			case 1:

				/**
				Uses a powerful commercial encoder / multiplexer combo supporting mpg 1, 2, & 4 etc. from
				http://www.standardmpeg.com
				**/

				fileSink = new FileSink("media/mpgSink.mpg");

				fileSink.setMultiplexer(DSFilterInfo.filterInfoForName("Standard MPEG Encoder v6"));

				break;

			case 2:

				/**
				Example for a FileSink writing WindowsMedia.
				**/

				fileSink = new FileSink("media/asfSink.asf");

				fileSink.setVideoEncoder(DSFilterInfo.filterInfoForSystemProfile(12));

            	fileSink.setMultiplexer(DSFilterInfo.filterInfoForName("WM ASF Writer"));

				fileSink.setFileWriter(null);

				break;

		}

		/** Make sure you set ffdShow to FLV1 for the flv sink! This can be done in code by using DSFilterInfo.filterInfoForCodecState.**/

		if (TYPE == DSFiltergraph.CAPTURE) fileSink.setFlags(Sink.DISPLAY_LOCAL | Sink.SHOW_DIALOGS);

        else fileSink.setFlags(Sink.SHOW_DIALOGS);

		int result = graph.connectSink(fileSink);

		if (TYPE == DSFiltergraph.CAPTURE) {

			/**
			Depending on the capture device and the graph built from it, a FileSink may be able to act much
			like a "normal" DSCapture recording setup. You know if that is the case by checking the return value
			from connectSink(..).
			**/

			if (result == de.humatic.dsj.sink.Sink.CONTROLABLE) {

				/** We can separately control the recording. Add a controller (which can call DSCapture.record()) **/

				f.getContentPane().add(java.awt.BorderLayout.SOUTH, new SwingMovieController(graph));

				f.pack();

			} else {

				/** The sink is not controlable and will now be recording. Break it after 10sec for this demo. **/

				try{ Thread.currentThread().sleep(10000); } catch (Exception e){}

				graph.dispose();

				System.exit(0);

    		}

		} else {

			/** Movie, wait until transcoding has finished **/

			while(!exportDone) {

				try{ Thread.currentThread().sleep(1000); } catch (Exception e){}

			}

			graph.removeSink();

			graph.dispose();

			System.exit(0);

    	}

	}

    public void propertyChange(java.beans.PropertyChangeEvent pe) {

		if (DSJUtils.getEventType(pe) == DSFiltergraph.EXPORT_PROGRESS) System.out.print(DSJUtils.getEventValue_int(pe)+"% done\r");
		if (DSJUtils.getEventType(pe) == DSFiltergraph.DONE ||
			DSJUtils.getEventType(pe) == DSFiltergraph.EXPORT_DONE ||
			DSJUtils.getEventType(pe) == DSFiltergraph.SAVE_DONE) exportDone = true;

	}

    public static void main(String args[]) {

    	FileSinks fs = new FileSinks();

	}

}

