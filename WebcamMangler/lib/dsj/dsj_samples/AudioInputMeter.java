/**
dsj demo code.
You may use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
copyright 2005-7
N.Peters
humatic GmbH
Berlin, Germany
**/

import de.humatic.dsj.*;

/**

Using the SampleAccessFilter to get at incoming audio data (for example to monitor audio input levels).


**/

public class AudioInputMeter implements java.beans.PropertyChangeListener {

	private DSFiltergraph graph;

	private java.nio.ByteBuffer bb;

	private byte[] data;

	public AudioInputMeter() {}

	public void createGraph() {

		javax.swing.JFrame f = new javax.swing.JFrame("dsj AudioInputMonitor");

		f.getContentPane().setLayout(new java.awt.BorderLayout());

		DSFilterInfo[][] dsi = DSCapture.queryDevices();

		graph = new DSCapture(DSFiltergraph.RENDER_NATIVE | DSFiltergraph.INIT_PAUSED,
							  DSFilterInfo.doNotRender(),
							  false,
							  dsi[1][0],
							  this
							  );

		f.add("North", new SwingMovieController(graph));

		int[] audioProps = graph.getAudioProperties();

		System.out.println("Samplerate: "+audioProps[0]+"\n"+
						   "SampleSize: "+audioProps[1]+"\n"+
						   "Channels: "+audioProps[2]);

		DSFilter[] filters = graph.listFilters();

		for (int i = 0; i < filters.length; i++) System.out.println(filters[i]);

		/**
		Although in a simple audio capture graph as build above, the capture filter
		will almost certainly be the first filter in the returned array from listFilters(...), a real world
		application should take better care of finding the right filters to insert
		the sample access filter in between!
		**/

		System.out.println("inserting between: "+filters[0].getName()+" & "+filters[1].getName());

		graph.insertSampleAccessFilter(filters[0], null, filters[1], 0);

		javax.swing.JPanel ctrls = new javax.swing.JPanel();

		ctrls.setLayout(new java.awt.GridLayout(0,1));

		/**
		We're going to use the CaptureDeviceControls class to set the recording level..
		**/

		for (int i = CaptureDeviceControls.MASTER_VOL; i < CaptureDeviceControls.TREBLE; i++) {

			/**
			Note: If the returned volume, pan, bass & treble controls for a device's separate inputs
			actually do something, depends entirely on the device driver.
			What usually does work is the master volume and pan controls for the entire device.
			**/

			try { ctrls.add(((DSCapture)graph).getActiveAudioDevice().getControls().getController(i, 0, true)); }catch (Exception ex){}

		}

		javax.swing.JButton toFile = new javax.swing.JButton("to file");

		toFile.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent e) {

				((DSCapture)graph).setCaptureFile("monitorTest.wav", DSFilterInfo.doNotRender(), DSFilterInfo.doNotRender(), true);

				/** setCaptureFile(..) will remove the filter, so you need to reinsert it, in case you want to keep monitoring.**/

				DSFilter[] filters = graph.listFilters();

				graph.insertSampleAccessFilter(filters[2], null, filters[3], 0);

				((DSCapture)graph).record();

			}

		});

		f.getContentPane().add("Center", ctrls);

		f.getContentPane().add("South", toFile);

		f.pack();

		f.setVisible(true);

		/**
		Don't do this at home. This demo relies on dsj closing and disposing off filtergraphs when the JVM exits. This is
		OK for a "open graph, do something & exit" style demo, but real world applications should take care of calling
		dispose() on filtergraphs they're done with themselves.
		**/

		f.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		graph.play();

	}

	public void propertyChange(java.beans.PropertyChangeEvent pe) {

		if (Integer.valueOf(pe.getNewValue().toString()).intValue() == DSFiltergraph.SAMPLE_BUFFER_FILLED) {

			DSSampleBuffer buffer = (DSSampleBuffer)(pe.getOldValue());

			int sampleLength = buffer.getSampleLength();

			byte[] data = buffer.getSample();

			System.out.println("got sample, length: "+sampleLength);

			/**
			When the sample access filter has been inserted behind a plain audio capture device the delivered data
			will be uncompressed PCM audio.
			Do whatever you like to monitor levels or process samples in other ways parallely to DirectShow.
			**/

			// just to show, we're actually receiving data:

			for (int i = 0; i < 50; i++) System.out.print(data[i]+"  ");

		}

	}

	public static void main(String[] args){

		new AudioInputMeter().createGraph();

	}

}