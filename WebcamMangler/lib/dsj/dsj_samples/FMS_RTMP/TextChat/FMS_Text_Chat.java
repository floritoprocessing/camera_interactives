/**
dsj samplecode.
You are free to use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
**/

import java.io.*;
import java.awt.*;
import java.net.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

import de.humatic.dsj.*;
import de.humatic.dsj.src.RTMPSource;
import de.humatic.dsj.src.rtmp.*;

/**
Demonstrates use of the RTMPSource for other aspects of RTMP than actual media streaming.
This has a number of requirements:
You need a running instance of FMS 3.x, which you can download from www.adobe.com. The development version (max. 10 clients) is free.
The code assumes that the server runs on the local machine, if that is not the case change the IP address in the first line
of code.
The demo will try to connect to a serverside application called "FMS_Text_Chat". This will not be present with a default FMS
installation. You need to create a folder with that name in the server's application directory and copy the main.asc
file into it.
After this, run two instances of this class and select user ID 1 with the first, 2 with the second. Click the "connect"
button to establish server connections, then enter text into the lower textfields and use the "send" button to submit it,
**/


public class FMS_Text_Chat implements ActionListener, java.beans.PropertyChangeListener {

	private javax.swing.JFrame f;

	private RTMPSource rtmpSrc;

	RTMPSource.SharedObject son;

	private JTextArea output,
					  log;
	private JTextField input;

	private int userID;

	private JButton connect,
					send;

	public FMS_Text_Chat() {}

	private RTMPSource createSource(int user) {

		try {

			String path = "rtmp://"+InetAddress.getLocalHost().getHostAddress()+"/FMS_Text_Chat";

			ConnectionParameter[] cp = new ConnectionParameter[]{new ConnectionParameter("", "user"+user, AMF.STRING, ConnectionParameter.APPEND)};

			RTMPSource.setEncoding(3);

			rtmpSrc = new de.humatic.dsj.src.RTMPSource(path, // application path
														null,   // not requesting a specific resource / stream
														cp,   // connection parameters, this goes to the ActionScript onConnect method
														RTMPSource.INTERACTIVE | RTMPSource.NO_STREAM, // want to intercept the connection process (see propertyChanged(...)) and do not need to create a Netstream
														this);

			rtmpSrc.setActionScriptHandler(this);

		} catch (Exception e) {

			e.printStackTrace();

		}

		return rtmpSrc;

	}

	void buildFrame() {

		f = new javax.swing.JFrame(" dsj - RTMP text chat");

		f.setPreferredSize(new Dimension(300, 500));

		f.setLayout(new java.awt.GridLayout(1, 0));

		f.getContentPane().add(createUserPanel(getUserID()));

		f.pack();

		f.setLocation(((int)(Toolkit.getDefaultToolkit().getScreenSize().getWidth()/ 2) - (userID == 1 ? f.getWidth() : 0)) , (int)(Toolkit.getDefaultToolkit().getScreenSize().getHeight() - f.getHeight()) / 2);

		f.setVisible(true);

		f.addWindowListener(new java.awt.event.WindowAdapter() {

			public void windowClosing(java.awt.event.WindowEvent we) {

				try{ rtmpSrc.closeSource(true); } catch (Exception e){}

				System.exit(0);

			}

		});

	}

	/** This method will be called from the server, as we registered ourselves as an ActionScriptHandler **/

	void setChatHistory(double d, String str) {

		output.setText("FMS called ActionScript method \"setChatHistory\": \nArgs:\n"+d+"\n"+str);

	}

	private JPanel createUserPanel(int id) {

		userID = id;

		JPanel userPanel = new JPanel(new BorderLayout());

		userPanel.add("North", new JLabel("User "+userID));

		JPanel txt = new JPanel(new BorderLayout());

		log = new JTextArea(7, 10);

		log.setEditable(false);

		txt.add("North", log);

		output = new JTextArea(10, 10);

		output.setEditable(false);

		txt.add("Center", new JScrollPane(output));

		input = new JTextField("message from client "+userID+"...");

		txt.add("South", input);

		userPanel.add("Center", txt);

		JPanel b = new JPanel();

		connect = new JButton("connect");

		connect.getAccessibleContext().setAccessibleName("connect");

		connect.addActionListener(this);

		send = new JButton("send");

		send.setEnabled(false);

		send.getAccessibleContext().setAccessibleName("send");

		send.addActionListener(this);

		b.add(connect);

		b.add(send);

		userPanel.add("South", b);

		return userPanel;

	}

	private int getUserID(){

		Object[] options = { "1", "2"};

		return JOptionPane.showOptionDialog(null, "Select user ID", " dsj RTMP text chat",
													JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
													null, options, options[0])+1;

	}

	public void actionPerformed(ActionEvent ae) {

		String actionName = (((JButton) ae.getSource()).getAccessibleContext().getAccessibleName());

		if (actionName.contains("connect")) {

			rtmpSrc = createSource(userID);

			connect.setEnabled(false);

			send.setEnabled(true);

		} else if (actionName.contains("send")) {

			/**
			Create an RTMPMessage with text input from user to call the serverside ActionScript method "updateChat"
			**/

			AMFType[] types = new AMFType[]{ new AMFType(AMF3.UNDEFINED, null),
											 new AMFType(AMF.STRING, "updateChat"),
											 new AMFType(AMF.NUM, new Double(userID)),
											 new AMFType(AMF.NULL, null),
											 new AMFType(AMF.STRING, input.getText())};

			RTMPMessage msg = new RTMPMessage(RTMP.FLEX_MSG, types);

			try{ rtmpSrc.sendRTMP(msg); } catch (Exception e){}

		}

	}

	public void propertyChange(java.beans.PropertyChangeEvent pe) {

		try {

			switch (DSJUtils.getEventType(pe)) {

				case DSFiltergraph.SOURCE_STATE_CHANGED:

					int srcState = DSJUtils.getEventValue_int(pe);

					System.out.println("Source State changed: " + srcState);

					break;

				case DSFiltergraph.RTMP_STATE_CHANGED:

					int rtmpState = DSJUtils.getEventValue_int(pe);

					System.out.println("RTMP_STATE_CHANGED: "+rtmpState);

					if (rtmpState == RTMPSource.RS_CREATE_STREAM) {

						/** Create or connect to a serverside shared object **/

						RTMPSource.SharedObject son = rtmpSrc.getSharedObject("ChatUsers", "", 0);

						rtmpSrc.connectionIntercept(rtmpState, son);

					}

					break;

				case DSFiltergraph.RTMP_EVENT:

					System.out.println("\nRTMP event");

					try {

						String[] kv = (String[]) (pe.getOldValue());

						StringBuffer sb = new StringBuffer();

						for (int i = 0; i < kv.length; i += 2) {

							System.out.println(kv[i] + ": " + kv[i + 1]);

							sb.append(kv[i] + ": " + kv[i + 1]+"\n");

						}

						log.setText(sb.toString());

					} catch (Exception e) { }

					System.out.println("_______________________________");

					break;

				case DSFiltergraph.RTMP_SO_CHANGED:

					/** Whenever a client sends text to the server app, the shared object will be updated and
					all clients will be notified.**/

					RTMPSource.SharedObject so = (RTMPSource.SharedObject)(pe.getOldValue());

					StringBuffer sb = new StringBuffer();

					sb.append("SharedObject changed: "+so.getName()+"\n");

					sb.append("version: "+so.getVersion()+"\n");

					sb.append("subtype: "+so.getSubType()+"\n");

					String[] kv = so.getData();

					for (int i = 0; i < kv.length; i += 2) {

						sb.append(kv[i] + ": " + kv[i + 1]+"\n");

					}

					log.setText(sb.toString());

					switch (so.getSubType()) {

						case RTMPSource.SharedObject.SERVER_SEND_MESSAGE:

							if (kv[0].equalsIgnoreCase("chatUpdated")) {

								output.setText(output.getText()+kv[1]);

								output.scrollRectToVisible(new java.awt.Rectangle(0, output.getHeight()-50, 10, 50));

							}

							break;

					}

					break;

				}

			} catch (Exception e) {
		}

	}

	public static void main(String[] args) {

		new FMS_Text_Chat().buildFrame();

	}

}
