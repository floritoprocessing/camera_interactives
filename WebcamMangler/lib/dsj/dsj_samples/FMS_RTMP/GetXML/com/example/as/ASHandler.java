package com.example.as;

public class ASHandler {

	/**
	Sample ActionScript handler class. To have methods in this class called from the RTMPSource construct an
	instance of the handler and pass it to RTMPSource.setActionScriptHandler(Object t). Whenever FMS invokes
	a method via RTMP, dsj will reflect into the set handler class, trying to find the appropriate method implementation
	and call it. If no method implementation is found, the originating FMS method call is passed to
	PropertyChangeListeners as a key-value pair String[] in RTMP_EVENT events.
	**/

	private javax.swing.JTextArea textArea;

	public ASHandler(javax.swing.JTextArea ta){ textArea = ta; }

	public void traceFromServer(double val, String xml) {

		textArea.setText("Method \"traceFromServer\" called in ActionScriptHandler:\ndouble argument: "+val+"\nString argument: "+xml);

	}

}