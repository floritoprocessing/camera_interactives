/**
dsj samplecode,
copyright humatic, np+=9.
You are free to use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
**/

import java.io.*;
import java.awt.*;
import java.net.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

import de.humatic.dsj.*;
import de.humatic.dsj.src.RTMPSource;
import de.humatic.dsj.src.rtmp.*;

import com.example.as.*;

/**
This sample reproduces the client side of a Flex / FMS sample coming with the "FMS Feature Explorer" application, available from
http://www.adobe.com/devnet/flashmediaserver/articles/fmis_feature_explorer.html.
Running it requires a local FMS 3x installation, with the applications coming with the explorer installed. Refer to the above URL
for more information.
**/

public class RTMP_GetXML implements ActionListener, java.beans.PropertyChangeListener {

	private de.humatic.dsj.DSGraph graph;
	private de.humatic.dsj.src.RTMPSource netSrc;
	private javax.swing.JFrame f;
	private JPanel screen;
	private String path;
	private boolean buffered;

	private JTextField input;

	private JTextArea ta;

	private RTMPSource.SharedObject so;

	public RTMP_GetXML() {}

	void createGraph() {

		try {

			path = "rtmp://"+InetAddress.getLocalHost().getHostAddress()+"/LoadXML";

			String res = "";

			RTMPSource.setEncoding(3);

			netSrc = new de.humatic.dsj.src.RTMPSource(path, res, null, RTMPSource.NO_STREAM, this);

		} catch (Exception e) {

			e.printStackTrace();

		}

	}

	void buildFrame() {

		createGraph();

		f = new javax.swing.JFrame("dsj - RTMP LoadXML");

		f.setPreferredSize(new Dimension(500, 300));

		f.getContentPane().setLayout(new BorderLayout());

		JButton setVal = new JButton("get XML data");

		setVal.getAccessibleContext().setAccessibleName("set");

		setVal.addActionListener(this);

		f.getContentPane().add("North", setVal);

		ta = new JTextArea(10,10);

		ta.setLineWrap(true);

		ta.setWrapStyleWord(true);

		f.getContentPane().add("Center", ta);

		com.example.as.ASHandler ash = new com.example.as.ASHandler(ta);

		netSrc.setActionScriptHandler(ash);

		f.pack();

		f.setVisible(true);

		f.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

	}



	public void actionPerformed(ActionEvent ae) {

		String actionName = (((JButton) ae.getSource()).getAccessibleContext()
				.getAccessibleName());

		if (actionName.equalsIgnoreCase("set")) {

			AMFType[] types = new AMFType[]{ new AMFType(AMF3.UNDEFINED, null),
											 new AMFType(AMF.STRING, "getVars"),
											 new AMFType(AMF.NUM, new Double(0)),
											 new AMFType(AMF.NULL, null)
											 };

			RTMPMessage msg = new RTMPMessage(RTMP.FLEX_MSG, types);

			((RTMPSource)netSrc).sendRTMP(msg);


		}

	}

	public void propertyChange(java.beans.PropertyChangeEvent pe) {

		try {

			switch (DSJUtils.getEventType(pe)) {

			case DSFiltergraph.RTMP_EVENT:

				System.out.println("\nRTMP event: ");

				try {

					String[] kv = (String[]) (pe.getOldValue());

					for (int i = 0; i < kv.length; i += 2) {

						System.out.println(kv[i] + ": " + kv[i + 1]);

					}

				} catch (Exception e) {
				}

				System.out.println("_______________________________");

				break;

			}

		} catch (Exception e) {
		}

	}

	public static void main(String[] args) {

		new RTMP_GetXML().buildFrame();

	}

}
