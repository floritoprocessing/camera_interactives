/**
dsj demo code.
You may use, modify and redistribute this code under the terms laid out in Disclaimer.txt and in the header of the DSJDemo application.
copyright 2008
N.Peters
humatic GmbH
Berlin, Germany
**/

import de.humatic.dsj.*;

/**
Demonstrates 3 ways to combine video and audio into a single file.
**/

public class AVCombine implements java.beans.PropertyChangeListener {

	DSFiltergraph combiner;

	int MODE = 0;

	String inputPath1 = "media/video.avi",
		   inputPath2 = "media/audio.wav";

	boolean exportDone;

	public AVCombine() {

		switch (MODE) {

			case 0:

				/** This is fast, but only works with uncompressed PCM audio **/

				DSMovie.mux(inputPath1, inputPath2, null, "media/mux.avi");

				return;

			case 1:

				/** Builds a filtergraphh with 2 source filters. In order to keep this easy to understand, the code does not
				check for errors (the low level methods don't throw exceptions but use boolean return values) **/

				combiner = DSGraph.createFilterGraph(DSFiltergraph.HEADLESS, this);

				DSFilter vSrc = combiner.addFilterToGraph(DSFilterInfo.filterInfoForCLSID("{E436EBB5-524F-11CE-9F53-0020AF0BA770}"));
				vSrc.setParameter("src", inputPath1);

				DSFilter aSrc = combiner.addFilterToGraph(DSFilterInfo.filterInfoForCLSID("{E436EBB5-524F-11CE-9F53-0020AF0BA770}"));
				aSrc.setParameter("src", inputPath2);

				/**
				Change this if you don't have XviD installed.
				Vista users: Run the Xvid control panel and disable encoding status display.
				**/

				DSFilter vEnc = combiner.addFilterToGraph(DSFilterInfo.filterInfoForName("XviD MPEG-4 Codec"));

				/**
				The MPEG Layer-3 encoder is no longer available publically on Vista / Windows 7, that is the filter is
				present, but won't connect in any other than Microsoft's applications. (can use LAME instead -
				http://www.free-codecs.com/LAME_DirectShow_Filter_download.htm. Filter registration must be run as administrator
				We're just going to leave audio uncompressed.
				**/

				DSFilter aEnc = null;
				if (DSEnvironment.getOSVersion() < 6) aEnc = combiner.addFilterToGraph(DSFilterInfo.filterInfoForName("MPEG Layer-3"));
				else {
					//aEnc = combiner.addFilterToGraph(DSFilterInfo.filterInfoForName("LAME Audio Encoder"));
					aEnc = combiner.addFilterToGraph(DSFilterInfo.filterInfoForName("PCM"));
				}

				DSFilter aviMux = combiner.addFilterToGraph(DSFilterInfo.filterInfoForCLSID("{E2510970-F137-11CE-8B67-00AA00A3F1A6}"));

				DSFilter writer = combiner.addFilterToGraph(DSFilterInfo.filterInfoForCLSID("{8596E5F0-0DA5-11D0-BD21-00A0C911CE86}"));
				writer.setParameter("src", "media/2Source.avi");

				aviMux.connectDownstream(aviMux.getOutputs()[0], writer.getInputs()[0], true);

				vSrc.connectDownstream(vSrc.getOutputs()[0], vEnc.getInputs()[0], false);
				aSrc.connectDownstream(aSrc.getOutputs()[0], aEnc.getInputs()[0], false);

				aEnc.connectDownstream(aEnc.getOutputs()[0], aviMux.getInputs()[0], false);

				vEnc.connectDownstream(vEnc.getOutputs()[0], aviMux.getInputs()[1], false);

				((DSGraph)combiner).setupComplete();

				combiner.play();

				break;

			case 2:

				/** Use an editable movie **/

				combiner = new DSMovie(inputPath1, DSFiltergraph.DD7 | DSFiltergraph.INIT_PAUSED | DSMovie.INIT_EDITABLE, this);

				((DSMovie)combiner).add(DSMovie.copyToClipboard(inputPath2, 0, -1));

				((DSMovie)combiner).export("media/combined.asf", null, null);

				break;

		}

		while(MODE > 0 && !exportDone) {

			try{ Thread.currentThread().sleep(500); } catch (Exception e){}

		}

		combiner.dispose();

	}

	public void propertyChange(java.beans.PropertyChangeEvent pe) {

		if (DSJUtils.getEventType(pe) == DSFiltergraph.DONE ||
			DSJUtils.getEventType(pe) == DSFiltergraph.EXPORT_DONE ||
			DSJUtils.getEventType(pe) == DSFiltergraph.SAVE_DONE) exportDone = true;

	}



	public static void main(String[] args) {

		AVCombine avc = new AVCombine();

	}

}

