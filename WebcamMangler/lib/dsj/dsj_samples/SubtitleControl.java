/**
dsj demo code.
You may use, modify and redistribute this code under the terms laid out in Disclaimer.txt and in the header of the DSJDemo application.
copyright 2008
N.Peters
humatic GmbH
Berlin, Germany
**/

import de.humatic.dsj.DSFiltergraph;
import de.humatic.dsj.ext.Registry;

/**
This sample is not runnable. Instead it presents a starting point for a utility class to control DirectShow
filters from code using the Registry class and methods in DSFiltergraph.
**/

public class SubtitleControl {

	public static boolean subtitlesAvailable(DSFiltergraph dsfg) {

		return dsfg.findFilterByName("ffdshow MPEG-4 Video Decoder") != null && Registry.isSubKeyAccessible(Registry.HKEY_CURRENT_USER, "SOFTWARE\\GNU\\ffdshow\\default", "isSubtitles") == Registry.SUCCESS;

	}

	public static boolean getSubtitlesActive() {

		return Registry.getSubKeyValue_Int(Registry.HKEY_CURRENT_USER, "SOFTWARE\\GNU\\ffdshow\\default", "isSubtitles") != 0;

	}

	public static void setSubtitlesActive(DSFiltergraph dsfg, boolean active) {

		Registry.setSubKeyValue_Int(Registry.HKEY_CURRENT_USER, "SOFTWARE\\GNU\\ffdshow\\default", "isSubtitles", (active ? 1 : 0));

		dsfg.reloadFilter(dsfg.findFilterByName("ffdshow MPEG-4 Video Decoder"));

	}

}