/*
	dsj demo code

	� Copyright np 2006/7, humatic gmbh, berlin, germany. All rights reserved.

	This software is supplied to you by humatic
	("humatic") in consideration of your agreement to the following terms, and your
	use, installation, modification or redistribution of this software
	constitutes acceptance of these terms.  If you do not agree with these terms,
	please do not use, install, modify or redistribute this software.

	In consideration of your agreement to abide by the following terms, and subject
	to these terms, humatic grants you a personal, non-exclusive license, under humatic's
	copyrights in this original humatic software (the "humatic software"), to use,
	reproduce, modify and redistribute the humatic software, with or without
	modifications, in source and/or binary forms; provided that if you redistribute
	the humatic software in its entirety and without modifications, you must retain
	this notice and the following text and disclaimers in all such redistributions of
	the software.  Neither the name, trademarks, service marks or logos of
	humatic may be used to endorse or promote products derived from the
	software without specific prior written permission from humatic.  Except as
	expressly stated in this notice, no other rights or licenses, express or implied,
	are granted by humatic herein, including but not limited to any patent rights that
	may be infringed by your derivative works or by other works in which the humatic
	software may be incorporated.

	The software is provided by humatic on an "AS IS" basis.  HUMATIC MAKES NO
	WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
	WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
	PURPOSE, REGARDING THE HUMATIC SOFTWARE OR ITS USE AND OPERATION ALONE OR IN
	COMBINATION WITH YOUR PRODUCTS.

	IN NO EVENT SHALL HUMATIC BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
	GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
	ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION, MODIFICATION AND/OR DISTRIBUTION
	OF THE HUMATIC SOFTWARE, HOWEVER CAUSED AND WHETHER UNDER THEORY OF CONTRACT, TORT
	(INCLUDING NEGLIGENCE), STRICT LIABILITY OR OTHERWISE, EVEN IF HUMATIC HAS BEEN
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

import de.humatic.dsj.*;
import java.awt.Color;
import java.io.IOException;

import javax.sound.sampled.*;

/**

AVSource.java
Shows how to build a filtergraph with both java video and audio source filters and make it render into
an avi file
**/

public class AVSource implements java.beans.PropertyChangeListener {

	private DSGraph graph;

	private JavaSourceFilter videoSourceFilter,
							 audioSourceFilter;

	private int bufferSize,
				BITS,
				CHANNELS,
				videoFrameTime,
				audioFrameTime,
				/**
				Demo timelimits.
				Keep these a little above even multiples of the audio/video time ratio (resp. in threaded mode
				the audioFrameTime) or you may not get the last audiosample sent and sound will cut out early.
				**/
				MAX_FRAMES = 205,
				MAX_TIME = 15005,
				/** to run threaded outcomment the CALLBACK flag **/
				SOURCE_FLAGS = JavaSourceFilter.BLOCKING | JavaSourceFilter.CALLBACK;

	private float FPS = 25f,
				  SAMPLE_RATE;

	private javax.swing.JFrame f;

	private SwingMovieController smc;

	private AudioInputStream audioInputStream;

	private byte[] audioBytes;

    private java.awt.Graphics2D g2d;

    private int frame = 0;

    private long startTime;

    String fileName;

	public AVSource() {}

	public void createGraph() {

		/**
		TODO:
		Change this to an (uncompressed PCM) audiofile on your machine! The demo uses standard javasound
		functions to read from a file. You may change that to other sources in line 122.
		**/

		java.io.File audioFile = new java.io.File("media/audio.wav");


		try {

			audioInputStream = AudioSystem.getAudioInputStream(audioFile);

			AudioFormat format = audioInputStream.getFormat();

			SAMPLE_RATE = format.getSampleRate();

			BITS = format.getSampleSizeInBits();

			CHANNELS = format.getChannels();

			System.out.println("audiofile: "+ audioFile.getName()+"\n"+
							   "sampleRate: "+SAMPLE_RATE+"\n"+
							   "bits: "+BITS+"\n"+
							   "channels: "+CHANNELS+"\n"+
							   "frameSize: "+format.getFrameSize()+"\n"+
							   "frameRate: "+format.getFrameRate());



		} catch (Exception e) {

			System.out.println("error loading audio file");

			return;

		}

		f = new javax.swing.JFrame("dsj_AVSource");

		f.setLayout(new java.awt.BorderLayout());

		graph = DSGraph.createFilterGraph(DSFiltergraph.JAVA_POLL | DSFiltergraph.INIT_PAUSED, this);

		startTime = System.currentTimeMillis();

		videoSourceFilter = graph.insertJavaSourceFilter(720, 480, FPS, SOURCE_FLAGS);

		g2d  = videoSourceFilter.getDrawingSurface();

		g2d.setFont(new java.awt.Font("Arial", 1, 48));

		g2d.setColor(java.awt.Color.red);

		g2d.setBackground(Color.black);

        videoSourceFilter.renderPin(videoSourceFilter.getPin(0, 0));

		audioSourceFilter = graph.insertJavaAudioSource(BITS, CHANNELS, SAMPLE_RATE, JavaSourceFilter.BLOCKING, null, null);

		audioSourceFilter.renderPin(audioSourceFilter.getPin(0, 0));

		bufferSize = audioSourceFilter.getBufferSize();

		System.out.println("bufferSize: "+bufferSize);

		audioBytes = new byte[bufferSize];

		graph.setupComplete();

		f.add("Center", graph.asComponent());

		/** figure out in which intervals DirectShow will want new data **/

		videoFrameTime = videoSourceFilter.getVideoFrameTime();

		audioFrameTime = audioSourceFilter.getAudioFrameTime();

		System.out.println("time per video frame: "+videoFrameTime);

		System.out.println("time per audio frame: "+audioFrameTime);

		/** just for stop button & (preview & replay) volume ctrl **/

		smc = new SwingMovieController();

		smc.setFiltergraph(graph);

		f.add("South", smc);

		f.pack();

		f.setVisible(true);

		/**
		If you break here you have a preview graph.

		We're going to tear down parts of the filtergraph to connect a filesink.
		In BLOCKING mode the source filters should be stopped explicitly

		**/

		videoSourceFilter.stop();

		audioSourceFilter.stop();

		/** create and connect the sink **/

		setupFileRendering();

		/** restart the graph **/

		if ((SOURCE_FLAGS & JavaSourceFilter.CALLBACK) == 0) {

			System.out.println("Rendering threaded for "+MAX_TIME+"msec");

			new Thread(new RenderThread()).start();

		} else System.out.println("Rendering "+MAX_FRAMES+" frames, callback driven");

		graph.play();

		f.addWindowListener(new java.awt.event.WindowAdapter () {
			public void windowClosing (java.awt.event.WindowEvent e) {

				try{

					videoSourceFilter.stop();

					audioSourceFilter.stop();

                    graph.stop();

					graph.dispose();

				} catch (Exception ex){}

				System.exit(0);

			}

			public void windowClosed (java.awt.event.WindowEvent e) {
				System.exit(0);
			}
		});

    }


	public void propertyChange(java.beans.PropertyChangeEvent pe) {

		if (Integer.valueOf(pe.getNewValue().toString()).intValue() == DSFiltergraph.VIDEO_BUFFER_REQUEST) {

			System.err.println("video frame request " +(System.currentTimeMillis()-startTime));

			g2d.clearRect(0, 0, 720, 480);

			g2d.drawString(Integer.toString(frame), 350, 240);

			videoSourceFilter.setJavaImage();

			frame++;

			/**
			Driving both filters in callback and blocking mode may result in video and audio getting out of sync
			when either filter is indeed blocked for a longer time and the other is not (which may happen if eventually an
			image composition takes longer to be prepared or the like) It has prooven hard to avoid this without
			riscing deadlocks. Therefore only he filter that may eventually need more time should be set up in
			callback mode and both filters should  be fed from that callback then.
			To simulate longer blocking remove the comments on the next line.
			**/

			if (frame % (audioFrameTime/videoFrameTime) == 0) {

				try {

					audioInputStream.read(audioBytes);

				} catch (IOException e) {}

				audioSourceFilter.setJavaAudio(audioBytes);

			}

			if (frame > MAX_FRAMES+1) {

				audioSourceFilter.stop();

				videoSourceFilter.stop();

				graph.stop();

				graph.dispose();

				DSMovie movie = new DSMovie("callback_testRender.avi", DSFiltergraph.RENDER_NATIVE, null);

				f.add(java.awt.BorderLayout.CENTER, movie.asComponent());

				smc.setFiltergraph(movie);

				f.pack();

				System.out.println(movie.getInfo());

			}

        } else if (Integer.valueOf(pe.getNewValue().toString()).intValue() == DSFiltergraph.AUDIO_BUFFER_REQUEST) {

			System.err.println("	audio request :"+(System.currentTimeMillis()-startTime));

			/** See comments in video callback

			try {
				audioInputStream.read(audioBytes);
			} catch (IOException e) {}

			audioSourceFilter.setJavaAudio(audioBytes);

			**/
		}

	}

	private void setupFileRendering() {

		try{

			fileName = (SOURCE_FLAGS & JavaSourceFilter.CALLBACK) == 0 ? "threaded_testRender.avi" : "callback_testRender.avi";

			de.humatic.dsj.sink.FileSink fs = new de.humatic.dsj.sink.FileSink(fileName);

			fs.setVideoEncoder(DSFilterInfo.filterInfoForName("Microsoft Video 1")); //"XviD MPEG-4 Codec"

			fs.setAudioEncoder(DSFilterInfo.filterInfoForName("PCM"));//MPEG Layer-3"));

			fs.setMultiplexer(DSFilterInfo.filterInfoForName("AVI Mux"));

			graph.connectSink(fs);

		} catch (Exception e){e.printStackTrace();}
	}




	private class RenderThread extends Thread {

		private int frame,
					time;

		private RenderThread(){}

		public void run() {

			try{

				while (time < MAX_TIME) {

					sleep(5);

					time+=5;

					if (time % videoFrameTime == 0) {

						System.out.print(".");

						//System.out.println(time+" - sending video");

						g2d.clearRect(320, 200, 150, 100);

						g2d.drawString(String.valueOf(frame++), 350, 240);

						videoSourceFilter.setJavaImage();

					}

					if (time % audioFrameTime < 5) {

						//System.out.println(time+" - sending audio");

						audioInputStream.read(audioBytes);

						audioSourceFilter.setJavaAudio(audioBytes);

					}

				}

				System.out.println("videoFrames written: "+frame);

				videoSourceFilter.stop();

				audioSourceFilter.stop();

				graph.stop();

				graph.dispose();

				DSMovie movie = new DSMovie(fileName, DSFiltergraph.RENDER_NATIVE, null);

				f.add(java.awt.BorderLayout.CENTER, movie.asComponent());

				smc.setFiltergraph(movie);

				f.pack();

				System.out.println(movie.getInfo());

			} catch (Exception e) {}

		}

	}

	public static void main(String[] args){

		new AVSource().createGraph();

	}

}
