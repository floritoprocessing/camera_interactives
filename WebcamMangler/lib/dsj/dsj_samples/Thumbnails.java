/**
dsj demo code.
You may use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
copyright 2008
N.Peters
humatic GmbH
Berlin, Germany
**/

import javax.swing.*;

import de.humatic.dsj.*;

public class Thumbnails extends javax.swing.JFrame {

	private DSMovie graph;

	private JPanel stage;

	public Thumbnails() {

		super("dsj - media browser");

	}

	public void buildGraph() {

		final String[] files = new java.io.File("media").list();

		JPanel browser = new JPanel(new java.awt.GridLayout(0, 1));

		final JPanel nav = new JPanel(new java.awt.GridLayout(1, 0));

		for (int i = 0; i < files.length; i++) {

			try{

				final String mPath = new java.io.File("media/"+files[i]).getAbsolutePath();

				JLabel label = new JLabel(files[i], new ImageIcon(DSJUtils.getMediaIcon(mPath, 100, 80, -1)), SwingConstants.LEFT);

				label.addMouseListener(new java.awt.event.MouseAdapter() {

					public void mouseClicked(java.awt.event.MouseEvent me) {

						if (graph != null) {

							graph.dispose();

							graph = null;

						} else remove(stage);

						graph = new DSMovie(mPath, DSFiltergraph.DD7 | DSFiltergraph.INIT_PAUSED, null);

						add(java.awt.BorderLayout.CENTER, graph.asComponent());

						nav.removeAll();

						for (int j = 0; j < 4; j++) {

							final int jt = j;

							JLabel navLabel = new JLabel(new ImageIcon(graph.getThumbnail(graph.getDuration()/4*j, 80)));

							navLabel.addMouseListener(new java.awt.event.MouseAdapter() {

								public void mouseClicked(java.awt.event.MouseEvent me) {

									graph.setTimeValue(graph.getDuration()/4*jt);

									graph.play();

								}

							});

							nav.add(navLabel);

						}

						pack();

						graph.play();

					}

				});

				browser.add(label);

			} catch (Exception e){ System.out.println(e.toString());}

		}

		add(java.awt.BorderLayout.WEST, new JScrollPane(browser));

		stage = new JPanel();

		stage.setPreferredSize(new java.awt.Dimension(320, 240));

		add(java.awt.BorderLayout.CENTER, stage);

		add(java.awt.BorderLayout.SOUTH, nav);

		pack();

		setVisible(true);

		/**
        Don't do this at home. This demo relies on dsj closing and disposing off filtergraphs when the JVM exits. This is
        OK for a "open graph, do something & exit" style demo, but real world applications should take care of calling
        dispose() on filtergraphs they're done with themselves.
        **/

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

	}


	public static void main(String[] args) {

		new Thumbnails().buildGraph();

	}

}