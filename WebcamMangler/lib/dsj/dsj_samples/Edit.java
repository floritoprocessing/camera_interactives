/**
dsj demo code.
You may use, modify and redistribute this code under the terms laid out in Disclaimer.txt and in the header of the DSJDemo application.
copyright 2008
N.Peters
humatic GmbH
Berlin, Germany
**/

import de.humatic.dsj.*;
import de.humatic.dsj.edit.*;

/**
Shows some aspects of an editable DSMovie. For reasons of clarity the code does not perform any errorchecking...
**/

public class Edit extends javax.swing.JFrame implements java.beans.PropertyChangeListener {

	private DSMovie graph;

	private boolean exportDone;

	String path = "media/2997HalfNTSC.asf",
		   path2 = "media/2997ndHalfNTSC.wmv",
		   path3 = "media/audio.wav",
		   path4 = "media/video.avi";

	public Edit() {

		super("dsj - editing demo");

	}

	public void buildGraph() {

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		graph = new DSMovie(path, DSFiltergraph.DD7 | DSFiltergraph.INIT_PAUSED | DSMovie.INIT_EDITABLE, this);

		add(java.awt.BorderLayout.CENTER, graph.asComponent());

		add(java.awt.BorderLayout.SOUTH, new SwingMovieController(graph));

		pack();

		setVisible(true);

		graph.setSelection(2000, 4000);

		graph.trim();

		graph.setTimeValue(2000);

		graph.paste(DSMovie.copyToClipboard(path2, 0, 4000));

		graph.setSelection(1000, 3000);

		graph.setTimeValue(graph.getDuration());

		graph.paste(graph.copy());

		graph.setSelection(3000, 5000);

		graph.cut();

		/** replace one videotrack by something else (we're not using DSMovie.replace(..), as that would work on the entire movie)**/

		MovieTrack vt = graph.getIndTrackType(DSMediaType.MT_VIDEO, 2);

		graph.setTimeValue(vt.getOffset());

		int repLength = vt.getDuration() - vt.getOffset();

		graph.removeTrack(vt);

		graph.add(DSMovie.copyToClipboard(path4, 0, repLength));

		graph.setTimeValue(0);

		/** add a long audio track **/

		graph.add(DSMovie.copyToClipboard(path3, 0, graph.getDuration()));

		MovieTrack t = graph.getIndTrackType(DSMediaType.MT_AUDIO, graph.getTrackCount(DSMediaType.MT_AUDIO)-1);

		t.setVolume(0);

		t.setVolume(0.1f, t.getOffset()+1000, t.getOffset()+3000, TrackEffect.JUMP);

		t.setVolume(0.3f, t.getOffset()+3000, t.getOffset()+5000, TrackEffect.LINEAR);

		t.setVolume(0.2f, t.getOffset()+5000, t.getDuration(), TrackEffect.LINEAR);

		/** Modify volume automation a bit **/

		TrackEffect volumeEffect = t.getIndTrackEffect(0);

		/** change one CuePoint **/

		CuePoint cp = volumeEffect.getCuePointAtTime(3000, -1);

		cp.setFloatValue(0.6f);

		/** add another one **/

		volumeEffect.insertCuePoint(new CuePoint("Vol", t.getOffset()+4000, 0.9f, TrackEffect.LINEAR));

		/** submit the changes **/

		t.updateEffectAutomation(volumeEffect);

		System.out.println(t);

		System.out.println(volumeEffect);

		System.out.println("1st:\n"+graph.getAsXML());

		graph.setTimeValue(graph.getDuration());

		/** Add video transition. First add another videotrack, overlapping all other videotracks.. **/

		graph.add(DSMovie.copyToClipboard(path2, 0, graph.getDuration()));

		/** remove the audio that was added with the video.. **/

		graph.removeTrack(graph.getIndTrackType(DSMediaType.MT_AUDIO, graph.getTrackCount(DSMediaType.MT_AUDIO)-1));

		/** Get the videotrack, that the effect will be asigned to.. **/

		vt = graph.getIndTrackType(DSMediaType.MT_VIDEO, graph.getTrackCount(DSMediaType.MT_VIDEO)-1);

		/** Move track so that it overlaps another one (alternatively we could have repositioned the movie before adding the track) **/

		try{ vt.setOffset(0); } catch (Exception e){}

		int whichEffect = 2;

		EffectDescription ed = null;

		switch (whichEffect) {

			case 0:

				EffectDescription[] eds = EffectDescription.getAvailableVideoTransitions();

				ed = eds[7];

				/** no parameters known **/

				break;

			case 1:

				/** all kinds of push pull transitions, defined by MaskNum **/

				ed = EffectDescription.createForType(TrackEffect.TRANSITION);

				ed.setSubType(EffectDescription.SMPTE_WIPE);

				ed.addParameter("MaskNum", 8);

				ed.addParameter("BorderWidth", 10);

				ed.addParameter("BorderSoftness",1);

				ed.addParameter("BorderColor", 255);

				break;

			case 2:

				/** Picture in Picture **/

				ed = EffectDescription.createForType(TrackEffect.TRANSITION);

				ed.setSubType(EffectDescription.COMPOSITOR);

				ed.addParameter("SrcWidth", 360);

				ed.addParameter("SrcHeight", 240);

				ed.addParameter("SrcOffsetX",0);

				ed.addParameter("SrcOffsetY", 0);

				ed.addParameter("Height", 80);

				ed.addParameter("Width", 120);

				ed.addParameter("OffsetX",50);

				ed.addParameter("OffsetY", 50);

				break;

		}

		vt.addEffect(ed, vt.getOffset(), vt.getDuration());

		dumpEffects(vt);

		System.out.println(graph.getAsXML());

		vt.setLayer(graph.getTrackCount(DSMediaType.MT_VIDEO));

		TrackEffect pip = vt.getIndTrackEffect(0);

		pip.insertCuePoint(new CuePoint("Width", graph.getDuration(), 200f, TrackEffect.LINEAR));

		pip.insertCuePoint(new CuePoint("Height", graph.getDuration()/2, 150f, TrackEffect.LINEAR));

		vt.updateEffectAutomation(pip);

		graph.setTimeValue(0);

		try{

			/** Save the timeline's xml representation. DSMovie can open this file and recreate the timeline from it.**/

			graph.saveAs("dsjEdit.xtl", false);

			/** Export everything into a new file **/

			graph.export("dsjEdit.asf", DSFilterInfo.doNotRender(), DSFilterInfo.doNotRender());

		} catch (Exception e){}

		while (!exportDone) {

			try{

				Thread.currentThread().sleep(500);

			} catch (Exception e){}

		}

	}

	private void dumpTracks() {

		int trackCount = graph.getTrackCount();

		System.out.println("_____________________________\ntrackCount: "+trackCount);

		for (int i = 0; i < trackCount; i++) System.out.println(graph.getIndTrack(i));

	}

	private void dumpEffects(MovieTrack t) {

		int effectCount = t.getEffectCount();

		for (int i = 0; i < effectCount; i++) System.out.println(t.getIndTrackEffect(i));

	}

	public void propertyChange(java.beans.PropertyChangeEvent pe) {

		switch(Integer.valueOf(pe.getNewValue().toString()).intValue()) {

			case DSFiltergraph.EXPORT_DONE:

				exportDone = true;

		}

	}


	public static void main(String[] args) {

		Edit tt = new Edit();

		tt.buildGraph();

	}

}