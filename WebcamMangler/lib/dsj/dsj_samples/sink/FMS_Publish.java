/**
dsj demo code.
You may use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
copyright 2010
N.Peters
humatic GmbH
Berlin, Germany
**/

import de.humatic.dsj.*;

import de.humatic.dsj.sink.JSink;
import de.humatic.dsj.sink.RTMPSink;

import de.humatic.dsj.src.RTMPSource;

/**
Shows streaming to a Flash Media Server. The free development version of FMS 3.5 is available from
http://www.adobe.com/products/flashmediaserver/. There are a number of alternative RTMP servers -
most notably Wowza and Red5.
Both Flash Player and dsj can be used to receive streams.
**/

public class FMS_Publish implements java.beans.PropertyChangeListener {

	private DSFiltergraph graph;

	private RTMPSink sink;

	private RTMPSource netSrc;

	private String FMS_IP = "127.0.0.1";

	private int encoderChoice = 0;

	public FMS_Publish() {}

	public void createGraph() {

		javax.swing.JFrame f = new javax.swing.JFrame("dsj - FMS publish");

		if (true) {

			graph = DSCapture.fromUserDialog(f, DSFiltergraph.DD7, this);

		} else {

			java.awt.FileDialog fd = new java.awt.FileDialog(f, "select movie", java.awt.FileDialog.LOAD);

			fd.setVisible(true);

			if (fd.getFile() == null) return;

			graph = new DSMovie(fd.getDirectory()+fd.getFile(), DSFiltergraph.DD7 | DSFiltergraph.INIT_PAUSED, this);

			graph.setLoop(true);

		}

		try{ f.getContentPane().add(java.awt.BorderLayout.CENTER, graph.asComponent()); } catch (Exception e){}

		f.getContentPane().add(java.awt.BorderLayout.SOUTH, new SwingMovieController(graph));

		final javax.swing.JButton record = new javax.swing.JButton("record");

		record.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent e) {

				rec = !rec;

				try{

					startDVR("livestream",
							 rec ? System.currentTimeMillis() : -1000,
							 rec ? -1000 : System.currentTimeMillis()
							 );

				} catch (Exception ex){}

				record.setText(rec ? "pause" : "record");

			}

		});

		f.getContentPane().add(java.awt.BorderLayout.NORTH, record);

		f.pack();

		int screenWidth = (int)(java.awt.Toolkit.getDefaultToolkit().getScreenSize().getWidth());

		f.setLocation(screenWidth / 2 - f.getWidth() / 2, 300);

		f.setVisible(true);

		try{

			int flags = DSFiltergraph.PREVIEW | JSink.SHOW_ENC_DLG;

			/** hardcoded encoders, change or make sure these are installed **/

			switch (encoderChoice) {

				case 0:

					/** MUST set ffdshow to either flv1 or h264/avc1 !! **/

					sink = new RTMPSink(graph, "rtmp://"+FMS_IP+"/live", "livestream", DSFilterInfo.filterInfoForName("ffdshow video encoder"), DSFilterInfo.filterInfoForName("LAME Audio Encoder"), flags);

					break;

				case 1:

					sink = new RTMPSink(graph, "rtmp://"+FMS_IP+"/live", "livestream", DSFilterInfo.filterInfoForName("Elecard AVC Video Encoder SD"), DSFilterInfo.filterInfoForName("Elecard AAC Audio Encoder"), flags);

					break;

			}

			graph.play();

		} catch (Exception e){e.printStackTrace();}

		/**
		Don't do this at home. This demo relies on dsj closing and disposing off filtergraphs when the JVM exits. This is
		OK for a "open graph, do something & exit" style demo, but real world applications should take care of calling
		dispose() on filtergraphs they're done with themselves.
		**/

		f.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

	}

	public void propertyChange(java.beans.PropertyChangeEvent pe) {

		switch (DSJUtils.getEventType(pe)) {

			case DSFiltergraph.RTMP_EVENT:

				System.out.println("\nRTMP event: ");

				try{

					String[] kv = (String[])(pe.getOldValue());

					for (int i = 0; i < kv.length; i+=2) System.out.println(kv[i]+": "+kv[i+1]);

				} catch (Exception e){}

				System.out.println("_______________________________");

			break;

		}

	}

	/**
	This builds and sends an RTMPMessage used to start and pause serverside recording of the published stream and will only
	work with FMS applications that both support recording and implement the DVRSetStreamInfo ActionScript method
	(both of which is not the case with the "live" service preinstalled on FMS).
	For more details and a working server application see the "dvrcast" FMS sample application available from Adobe.
	To automatically start recording on stream publish set the RTMPSink.RECORD flag in the sink's constructor.
	**/

	private void startDVR(String streamName,
						  long startTime,
						  long stopTime
						  ) throws Exception {

		/** Parameters going into a nested object **/

		AMFType[] nestedTypes = new AMFType[]{
			new AMFType(AMF.STRING, "streamName", false),
			new AMFType(AMF.STRING, streamName),
			new AMFType(AMF.STRING, "callTime", false),
			new AMFType(AMF.DATE, new Double(System.currentTimeMillis()), false),
			new AMFType(AMF.STRING, "startRec", false),
			new AMFType(AMF.DATE, Long.valueOf(startTime)),
			new AMFType(AMF.STRING, "stopRec", false),
			new AMFType(AMF.DATE, new Double(stopTime)),
			new AMFType(AMF.STRING, "maxlen", false),
			new AMFType(AMF.NUM, Double.valueOf(-1)),
			new AMFType(AMF.STRING, "begOffset", false),
			new AMFType(AMF.NUM, Double.valueOf(-1)),
			new AMFType(AMF.STRING, "endOffset", false),
			new AMFType(AMF.NUM, Double.valueOf(-1)),
			new AMFType(AMF.STRING, "append", false),
			new AMFType(AMF.BOOL, Boolean.valueOf(true)),
			new AMFType(AMF.STRING, "offline", false),
			new AMFType(AMF.BOOL, Boolean.valueOf(false))
		};

		/** The toplevel "invoke" method's body. **/

		AMFType[] types = new AMFType[]{new AMFType(AMF.STRING, "DVRSetStreamInfo"),
										new AMFType(AMF.NUM, Double.valueOf(0)),
										new AMFType(AMF.NULL, null),
										new AMFType(AMF.OBJECT, nestedTypes),
										new AMFType(AMF.END, null)
										};

		RTMPMessage msg = new RTMPMessage(3, 0, de.humatic.dsj.src.rtmp.RTMP.INVOKE, types, sink.getChunkSize());

		sink.sendRTMP(msg);

	}

	public static void main(String[] args){

		new FMS_Publish().createGraph();

	}

}