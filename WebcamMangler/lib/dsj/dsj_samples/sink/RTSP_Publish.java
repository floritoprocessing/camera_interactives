/**
dsj demo code.
You may use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
copyright 2009
N.Peters
humatic GmbH
Berlin, Germany
**/

import de.humatic.dsj.*;

import de.humatic.dsj.sink.JSink;
import de.humatic.dsj.sink.RTPSink;
import de.humatic.dsj.sink.sa.StreamAnnouncement;

import de.humatic.dsj.src.RTSPSource;

/**
Demonstrates streaming a filtergraph's data in RTP format and announcing the stream via RTSP. This does not necessarily
involve a streaming server, although for larger projects you will probably want to use one. The best known and most
standards compliant RTSP servers around probably are Quicktime Streaming Server (http://www.apple.com/support/downloads/quicktimestreamingserver.html)
and its open source Darwin variant (http://dss.macosforge.org/).
For details on supported encodings etc. see the javadocs for the sink and src/rtp packages.
You can use dsj, VLC, Quicktime Player and others to receive the streams.
**/

public class RTSP_Publish implements java.beans.PropertyChangeListener {

	private DSFiltergraph graph;

	private RTPSink sink;

	private RTSPSource netSrc;

	private StreamAnnouncement announcement;

	/** adjust to try different aspects of this demo **/

	private int announcementMode = 0,
				encoderChoice	 = 1;

	/** CHANGE TO YOUR SERVER'S IP !! */

	private String DARWIN_IP = "192.168.0.130";

	public RTSP_Publish() {}

	public void createGraph() {

		javax.swing.JFrame f = new javax.swing.JFrame("dsj - RTSP publish");

		if (true) {

			graph = DSCapture.fromUserDialog(f, DSFiltergraph.DD7, this);

		} else {

			java.awt.FileDialog fd = new java.awt.FileDialog(f, "select movie", java.awt.FileDialog.LOAD);

			fd.setVisible(true);

			if (fd.getFile() == null) return;

			graph = new DSMovie(fd.getDirectory()+fd.getFile(), DSFiltergraph.DD7 | DSFiltergraph.INIT_PAUSED, this);

			graph.setLoop(true);

		}

		f.add(java.awt.BorderLayout.CENTER, graph.asComponent());

		f.add(java.awt.BorderLayout.SOUTH, new SwingMovieController(graph));

		f.pack();

		int screenWidth = (int)(java.awt.Toolkit.getDefaultToolkit().getScreenSize().getWidth());

		f.setLocation(screenWidth / 2 - f.getWidth(), 300);

		f.setVisible(true);

		try{

			/** %announcementMode determines how this stream will be announced and (for this demo) what targetURLs will be used **/

			if (announcementMode == 0) {

				/** creates an sdp file that can then be opened by dsj RTSPSource, VLC, Quicktime etc. **/

				announcement = StreamAnnouncement.createForType(StreamAnnouncement.SDP, "dsj_broadcast.sdp");

			} else if (announcementMode == 1) {

				/** starts a SAP announcement thread **/

				announcement = StreamAnnouncement.createForType(StreamAnnouncement.SAP, null);

			} else if (announcementMode == 2) {

				/** pushes stream up to a QTSS from where clients can request it **/

				announcement = StreamAnnouncement.createForType(StreamAnnouncement.DARWIN_PUSH, DARWIN_IP+":554/pushMeUpAV.sdp");

				/** optional, depends on server settings **/

				announcement.setParameter("username", "user");

				announcement.setParameter("password", "password");

			}

			/** optional.. (Quicktime player will use %title as window title etc.) **/

			announcement.setParameter("author", "somebody");

			announcement.setParameter("title", "dsj (re)stream");

			String[] targetURLs = null;

			if (announcementMode < 2) {

				/** use multicast for "local" connections. This could just as well be unicast **/

				targetURLs = new String[]{"230.0.0.1:5032", "230.0.0.1:5034"};

			} else {

				/** but we can not multicast to Darwin **/

				targetURLs = new String[]{DARWIN_IP+":5032"};

			}

			int flags = DSFiltergraph.PREVIEW | JSink.SHOW_ENC_DLG;

			/** Hardcoded encoders, change or make sure you have these installed **/

			switch (encoderChoice) {

				case 0:

					sink = new RTPSink(graph, targetURLs, DSFilterInfo.filterInfoForName("Elecard MPEG-4 Video Encoder SD"), DSFilterInfo.filterInfoForName("LAME Audio Encoder"), announcement, flags);

					break;

				case 1:

					sink = new RTPSink(graph, targetURLs, DSFilterInfo.filterInfoForName("Elecard AVC Video Encoder SD"), DSFilterInfo.filterInfoForName("Elecard AAC Audio Encoder"), announcement, flags);

					break;

				case 2:

					/** MUST set ffdshow to either mpeg4 or h264/avc1 !! **/

					sink = new RTPSink(graph, targetURLs, DSFilterInfo.filterInfoForName("ffdshow video encoder"), DSFilterInfo.filterInfoForName("Elecard AAC Audio Encoder"), announcement, flags);

					break;

				case 3:

					/**
					Shows usage of save flag & DSFilterInfo.filterInfoForCodecState(..): With SHOW_DLG_SAVE in place dsj will serialize
					the encoders state to a tmp. file under the encoder's name. By reloading it back from that location and keeping the
					save flag in place all changes to the filter's properties will be overwritten, saved and reopened like that on the next run.
					To permanently save some state to a location of your choice use DSEnvironment.persistCodecState(..) after the sink constructor.
					**/

					flags = DSFiltergraph.PREVIEW | JSink.SHOW_DLG_SAVE;

					DSFilterInfo videoEncoder = null;

					DSFilterInfo audioEncoder = null;

					try{

						videoEncoder = DSFilterInfo.filterInfoForCodecState("Elecard AVC Video Encoder SD", new java.io.File(DSEnvironment.getProperty(DSEnvironment.SP_FILTERSTATE_DIR)+"Elecard AVC Video Encoder SD.stg"));

					} catch (Exception fe){

						/** first run will throw NullPointerException **/

						videoEncoder = DSFilterInfo.filterInfoForName("Elecard AVC Video Encoder SD");

					}

					try{

						audioEncoder = DSFilterInfo.filterInfoForCodecState("Elecard AAC Audio Encoder", new java.io.File(DSEnvironment.getProperty(DSEnvironment.SP_FILTERSTATE_DIR)+"Elecard AAC Audio Encoder.stg"));

					} catch (Exception fe){

						audioEncoder = DSFilterInfo.filterInfoForName("Elecard AAC Audio Encoder");

					}

					sink = new RTPSink(graph, targetURLs, videoEncoder, audioEncoder, announcement, flags);

					break;

			}

		} catch (Exception e){e.printStackTrace();}

		/**
		Don't do this at home. This demo relies on dsj closing and disposing off filtergraphs when the JVM exits. This is
		OK for a "open graph, do something & exit" style demo, but real world applications should take care of calling
		dispose() on filtergraphs they're done with themselves.
		**/

		f.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

	}

	public void propertyChange(java.beans.PropertyChangeEvent pe) {

		//System.out.println(DSJUtils.getEventType(pe));

	}

	public static void main(String[] args){

		new RTSP_Publish().createGraph();

	}

}