/**
dsj demo code.
You may use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
copyright 2005-7
N.Peters
humatic GmbH
Berlin, Germany
**/

import de.humatic.dsj.*;

/**
Quick demo for the JavaOverlayFilter.
After capturing to file, on reseting the graph to "normal", the overlay filter will be removed and you would need to
reinsert it if you want to proceed with it (or alternatively instead of calling setPreview(..) make use of the changeCaptureFile(..)
method when using DSCapture).
In a capture graph, you will not see the results of your drawing in the preview during capture, as the overlay filter has been
moved to the capture branch of the graph.
**/

public class InGraphOverlay implements java.beans.PropertyChangeListener {

	private DSFiltergraph graph;

	private final String graph_FILE = "media/video.avi";

	private JavaOverlayFilter jof;

	private java.awt.Graphics2D g2d;

	private int frameAnim = 0;

	private boolean exporting;

	private java.awt.image.BufferedImage image;

	private javax.swing.JCheckBox fade;

	private int TYPE = DSFiltergraph.MOVIE;

	public InGraphOverlay() {}

	public void createGraph() {

		javax.swing.JFrame f = new javax.swing.JFrame("dsj InGraphOverlay");

		if (TYPE == DSFiltergraph.MOVIE) {

			graph = new DSMovie(graph_FILE, DSFiltergraph.DD7 | DSFiltergraph.INIT_PAUSED, this);

			graph.setLoop(true);

		} else {

			DSFilterInfo[][] dsi = DSCapture.queryDevices();

			graph = new DSCapture(DSFiltergraph.DD7 | DSFiltergraph.INIT_PAUSED, dsi[0][0], false, DSFilterInfo.doNotRender(), this);

		}

		f.add(java.awt.BorderLayout.CENTER, graph.asComponent());

		javax.swing.JButton toFile = new javax.swing.JButton("to file");

		toFile.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent e) {

				frameAnim = 0;

				exporting = true;

				if (graph.type == DSFiltergraph.MOVIE) ((DSMovie)graph).export("media/overlayTest.asf", DSFilterInfo.doNotRender(), DSFilterInfo.doNotRender());

				else {

					((DSCapture)graph).setCaptureFile("media/overlayCapTest.asf", DSFilterInfo.doNotRender(), DSFilterInfo.doNotRender(), true);

					((DSCapture)graph).record();

				}

				System.out.println("writing 255 frames to file");

			}

		});

		f.add(java.awt.BorderLayout.SOUTH, toFile);

		fade = new javax.swing.JCheckBox("fade in / out");

		f.add(java.awt.BorderLayout.NORTH, fade);

		f.pack();

		f.setVisible(true);

		/**
		Don't do this at home. This demo relies on dsj closing and disposing off filtergraphs when the JVM exits. This is
		OK for a "open graph, do something & exit" style demo, but real world applications should take care of calling
		dispose() on filtergraphs they're done with themselves.
		**/

		f.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		try{

			image = javax.imageio.ImageIO.read(new java.io.File("media/test.gif").getAbsoluteFile());

		} catch (Exception e){}

		jof = graph.insertOverlayFilter(JavaOverlayFilter.CALLBACK);

		g2d = jof.getDrawingSurface();

		/**
		We'll paint some green counter animation on a black square. The filter's default key color is black, which
		is also the default background for Graphics2D. Change that in order to "box" the drawn text.
		**/

		jof.setKeyColor(java.awt.Color.blue);

		g2d.setFont(new java.awt.Font("Arial", 1, 18));

		graph.play();


	}

	public void propertyChange(java.beans.PropertyChangeEvent pe) {

		if (Integer.valueOf(pe.getNewValue().toString()).intValue() == DSFiltergraph.OVERLAY_BUFFER_REQUEST) {

			try{

				if (exporting) System.out.print("OverlayFilter, buffer requested for : "+pe.getOldValue().toString()+" count "+frameAnim+"\r");

				/**
				When you changed the keycolor to something non-black (see line 105)
				you will have to clear the entire area before drawing.
				**/

				g2d.clearRect( 0, 0, (int)(graph.getMediaDimension().getWidth()), (int)(graph.getMediaDimension().getHeight()));

				g2d.drawImage(image, 30,40,80, 80, null);

				g2d.setColor(java.awt.Color.black);

				g2d.fillRect( 130, 170, 50, 40);

				g2d.setColor(java.awt.Color.green);

				g2d.drawString(String.valueOf(frameAnim++), 140, 190);

				/** Now draw something with the keycolor. This will become transparent **/

				g2d.setColor(java.awt.Color.blue);

				g2d.fillRect( 135, 195, 40, 8);

				if (fade.isSelected()) jof.setAlpha(frameAnim / 255f);

				try{

					jof.submitFrame();

				} catch (Exception e){

					g2d = jof.getDrawingSurface();

					System.out.println(e.toString());

				}

				if (exporting && frameAnim > 255) {

					/** sample movie file is shorter than 255 frames, this will not execute when type is MOVIE...**/

					exporting = false;

					if (graph.type == DSFiltergraph.MOVIE) ((DSMovie)graph).cancelExport();

					else ((DSCapture)graph).setPreview();

					System.out.println("\ndone, graph reset");

				}

			/** g2d may be null on first call **/

			} catch (NullPointerException npe){}

		}

	}

	public static void main(String[] args){

		new InGraphOverlay().createGraph();

	}

}