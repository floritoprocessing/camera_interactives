/**
dsj demo code.
You are free to use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
copyright 2010
N.Peters
humatic GmbH
Berlin, Germany
**/

import de.humatic.dsj.*;
import de.humatic.dsj.src.*;

import java.net.*;

/**
The main purpose of this sample is to show how NetworkSource subclasses can be directed via proxy servers by
extending de.humatic.dsj.src.HTTPConnector. It might play some nice D&B tunes along with that - given you have
an AAC decoder on your system and some good luck with the DJ.
**/

public class ProxyRadio implements java.beans.PropertyChangeListener {

	private DSFiltergraph graph;

	private javax.swing.JLabel nowPlaying,
							   format;

	private String playingURL,
				   playingName;

	private String[] urls = new String[]{"http://72.26.204.18:6066",
										 "http://38.107.149.28:8888",
										 "http://78.129.228.37:2470",
										 "http://94.23.8.96:8005"
									   };

	public ProxyRadio() {}

	public void createGraph() {

		/**
		Instantiate the factory that will provide the source class with a HttpURLConnection using a proxy server.
		See nested ProxyRadioConnector class below.
		There are a number of websites with regularily updated lists of free open proxy servers (for example: http://www.proxy-listen.de/Proxy/Proxyliste.html).
		As these servers come and go, it would be useless to hardcode an IP address here. Please look one up and insert it below,
		i.e. make PROXY_IP non null. As is this will just open the stream URL directly.
		**/

		String PROXY_IP = null;

		int PROXY_PORT = 8080;

		if (PROXY_IP != null) {

			ProxyRadioConnector prc = new ProxyRadioConnector(PROXY_IP, PROXY_PORT);

			NetworkSource.setHTTPConnector(prc);

		}

		javax.swing.JFrame f = new javax.swing.JFrame("dsj - ProxyRadio");

		final javax.swing.JComboBox streams = new javax.swing.JComboBox(urls);

		streams.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent ae) {

				createSource(streams.getItemAt(streams.getSelectedIndex()).toString().trim());

			}
		});

		f.getContentPane().add(java.awt.BorderLayout.NORTH, streams);

		nowPlaying = new javax.swing.JLabel("======================================");

		f.getContentPane().add(java.awt.BorderLayout.SOUTH, nowPlaying);

		format = new javax.swing.JLabel();

		try{

			/** This builds the radio itself **/

			final ShoutcastSource netSrc = new ShoutcastSource(urls[0], 0, this);

			graph = netSrc.createGraph(DSFiltergraph.DD7);

		} catch (Exception ex){ex.printStackTrace();}

		javax.swing.JPanel controls = new javax.swing.JPanel();

		final javax.swing.JButton pause = new javax.swing.JButton(" || ");

		pause.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent ae) {

				graph.setRate(graph.getRate() == 0 ? 1 : 0);

				pause.setText(graph.getRate() == 0 ? " > " : " || ");

			}
		});

		final javax.swing.JSlider vol = new javax.swing.JSlider(50, 100, 100);

		vol.addChangeListener(new javax.swing.event.ChangeListener() {

			public void stateChanged(javax.swing.event.ChangeEvent ce) {

				graph.setVolume(vol.getValue() / 100f);

			}
		});

		controls.add(pause);

		controls.add(vol);

		controls.add(format);

		f.getContentPane().add(java.awt.BorderLayout.CENTER, controls);

		f.pack();

		f.setLocation((int)(java.awt.Toolkit.getDefaultToolkit().getScreenSize().getWidth()/2 - f.getWidth()/2), (int)(java.awt.Toolkit.getDefaultToolkit().getScreenSize().getHeight()/2 - f.getHeight()/2));

		f.setVisible(true);

		/**
		Don't do this at home. This demo relies on dsj closing and disposing off filtergraphs when the JVM exits. This is
		OK for a "open graph, do something & exit" style demo, but real world applications should take care of calling
		dispose() on filtergraphs they're done with themselves.
		**/

		f.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

	}

	private void createSource(String streamPath){

		try{

			final ShoutcastSource netSrc = new ShoutcastSource(streamPath, 0, this);

			graph.dispose();

			graph = netSrc.createGraph(DSFiltergraph.DD7);

		} catch (Exception e){e.printStackTrace();}

	}

	public void propertyChange(java.beans.PropertyChangeEvent pe) {

		switch (DSJUtils.getEventType(pe)) {

			case DSFiltergraph.SOURCE_ERROR:

				System.out.println("Source error: ");

				String[] desc = (String[])(pe.getOldValue());

				for (int i = 0; i < desc.length; i++) System.out.println(desc[i]);

				break;

			case DSFiltergraph.SOURCE_USER_DATA:

				byte[][] ud = (byte[][])(pe.getOldValue());

				DSJUtils.logln("user data received: ");

				for (int i = 0; i < ud.length; i++) {

					DSJUtils.dump(ud[i]);

					String icyString = new String(ud[i]);

					if (icyString.toLowerCase().indexOf("streamtitle") != -1) nowPlaying.setText(icyString.split(";")[0].split("=")[1]);

					else if (icyString.toLowerCase().indexOf("icy-url") != -1) {

						playingURL = icyString.substring(icyString.indexOf(":")+1);

						nowPlaying.setToolTipText(playingURL+" - "+playingName);

					} else if (icyString.toLowerCase().indexOf("icy-name") != -1) {

						playingName = icyString.substring(icyString.indexOf(":")+1);

						nowPlaying.setToolTipText(playingURL+" - "+playingName);

					} else if (icyString.toLowerCase().indexOf("content-type") != -1) {

						format.setText(icyString.indexOf("aac") != -1 ? "aac" : "mp3");

					}

				}

				break;

			case DSFiltergraph.PLAYLIST_PARSED:

				String[] list = (String[])(pe.getOldValue());

				DSJUtils.logln("playlist parsed: ");

				for (int i = 0; i < list.length; i++) DSJUtils.logln(list[i]);

				break;

		}

	}

	/** HTTPConnector implementation **/

	private class ProxyRadioConnector extends de.humatic.dsj.src.HTTPConnector {

		private Proxy httpProxy;

		public ProxyRadioConnector(String ip, int port) {

			super();

			SocketAddress addr = new InetSocketAddress(ip, port);

			httpProxy = new Proxy(Proxy.Type.HTTP, addr);

		}

		/**
		The NetworkSource subclass will call this method, expecting it to return a
		HttpURLConnection for the given URL.
		**/

		public HttpURLConnection createConnection(URL forURL) throws java.io.IOException {

			return (java.net.HttpURLConnection)(forURL.openConnection(httpProxy));

		}

	}


	public static void main(String[] args){

		new ProxyRadio().createGraph();

	}

}