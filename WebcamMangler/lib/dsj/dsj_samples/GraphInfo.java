/**
dsj demo code.
You may use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
copyright 2005-7
N.Peters
humatic GmbH
Berlin, Germany
**/

import de.humatic.dsj.*;
import de.humatic.dsj.DSFilter.DSPin;
import de.humatic.dsj.DSFilterInfo.DSPinInfo;

/**
This prints some information on a filtergraph, its filters, their connections and the media types of those connections.
For pure debugging purposes it is easier to just call DSFiltergraph.dumpGraph(true), which does almost the same, but does so
entirely on the native side (and prints to stderr only).
The way this demo does it, shows how to get all that information in java.
**/

public class GraphInfo {

   public GraphInfo() {

	    de.humatic.dsj.DSCapture graph = new de.humatic.dsj.DSCapture( DSFiltergraph.JAVA_POLL | DSFiltergraph.INIT_PAUSED, null);

		DSFilter[] filters = graph.listFilters();

        for (DSFilter f: filters) {

            System.out.println(f.getName());

            System.out.println("CLSID: "+f.getCLSID());

            dumpFilterInGraph("inputs", f.getInputs());

            dumpFilterInGraph("outputs", f.getOutputs());

            System.out.println("\n*********************************************************************\n");
        }

		graph.dispose();

        System.exit(0);
    }

    private void dumpFilterInGraph(String title, DSPin[] pins) {

        System.out.println(title);

        for (DSPin pin : pins) {

            DSPin connectedTo = pin.connectedTo();

            if (connectedTo == null) {

                System.out.println("     "+pin.getName());
                System.out.println("      -| none");

            } else {

                System.out.println("     "+pin.getName());
                int d = connectedTo.getDirection();
                System.out.println("      "+(d == 0 ? "<-   " : "->   ")+connectedTo);
                System.out.println("      Media type: "+ connectedTo.getConnectionMediaType());

            }
        }
    }

    public static void main(String[] args) {

        new GraphInfo();

    }


}
