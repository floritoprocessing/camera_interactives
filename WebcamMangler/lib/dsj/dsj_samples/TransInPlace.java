/**
dsj demo code.
You may use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
copyright 2009
N.Peters
humatic GmbH
Berlin, Germany
**/

import de.humatic.dsj.*;

/**
Shows how to modify data in the native filtergraph using the JTransInPlace filter. The sample assumes that the
transform filter will receive 24bit RGB video data. Depending on where the filter is inserted and what rendering
mode is used, this assumption may become invalid.
**/

public class TransInPlace implements java.beans.PropertyChangeListener {

	private DSFiltergraph graph;

	private boolean doTransform = true;

	public TransInPlace(){}

	public void buildGraph() {

		javax.swing.JFrame f = new javax.swing.JFrame("dsj - TransInPlace");

		graph = new DSMovie("media/2997HalfNTSC.asf", DSFiltergraph.DD7, this);

		graph.setLoop(true);

		DSFilter videoRenderer = graph.findRenderer(DSMediaType.MT_VIDEO);

		DSFilter.DSPin vrIn = videoRenderer.getPin(1, 0);

		DSFilter.DSPin srcOut = vrIn.connectedTo();

		graph.insertTransInPlaceFilter(srcOut.getFilter(), null, videoRenderer, new JTIP(), 0);

		try{ f.getContentPane().add(java.awt.BorderLayout.CENTER, graph.asComponent()); } catch (Exception e){}

		f.getContentPane().add(java.awt.BorderLayout.SOUTH, new SwingMovieController(graph));

		final javax.swing.JButton tf = new javax.swing.JButton("no transform");

		tf.addActionListener (new java.awt.event.ActionListener () {
				public void actionPerformed (java.awt.event.ActionEvent event) {
					doTransform = !doTransform;
					tf.setText((doTransform ? "no " : "")+"transform");
				}
			}
		);

		f.getContentPane().add(java.awt.BorderLayout.NORTH, tf);

		f.pack();

		f.setVisible(true);

		f.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

	}

	public void propertyChange(java.beans.PropertyChangeEvent pe) {

		switch(DSJUtils.getEventType(pe)) {

		}

	}

	public class JTIP extends de.humatic.dsj.JTransInPlaceFilter {

		private int[] dirty = new int[2];

		private int bytesPerSample = 3;

		public JTIP() {}

		public int[] transform(DSSampleBuffer buffer) {

			if (!doTransform) return null;

			/** This is not the proper way to do grayscale transforms...**/

			for (int i = 0; i < buffer.getSampleLength()-bytesPerSample; i+=bytesPerSample) {

				int pixelSum = 0;

				for (int j = 0; j < bytesPerSample; j++) pixelSum += (buffer.getSample()[i+j] & 0xFF);

				pixelSum /= bytesPerSample;

				for (int j = 0; j < bytesPerSample; j++) buffer.getSample()[i+j] = (byte)pixelSum;

			}

			/** entire sample changed **/

			dirty[1] = buffer.getSampleLength();

			return dirty;

		}

	}

	public static void main(String[] args){

		new TransInPlace().buildGraph();

	}

}
