/**
dsj demo code.
You may use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
copyright 2010
N.Peters
humatic GmbH
Berlin, Germany
**/

import de.humatic.dsj.*;



public class PlayDVD implements java.beans.PropertyChangeListener {

	private DSDvd dvd;

	private javax.swing.JFrame f;

	/**
	Rendering mode flags. Some decoders, subtitles etc. will only work with DirectShow renderer filters (VMR7 / 9 or EVR). See
	DSFiltergraph javadocs for more details on the matter.
	**/

	private int flags = DSFiltergraph.VMR9; //DSFiltergraph.DD7 | DSFiltergraph.YUV | DSFiltergraph.DVD_MENU_ENABLED;

	/**
	Video and Audio decoders. None of these is installed by dsj, you will need to check what is available on your system and
	adjust the names here.
	**/

	private DSFilterInfo[] videoDecoders = new DSFilterInfo[]{null,
															  DSFilterInfo.filterInfoForName("Elecard MPEG-2 Video Decoder SD"),
															  DSFilterInfo.filterInfoForName("Microsoft DTV-DVD Video Decoder"),
															  DSFilterInfo.filterInfoForName("CyberLink Video/SP Decoder (PDVD9)"),
															  DSFilterInfo.filterInfoForName("MainConcept (Demo) MPEG-2 Video Decoder")
														  	};

	private DSFilterInfo[] audioDecoders = new DSFilterInfo[]{null,
															  DSFilterInfo.filterInfoForName("Microsoft DTV-DVD Audio Decoder"),
															  DSFilterInfo.filterInfoForName("ffdshow audio decoder")
														  	};
	public PlayDVD() {}

	public void createGraph() {

		try{

			f = new javax.swing.JFrame(" dsj - play dvd");

			dvd = new DSDvd(/*path_to_ifo_directory,*/ 0, flags, videoDecoders[3], audioDecoders[0], this);

 			f.getContentPane().add(java.awt.BorderLayout.CENTER, dvd.asComponent());

			/** The SwingMovieController handles keyboard events for navigation using DSDvd.navigate(DSDvd.NAV_x). **/

			f.getContentPane().add(java.awt.BorderLayout.NORTH, new SwingMovieController(dvd));

			f.pack();

			DSJUtils.centerFrame(f, null);

			f.setVisible(true);

			dvd.dumpGraph(false);

			f.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

			dvd.asComponent().addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent me) {
					if (me.getButton() != 3) return;
					final javax.swing.JPopupMenu popMeUp = new javax.swing.JPopupMenu();
					popMeUp.setLightWeightPopupEnabled(false);

					final javax.swing.JMenuItem fs = popMeUp.add("Fullscreen");
					fs.addActionListener (new java.awt.event.ActionListener () {
						public void actionPerformed (java.awt.event.ActionEvent event) {
							dvd.goFullScreen(null, 0);
						}
					});

					popMeUp.addSeparator();

					String[] streams = dvd.getStreams(DSDvd.DVD_AUDIO);
					int selected = dvd.getSelectedStream(DSDvd.DVD_AUDIO);
					for (int i = 0; i < streams.length; i++) {
						final int ID = i;
						final javax.swing.JMenuItem mi = popMeUp.add("Audio "+streams[i]);
						if (i == selected) mi.setBackground(java.awt.Color.lightGray);
						mi.addActionListener (new java.awt.event.ActionListener () {
							public void actionPerformed (java.awt.event.ActionEvent event) {
								dvd.selectStream(DSDvd.DVD_AUDIO, ID);
							}
						});
					}
					streams = dvd.getStreams(DSDvd.DVD_SUBPICTURE);
					if (streams.length > 0) {
						popMeUp.addSeparator();
						selected = dvd.getSelectedStream(DSDvd.DVD_SUBPICTURE);
						for (int i = 0; i < streams.length; i++) {
							final int ID = i;
							final javax.swing.JMenuItem mi = popMeUp.add("Subpicture "+streams[i]);
							if (i == selected) mi.setBackground(java.awt.Color.lightGray);
							mi.addActionListener (new java.awt.event.ActionListener () {
								public void actionPerformed (java.awt.event.ActionEvent event) {
									dvd.selectStream(DSDvd.DVD_SUBPICTURE, ID);
								}
							});
						}
					}
					streams = dvd.getStreams(DSDvd.DVD_MENU_LANGUAGE);
					if (streams.length > 0) {
						popMeUp.addSeparator();
						selected = dvd.getSelectedStream(DSDvd.DVD_MENU_LANGUAGE);
						for (int i = 0; i < streams.length; i++) {
							final int ID = i;
							final javax.swing.JMenuItem mi = popMeUp.add("Menu language "+streams[i]);
							if (i == selected) mi.setBackground(java.awt.Color.lightGray);
							mi.addActionListener (new java.awt.event.ActionListener () {
								public void actionPerformed (java.awt.event.ActionEvent event) {
									dvd.setDefaultStream(DSDvd.DVD_MENU_LANGUAGE, ID, 0);
								}
							});
						}
					}

					popMeUp.addSeparator();

					final javax.swing.JMenuItem sb = popMeUp.add("Set bookmark");
					sb.addActionListener (new java.awt.event.ActionListener () {
						public void actionPerformed (java.awt.event.ActionEvent event) {
							try{ dvd.saveBookmark(null); } catch (Exception e){e.printStackTrace();}
						}
					});

					final javax.swing.JMenuItem rb = popMeUp.add("Restore bookmark");
					rb.addActionListener (new java.awt.event.ActionListener () {
						public void actionPerformed (java.awt.event.ActionEvent event) {
							try{ dvd.restoreBookmark(null); } catch (Exception e){e.printStackTrace();}
						}
					});

					if ((flags & (DSFiltergraph.VMR9 | DSFiltergraph.EVR | DSFiltergraph.DVD_MENU_ENABLED)) != 0) {

						popMeUp.addSeparator();

						final javax.swing.JMenu bm = new javax.swing.JMenu("Button mapping");
						popMeUp.add(bm);
						final javax.swing.JMenuItem bmd = bm.add("BM_DISPLAY");
						bmd.addActionListener (new java.awt.event.ActionListener () {
							public void actionPerformed (java.awt.event.ActionEvent event) {
								dvd.setButtonMapping(DSDvd.BM_DISPLAY);
							}
						});
						final javax.swing.JMenuItem bms = bm.add("BM_SOURCE");
						bms.addActionListener (new java.awt.event.ActionListener () {
							public void actionPerformed (java.awt.event.ActionEvent event) {
								dvd.setButtonMapping(DSDvd.BM_SOURCE);
							}
						});

					}

					popMeUp.show(dvd.asComponent(), me.getX(), me.getY());

				}
			});

		} catch (Exception e){}

	}



	public void propertyChange(java.beans.PropertyChangeEvent pe) {

		switch(DSJUtils.getEventType(pe)){

			case DSFiltergraph.GRAPH_EVENT:

				/** With DVDs you will get a lot of EC_DVD_... type events. Please refer to MSDN documentation for their
				parameters' meaning. You should especially look for EC_DVD_ERROR events, as the dreaded "Copy protection error
				resulting from the combination of DVDNavigator, video decoder and graphics hardware occured. Updating your graphics
				driver may help" (or the like), that you might know from WMP is signalled here.
				**/

				System.out.print("Graph Event: "+DSConstants.eventToString(DSJUtils.getEventValue_int(pe)));

				int[] ge = (int[])(pe.getOldValue());

				for (int i = 1; i < ge.length; i++) System.out.print(", param_"+i+": "+ge[i]);

				System.out.println("");

				break;

			case DSFiltergraph.GRAPH_ERROR:

				if (DSJUtils.getEventValue_int(pe) == DSJException.E_CANT_CONNECT) {

					System.out.println("oops, reconnection after format change failed");

					f.setVisible(false);

					f.dispose();

				}

				break;

		}
	}

	public static void main(String[] args){

		new PlayDVD().createGraph();

	}

}