/*
	dsj demo code

	� Copyright np 2006-11, humatic gmbh, berlin, germany. All rights reserved.

	This software is supplied to you by humatic
	("humatic") in consideration of your agreement to the following terms, and your
	use, installation, modification or redistribution of this software
	constitutes acceptance of these terms.  If you do not agree with these terms,
	please do not use, install, modify or redistribute this software.

	In consideration of your agreement to abide by the following terms, and subject
	to these terms, humatic grants you a personal, non-exclusive license, under humatic's
	copyrights in this original humatic software (the "humatic software"), to use,
	reproduce, modify and redistribute the humatic software, with or without
	modifications, in source and/or binary forms; provided that if you redistribute
	the humatic software in its entirety and without modifications, you must retain
	this notice and the following text and disclaimers in all such redistributions of
	the software.  Neither the name, trademarks, service marks or logos of
	humatic may be used to endorse or promote products derived from the
	software without specific prior written permission from humatic.  Except as
	expressly stated in this notice, no other rights or licenses, express or implied,
	are granted by humatic herein, including but not limited to any patent rights that
	may be infringed by your derivative works or by other works in which the humatic
	software may be incorporated.

	The software is provided by humatic on an "AS IS" basis.  HUMATIC MAKES NO
	WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
	WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
	PURPOSE, REGARDING THE HUMATIC SOFTWARE OR ITS USE AND OPERATION ALONE OR IN
	COMBINATION WITH YOUR PRODUCTS.

	IN NO EVENT SHALL HUMATIC BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
	GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
	ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION, MODIFICATION AND/OR DISTRIBUTION
	OF THE HUMATIC SOFTWARE, HOWEVER CAUSED AND WHETHER UNDER THEORY OF CONTRACT, TORT
	(INCLUDING NEGLIGENCE), STRICT LIABILITY OR OTHERWISE, EVEN IF HUMATIC HAS BEEN
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

import de.humatic.dsj.*;
import de.humatic.dsj.com.*;

/*
Shows usage of DirectX Media Object (DMO) DSPs for audio and video manipulation. Video DMOs are only available
on Vista and beyond.
*/

public class DMO_FX implements java.beans.PropertyChangeListener {

	private DSFiltergraph dsfg;

	private boolean audio = true;

	public DMO_FX(){

		try{

			DSFilter[] filters = null;

			if (audio) {

				dsfg = new DSMovie("media/Audio.mp3", DSFiltergraph.DD7, this);

				filters = dsfg.listFilters();

				DSFilter peq = dsfg.insertFilter(filters[2], filters[3], DSFilterInfo.filterInfoForName("ParamEq"));

				DSFilter peq2 = dsfg.insertFilter(peq, filters[3], DSFilterInfo.filterInfoForName("ParamEq"));

				DSFilter peq3 = dsfg.insertFilter(peq2, filters[3], DSFilterInfo.filterInfoForName("ParamEq"));

			} else {

				dsfg = new DSMovie("media/2997HalfNTSC.asf", DSFiltergraph.DD7, this);

				filters = dsfg.listFilters();

				DSFilter ColorControl = dsfg.insertFilter(filters[1], filters[3], DSFilterInfo.filterInfoForName("Color Control"));

			}

			javax.swing.JFrame f = new javax.swing.JFrame();

			javax.swing.JPanel ctrl = new javax.swing.JPanel(new java.awt.GridLayout(1, 0));

			filters = dsfg.listFilters();

			for (int j = 0; j < filters.length; j++) {

				System.out.println(filters[j]);

				try{

					final DMO dmo = (DMO)(COMFactory.queryInterface(dsfg, filters[j], COMFactory.IID_IMediaObject));

					final DMO.DMOParameterInfo[] pInfos = dmo.getParameterInfo();

					if (pInfos.length == 0) continue;

					java.awt.Font titleFont = new java.awt.Font("Helvetica", 0, 8);

					final javax.swing.JPanel jp = new javax.swing.JPanel(new java.awt.GridLayout(1, 0));

					jp.setBorder(new javax.swing.border.TitledBorder(filters[j].getName()));

					for (int i = 0; i < pInfos.length; i++) {

						final int id = i;

						String pName = pInfos[i].getName();

						if (pInfos[i].getCaps() == DMO.READ_ONLY) {

							jp.add(new javax.swing.JLabel(pName+": "+dmo.getParameter(id)));

						} else if (pInfos[i].getType() == DSConstants.VT_BOOL || pInfos[i].getType() == DMOAudioEffect.MPT_BOOL) {

							final javax.swing.JCheckBox cb = new javax.swing.JCheckBox(pName);
							cb.setToolTipText(pInfos[i].getName());
							cb.setFont(titleFont);
							cb.addActionListener(new java.awt.event.ActionListener() {
								public void actionPerformed(java.awt.event.ActionEvent ae){

									dmo.setParameter(id, cb.isSelected() ? 1 : 0);

								}
							});

							cb.setBorder(new javax.swing.border.TitledBorder(pName));

							jp.add(cb);

						} else {

							final javax.swing.JSlider slider = new javax.swing.JSlider(javax.swing.JSlider.VERTICAL, (int)(pInfos[i].getMinimum()), (int)(Math.abs(pInfos[i].getMaximum())), (int)(pInfos[i].getDefault()));
							slider.setToolTipText(pInfos[i].getName());
							javax.swing.border.TitledBorder title = new javax.swing.border.TitledBorder(pName);
							title.setTitleFont(titleFont);
							slider.setBorder(title);

							slider.setPreferredSize(new java.awt.Dimension(60, 200));

							slider.addChangeListener(new javax.swing.event.ChangeListener() {
								public void stateChanged(javax.swing.event.ChangeEvent ev){

									dmo.setParameter(id, (float)(slider.getValue()));

								}

							});

							jp.add(slider);

						}



					}

					ctrl.add(jp);

				} catch (Exception ex){ ex.printStackTrace(); }

			}

			try{ f.getContentPane().add(java.awt.BorderLayout.CENTER, dsfg.asComponent()); } catch (Exception ex){}

			f.getContentPane().add(java.awt.BorderLayout.SOUTH, ctrl);

			f.getContentPane().add(java.awt.BorderLayout.NORTH, new SwingMovieController(dsfg));

			f.pack();

			f.setVisible(true);

			f.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

			dsfg.setRate(1);

			dsfg.setLoop(true);

		} catch (Exception e){e.printStackTrace();}

	}

	public void propertyChange(java.beans.PropertyChangeEvent pe) {

		switch (DSJUtils.getEventType(pe)) {


		}

	}

	public static void main(String[] args){

		new DMO_FX();

	}

}