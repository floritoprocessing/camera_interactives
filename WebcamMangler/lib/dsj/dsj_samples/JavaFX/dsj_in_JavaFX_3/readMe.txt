Before running this you might need to "add a library" to your NetBeans installation.

After building you need to "jar up" the dll and modify the generated jnlp files if you want to deploy from a webserver.
Namely change the codebase parameters and change the last "<jar href..." entry from
<jar href="lib/libdsj.jar"/>
to
<nativelib href="lib/libdsj.jar"/>

example:

<?xml version="1.0" encoding="UTF-8"?>
<jnlp spec="1.0+" codebase="http://www.humatic.de/htools/dsj/fx/" href="dsjFX.jnlp">
    <information>
        <title>dsjFX</title>
        <vendor>humatic</vendor>
        <homepage href="http://www.humatic.de/htools/dsj/fx/"/>
        <description>dsjFX</description>
        <offline-allowed/>
        <shortcut>
            <desktop/>
        </shortcut>
    </information>
    <security>
        <all-permissions/>
    </security>
    <resources>
        <j2se version="1.5+"/>
        <property name="jnlp.packEnabled" value="true"/>
        <property name="jnlp.versionEnabled" value="true"/>
        <extension name="JavaFX Runtime" href="http://dl.javafx.com/javafx-rt.jnlp"/>
        <jar href="dsjFX.jar" main="true"/>
        <jar href="lib/dsj.jar"/>
        <nativelib href="lib/libdsj.jar"/>
    </resources>
    <application-desc main-class="dsjFX.Main"/>
</jnlp>