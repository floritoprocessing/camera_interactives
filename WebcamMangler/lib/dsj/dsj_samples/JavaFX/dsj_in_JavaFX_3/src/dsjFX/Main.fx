
package dsjFX;

import javafx.lang.FX;
import javafx.stage.Stage;
import javafx.scene.Scene;

/**
@author np

dsj demo code.
You may use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
copyright 2009
N.Peters
humatic GmbH
Berlin, Germany

Updated for JavaFX 1.2, Sep.09
This is still mainly about experimenting with dsj in JavaFX context and definetely not meant to
show best practices in JavaFX. Especially the swapping of stage content can sure be done in
some smarter way.
**/

var vidWidth = 320;
var vidHeight = 240;

var dsjPlayer = new dsjFXAdapter();

var stage = Stage {
    title: "dsj in javaFX, Pt.3"
    visible: true
    scene: Scene{
        width: vidWidth
        height: vidHeight + 28
        content: bind dsjPlayer.asNode()
    } 
}
/**
 This (unlike Stage's onClose handler or any other applet lifecycle function as it seems)
 is called when the webpage containing the applet is reloaded. We essentially
 need to track that and close opened graphs. Failure to do so would for example
 leave you with a capture device still being active in the previous - then zombified -
 instance and thus unavailable for future use.
 **/
FX.addShutdownAction(
    function() {
        dsjPlayer.shutDown();
    }
);

