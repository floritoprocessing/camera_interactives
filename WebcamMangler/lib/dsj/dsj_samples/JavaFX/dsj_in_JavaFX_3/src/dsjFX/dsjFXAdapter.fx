package dsjFX;

import java.lang.Exception;

import javafx.ext.swing.*;
import javafx.scene.*;
import javafx.scene.layout.*;

import de.humatic.dsj.*;
import de.humatic.dsj.src.*;
import java.awt.Dimension;

/**
 * @author np
 * Sep.2009, updated for JavaFX 1.2.
 * Code is probably an overcomplicated mess by now. I'm not getting friends
 * with JavaFX really and for sure not with Netbeans...
 */

public class DSJContainer extends SwingComponent{
    var panel: javax.swing.JPanel;
    public override function createJComponent():javax.swing.JComponent{
        panel = new javax.swing.JPanel(new java.awt.GridLayout(1,0));
        panel.setBackground(java.awt.Color.black);
        return panel;
    }
    postinit{
       try {panel.add(player.asComponent()); } catch ( e : Exception ) { println(e);}
    }
}

class Unlink extends java.lang.Runnable {
   public override function run():Void{
      showSelector();
   }
}

public class dsjFXAdapter extends java.beans.PropertyChangeListener {

    var xpos: Number;
    var ypos: Number;
    var flags: Integer;
    var moviePath: String;
    var appURL: String;
    var resource: String;
    var player: DSFiltergraph;
    var netSrc: NetworkSource;
    var dsjPanel: DSJContainer;
    var cont = new TopContainer;
    var selector = new Selector;
    var displayBox: VBox;
    var controller = new fxController;
    
    override public function propertyChange(e:java.beans.PropertyChangeEvent)  {
       if (DSJUtils.getEventType(e) == DSFiltergraph.DONE) {
           if (player.isFullScreen()) {
               player.leaveFullScreen();
           }
           javax.swing.SwingUtilities.invokeLater(Unlink {});
       } else if (DSJUtils.getEventType(e) == DSFiltergraph.BUFFER_COMPLETE) {

       }
    }

    public function buildStage() {

        /* to avoid 1.2 WARNING*WARNINGs*/
        try{ delete displayBox from cont.content; } catch ( e : Exception ) { println(e);}
        try{ delete dsjPanel from displayBox.content; } catch ( e : Exception ) { println(e);}
        try{ delete controller.asNode() from displayBox.content; } catch ( e : Exception ) { println(e);}

        dsjPanel = DSJContainer {
                    width: 320
                    height: 240
                }

        displayBox = VBox{ content: [
               dsjPanel,
               controller.asNode()
            ]
        }
        cont.content = displayBox;
    }
    
    public function asNode(): Node {
        selector.setApp(this);
        controller.setApp(this);
        controller.setBounds(320, 30);
        cont.content = selector;
        var gr = Group {

            content: bind cont
                        
        }

        return gr;
    }

    public function createMovie(userChoice: Integer) {
        if (player != null) {
            controller.graphClosed();
            player.dispose();
        }
        //DSEnvironment.setDebugLevel(3);
        if(userChoice == 0) {
            flags = DSFiltergraph.J2D + DSFiltergraph.FRAME_CALLBACK; /** no | & << >> in javFX? **/
            moviePath = FX.getArgument("mmsPath") as String;
            try{
               player = new DSMovie(moviePath, flags, null);
            } catch (Exception){
                player = new DSMovie("http://www.humatic.de/htools/dsj/applet/logoani.asf", flags, null);
            }
            player.asComponent().setPreferredSize(new Dimension(320, 240));
            buildStage();
       } else if (userChoice == 1) {
            flags = DSFiltergraph.J2D + DSFiltergraph.FRAME_CALLBACK; 
            moviePath = FX.getArgument("rtmpPath") as String;
            if (isApplet == false){ moviePath =  "rtmp://fcvod.mgnetwork.com/mgeneral/flash/vams/SLS?de4161fa0331102cad4f001ec92a4a0d.flv";}
            appURL = moviePath.substring(0, moviePath.indexOf("?"));
            resource = moviePath.substring(moviePath.indexOf("?") + 1);
            netSrc = new RTMPSource(appURL, resource, 0, null);
            player = netSrc.createGraph(flags);
            buildStage();
       } else if (userChoice == 2){
            showDeviceSelector();
            return;
        }
        controller.setFiltergraph(player);
        player.addPropertyChangeListener(this);
    }

    public function showSelector() {
        /* to avoid 1.2 WARNING*WARNINGs*/
        try{ delete displayBox from cont.content; } catch ( e : Exception ) { println(e);}
        try{ delete dsjPanel from displayBox.content; } catch ( e : Exception ) { println(e);}
        try{ delete controller.asNode() from displayBox.content; } catch ( e : Exception ) { println(e);}

        player.dispose();
        cont.content = selector;
        player = null;
    }
    function showDeviceSelector() {

        var ds: DeviceSelector = new DeviceSelector;
        ds.setApp(this);
        cont.content = ds;

    }

    public function openCapture(vDev: DSFilterInfo , aDev: DSFilterInfo) {
        var wantedIndex: Integer;
        /** Two ways to set up video capture with a specified format. See non javaFX demos for more details. **/
        try{
            for (mt in (vDev.getDownstreamPins()[0]).getFormats()) {
                println(mt);
                 if (mt.getWidth() == 352) {
                    /** Preconfigure DSFilterInfo with desired format **/
                    //vDev.getDownstreamPins()[0].setPreferredFormat(wantedIndex);
                    break;
                }
                wantedIndex++;
            }
        } catch (e : Exception){}

        flags = DSFiltergraph.J2D;
        player = new DSCapture(flags, vDev, false, aDev, this);
        
        /** Change output format after construction **/
        //(player as DSCapture).getActiveVideoDevice().setOutputFormat((player as DSCapture).getActiveVideoDevice().getDeviceOutput(0), wantedIndex);
        
        player.asComponent().setPreferredSize(new Dimension(320, 240));
        controller.setFiltergraph(player);
        buildStage();

    }

    public function getWidth(): Number {
        return player.getMediaDimension().getWidth();

    }
    public function getHeight(): Number { 
        return player.getMediaDimension().getHeight();

    }

    public function getFiltergraph() : DSFiltergraph { 
        return player;

    }

    public function shutDown() : Void {
        if (player != null) player.dispose();
    }

    var isApplet = "true".equals(FX.getArgument("isApplet") as String);
    var printDebug = "true".equals(FX.getArgument("debug") as String);


}

