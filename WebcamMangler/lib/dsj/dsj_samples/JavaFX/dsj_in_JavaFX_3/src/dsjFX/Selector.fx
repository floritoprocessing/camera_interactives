/*
 * Selector.fx
 *
 * Created on 02.03.2009, 13:20:54
 */

package dsjFX;

import javafx.stage.*;
import javafx.ext.swing.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import javafx.scene.layout.*;
import javafx.scene.text.*;
import javafx.scene.input.MouseEvent;
import javafx.animation.transition.*;

/**
 * @author np
 */

public class Selector extends CustomNode {

    var dsjLink: dsjFXAdapter;
    var pmms: SwingButton = SwingButton {
        //focusable: false
        translateX: bind (320 - pmms.layoutBounds.width) / 2

        text: " play movie "
        action: function() {
            dsjLink.createMovie(0);
        }
        onMouseEntered: function (e: MouseEvent) {
            rotTransition.playFromStart();
            scaleTransition.playFromStart();
        }
    }
    var prtmp: SwingButton = SwingButton {
        //focusable: false
        translateX: bind (320 - prtmp.layoutBounds.width) / 2
        text: " play RTMP stream "
        action: function() {
            dsjLink.createMovie(1);
        }
        onMouseEntered: function (e: MouseEvent) {
             rotTransition.playFromStart();
             scaleTransition.playFromStart();
        }
    }


    var oc: SwingButton = SwingButton {
        //focusable: false
        translateX: bind (320 - oc.layoutBounds.width) / 2
        text: " open capture device "
        action: function() {
            dsjLink.createMovie(2);
        }
        onMouseEntered: function (e: MouseEvent) {
            rotTransition.playFromStart();
            scaleTransition.playFromStart();
        }
    }

    var H = new H;
    var rotTransition = RotateTransition {
        duration: 1.6s
        node: H
        byAngle: 180
        repeatCount: 1
    };
    var scaleTransition = ScaleTransition {
        duration: 200ms
        node: H
        byX: 1.5
        byY: 1.5
        fromX: 1
        fromY: 1
        toX: 1.2
        toY: 1.2
        repeatCount: 8
        autoReverse: true
    }
    var me: MouseEvent;
    var grX = bind me.x;
    var grY = bind me.y;

    override protected function create() : Node {

        Group {
            content: [
                Rectangle {
                    width: 320
                    height: 268
                    fill: RadialGradient {
                        centerX: 140,
                        centerY: 100,
                        radius: 300,
                        proportional: false
                        stops: [
                            Stop {
                                offset: 0.0
                                color: Color.GRAY
                            },
                            Stop {
                                offset: 1.0
                                color: Color.BLACK
                            }
                        ]
                    }

                }
                H,
                VBox{
                    spacing: 11
                    translateY: 38
                    content: [
                        pmms,
                        Text {
                            fill: Color.LIGHTGRAY
                            translateX: 85
                            content: "(No further requirements)"
                        }
                        prtmp,
                        Text {
                            translateX: 64
                            fill: Color.LIGHTGRAY
                            content: "(Requires mp4 decoder, ffdShow..)"
                        }
                        oc,
                        Text {
                            translateX: 48
                            fill: Color.LIGHTGRAY
                            content: "(Requires audio & video capture devices)"
                        }

                    ]
                }
            ]
            
        }
        

    }

    public function setApp(da : dsjFXAdapter){
        dsjLink = da;
    }

}

class H extends CustomNode{
        override function create():Node {
        return Group {
            content: [
               Rectangle {
                    width: 65
                    height: 208
                    x: 50
                    y: 30
                    fill: Color.rgb(23,23,23)
                }
                Rectangle {
                    width: 65
                    height: 208
                    x: 200
                    y: 30
                    fill: Color.rgb(23,23,23)
                }
                Rectangle {
                    width: 140
                    height: 65
                    x: 100
                    y: 101
                    fill: Color.rgb(23,23,23)
                }
                Rectangle {
                    width: 30
                    height: 85
                    x: 145
                    y: 5
                    fill: Color.rgb(23,23,23)
                }
                Rectangle {
                    width: 30
                    height: 85
                    x: 145
                    y: 178
                    fill: Color.rgb(23,23,23)
                }
            ]
        }
    }
  }
