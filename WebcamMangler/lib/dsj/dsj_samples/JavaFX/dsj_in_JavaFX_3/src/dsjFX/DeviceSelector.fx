/*
 * DeviceSelector.fx
 *
 * Created on 04.03.2009, 10:40:10
 */

package dsjFX;

import javafx.scene.CustomNode;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.layout.*;
import javafx.scene.paint.*;
import javafx.scene.text.*;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;
import javafx.ext.swing.SwingCheckBox;
import javafx.ext.swing.SwingComboBox;
import javafx.ext.swing.SwingComboBoxItem;
import javafx.ext.swing.SwingButton;

import de.humatic.dsj.*;

/**
 * @author np
 */
var videoCaptureDevs: DSFilterInfo[];
var audioCaptureDevs: DSFilterInfo[];
var selVDev: DSFilterInfo;
var selADev: DSFilterInfo;
var vds : SwingComboBox;
var ads : SwingComboBox;
var i : Integer;
var dsjLink : dsjFXAdapter;

public class DeviceSelector extends CustomNode {

    
    function getDevices() {
        /**
        * cant use 2dimensional arrays in .fx, or at least I have no idea how to do it
        * The interesting classes (Sequence et.all) are once again proprietary undocumented com.sun... stuff.
        * Note that - other than DSCapture queryDevices() - this way of listing
        * capture devices does not filter out currently unavailable USB devices.
        **/
        videoCaptureDevs = DSEnvironment.getFilters(DSEnvironment.CLSID_VideoInputDeviceCategory);
        vds = SwingComboBox {
            //focusable: false
            translateX: bind (320 - vds.layoutBounds.width) / 2
            items: [
                for (vDev in videoCaptureDevs) {
                    SwingComboBoxItem {
                        text: vDev.getName()
                        selected: i == 0
                    }
                }
            ]
        }
        i = 0;
        audioCaptureDevs = DSEnvironment.getFilters(DSEnvironment.CLSID_AudioInputDeviceCategory);
        ads = SwingComboBox {
            //focusable: false
            translateX: bind (320 - ads.layoutBounds.width) / 2
            items: [
                for (aDev in audioCaptureDevs) {
                    SwingComboBoxItem {
                        text: aDev.getName()
                        selected: i == 0
                    }
                }
            ]
        }

    }
    
    public function setApp(dsjApp : dsjFXAdapter) {

        dsjLink = dsjApp;

    }
    

    public override function create(): 
        Node {
        getDevices();
        return Group {
            content: [
                Rectangle {
                    width: 320,
                    height: 268
                    fill: RadialGradient {
                        centerX: 140,
                        centerY: 100,
                        radius: 300,
                        proportional: false
                        stops: [
                            Stop {
                                offset: 0.0
                                color: Color.GRAY
                            },
                            Stop {
                                offset: 1.0
                                color: Color.BLACK
                            }
                        ]
                    }
                }
                VBox{
                    spacing: 20
                    translateY: 40
                    content: [
                        vds,
                        ads,
                        SwingButton {
                            //focusable: false
                            translateX: 135
                            translateY: 25
                            text: " go "
                            action: function() {
                                dsjLink.openCapture(videoCaptureDevs[vds.selectedIndex], audioCaptureDevs[ads.selectedIndex])
                            }
                        }

                    ]
                }
            ]
        };
    }

}
