
package dsjFX;

import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import javafx.scene.layout.*;

import javafx.stage.*;
import javafx.ext.swing.*;

import de.humatic.dsj.*;
import javafx.scene.transform.Translate;

/**
 * @author np
 */

var buttonGroup: Node;
var graph: DSFiltergraph;
var movDur: Number;
var ctrlW: Number = 360;
var ctrlH: Number = 30;
var time : Number;
var firstRun: Boolean;
var dsjLink : dsjFXAdapter;

package class fxController extends java.beans.PropertyChangeListener {

    var slider = new fxSlider;
    
    var ret: SwingButton = SwingButton {
        //focusable: false
        text: "<"
        action: function() {
            dsjLink.showSelector();
        }
    }

    var ppb: SwingButton = SwingButton {
        //focusable: false
        text: " > "
        action: function() {
            run();
            if (graph.getRate() != 0) {
                ppb.text = "||";

            }
            else {
                ppb.text =">";

            }
        }
    }
    var mb: SwingButton = SwingButton {
        //focusable: false
        text: " M "
        action: function() {
            if (graph.getVolume() != 1) {
                graph.setVolume(1);
                mb.text = "M";
            } else {
                graph.setVolume(0);
                mb.text ="U";
            }
        }
    }


    var fs: SwingButton = SwingButton {
        //focusable: false
        text: "F"
        action: function() {
            graph.goFullScreen(java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice(), 0);
        }
    }

    override public function propertyChange(e:java.beans.PropertyChangeEvent) {
        if (DSJUtils.getEventType(e) == DSFiltergraph.FRAME_NOTIFY) {
            //slider.setValue(DSJUtils.getEventValue_int(e));
            time = DSJUtils.getEventValue_int(e);
            javax.swing.SwingUtilities.invokeLater(UnlinkCtrl{});
        } 
    }

    public function asNode(): Node {
        if (buttonGroup != null) {
            return buttonGroup;
        } else {
              buttonGroup = createBG();
              return buttonGroup;
        }
    }

    function createBG(): Node {
        Group {
            content: [
                Rectangle {
                    width: ctrlW
                    height: ctrlH
                }
                Rectangle {
                    width: ctrlW
                    height: ctrlH
                    arcWidth: 10
                    arcHeight: 10
                    fill: LinearGradient {
                        startX: 0
                        startY: 0
                        endX: 0
                        endY: 1
                        stops: [
                            Stop {
                                offset: 0.0
                                color: Color.rgb(52, 52, 52)
                            },
                            Stop {
                                offset: 0.6
                                color: Color.rgb(115, 115, 115)
                            },
                            Stop {
                                offset: 1.0
                                color: Color.rgb(124, 124, 124)
                            },
                        ]
                    }
                    stroke: LinearGradient {
                        startX: 0
                        startY: 0
                        endX: 0
                        endY: 1
                        stops: [
                            Stop {
                                offset: 0.0
                                color: Color.rgb( 15,  15,  15)
                            },
                            Stop {
                                offset: 1.0
                                color: Color.rgb(224, 224, 224)
                            },
                        ]
                    }
                },
                HBox{
                    spacing: 2
                    content: [
                        HBox{
                            spacing: 2
                            content: [
                                ret,
                                ppb,
                            ]
                        }
                        VBox{
                            spacing: 10
                            content: [
                                Rectangle {
                                    width: 1
                                    height: 1
                                }
                                slider
                            ]
                        }
                        HBox{
                            spacing: 2
                            content: [
                                mb,
                                fs,
                            ]
                        }
                    ]
                }
            ]
        }
    }

    public function setFiltergraph(dsfg : DSFiltergraph) {

        graph = dsfg;
        movDur = graph.getDuration();
        if (movDur > 0) {
            graph.addPropertyChangeListener(this);
        }
        graph.setVolume(1);
    }

    /** 
     * ugly hack used a couple of times in this sample, overcoming the
     * oddities of how constructors are working in JavaFX. Maybe I just 
     * missed something...
     **/
    public function setApp(dsjApp : dsjFXAdapter) {

        dsjLink = dsjApp;

    }

    public function graphClosed(){

    }

    public function setBounds(cw : Number, ch : Number) {
        ctrlW = cw;
        ctrlH = ch;
        slider.width =  
        cw - 170 as Integer;
    }

    public function run() {
        if (graph.getRate() == 0) {
            graph.setRate(1);
        }
        else graph.setRate(0);
    }

}
class UnlinkCtrl extends java.lang.Runnable {
   public override function run():Void{
      slider.setValue(time);
   }
}
def thumbWidth = 20;
def thumbHeight = 12;

/**
slider code borrowed from JavaFX sample "Effects Playground", http://javafx.com/samples/EffectsPlayground/
copyright C.Campbell, Sun Microsystems 2008
**/

class fxSlider extends CustomNode {

    public var value = 0.0;
    public var minimum = 0.0;
    public var maximum = 1.0;

    public var width = 240;
    var adjValue = bind (value - minimum) / (maximum - minimum);
    var origValue = 0.0;

    var thumb = SliderThumb {
                    translateX: 0; //bind (adjValue * width) - (thumbWidth / 2)
                    translateY: -2
                    onMousePressed: function(e) {
                        origValue = adjValue;
                    }
                    onMouseDragged: function(e) {
                        var v = origValue + (e.dragX / width);
                        if (v < 0) {
                            v = 0;
                        } else if (v > 1) {
                            v = 1;
                        }
                        graph.setTimeValue(movDur*v);
                        value = minimum + (v * (maximum-minimum));
                    }
                }

    override protected function create() : Node {
        Group {
            content: [
            Rectangle {
                width: bind width
                height: 8
                arcWidth: 10
                arcHeight: 10
                fill: LinearGradient {
                    startX: 0
                    startY: 0
                    endX: 0
                    endY: 1
                    stops: [
                        Stop { offset: 0.0 color: Color.rgb(172, 172, 172) },
                        Stop { offset: 0.6 color: Color.rgb(115, 115, 115) },
                        Stop { offset: 1.0 color: Color.rgb(124, 124, 124) },
                    ]
                }
                stroke: LinearGradient {
                    startX: 0
                    startY: 0
                    endX: 0
                    endY: 1
                    stops: [
                        Stop { offset: 0.0 color: Color.rgb( 15,  15,  15) },
                        Stop { offset: 1.0 color: Color.rgb(224, 224, 224) },
                    ]
                }
            },
            thumb
            ]
        }
    }

    function setValue(val : Integer) {
        thumb.translateX = ((val/movDur * width) - (thumbWidth / 2)) - thumb.layoutBounds.minX;
    }
}
class SliderThumb extends CustomNode {

    override protected function create() : Node {
        Group {
            cursor: Cursor.HAND
            content: [
            Rectangle {
                width: thumbWidth
                height: thumbHeight
                arcWidth: 7
                arcHeight: 7
                fill: LinearGradient {
                    startX: 0
                    startY: 0
                    endX: 0
                    endY: 1
                    stops: [
                        Stop { offset: 0.0  color: Color.rgb(107, 107, 107) },
                        Stop { offset: 0.55 color: Color.BLACK },
                        Stop { offset: 0.7  color: Color.rgb( 75,  75,  75) },
                        Stop { offset: 1.0  color: Color.rgb( 23,  23,  23) },
                    ]
                }
                stroke: LinearGradient {
                    startX: 0
                    startY: 0
                    endX: 0
                    endY: 1
                    stops: [
                        Stop { offset: 0.0 color: Color.rgb(  3,   3,   3) },
                        Stop { offset: 1.0 color: Color.rgb( 82,  82,  82) },
                    ]
                }
                strokeWidth: 1
            },
            Polygon {
                translateX: thumbWidth/2-3.8
                translateY: thumbHeight/2
                points: [0, 0, 3, -3, 3, 3]
                fill: Color.WHITE
            },
            Polygon {
                translateX: thumbWidth/2+1.8
                translateY: thumbHeight/2
                points: [3, 0, 0, -3, 0, 3]
                fill: Color.WHITE
            },
            ]
        }
    }
}

