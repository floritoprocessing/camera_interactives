These are some pretty basic examples, dating back to the 0_8_2 version, of how dsj MAY be integrated with JMF, mostly based on code from Sun's examples and potentially full of stuff, that an experienced JMF user can do a lot better. Code in this folder is not considered to be part of dsj and we do not generally offer support for it. Please review the code thoroughly and change it to your needs if you want to use it. 

Compile and run dsJMFDemo.java
The supplied sources set up a custom DataSource and Player, register package prefix and protocol with JMF and create a realized player through JMF's Manager.

There are two more DataSources in de.humatic.media.protocol.dsj. (Push & Pull) that will also enable the Manager to create a processor, see the supplied copy of Sun's AVTransmit2. This uses the new setup flag (JAVA_POLL_RGB) to make the native code send RGB rather than BGR data. See the dsj readme.
It is essential that DataSources and Handler remain in the media.content.dsj, resp. media.protocol.dsj packages and remain named as they are. You can change the de.humatic if you want to.
