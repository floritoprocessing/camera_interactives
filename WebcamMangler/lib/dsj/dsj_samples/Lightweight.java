/**
dsj demo code.
You may use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
copyright 2005-8
N.Peters
humatic GmbH
Berlin, Germany
**/

import de.humatic.dsj.*;



public class Lightweight implements java.beans.PropertyChangeListener {

	private DSFiltergraph movie;

	private boolean heavyweight = false;

	public Lightweight() {}

	public void createGraph() {

		javax.swing.JFrame f = new javax.swing.JFrame("dsj - lightweight renderer");

		java.awt.FileDialog fd = new java.awt.FileDialog(f, "select movie", java.awt.FileDialog.LOAD);

		fd.setVisible(true);

		if (fd.getFile() == null) return;

		f.getContentPane().add(java.awt.BorderLayout.CENTER, new javax.swing.JScrollPane(createMoviePlayerPanel(fd.getDirectory()+fd.getFile())));

		f.setPreferredSize(new java.awt.Dimension(600, 400));

		f.getContentPane().add(java.awt.BorderLayout.NORTH, new javax.swing.JComboBox(new String[]{"a swing component", "2", "3", "4"}));

		f.getContentPane().add(java.awt.BorderLayout.SOUTH, new javax.swing.JLabel("OK, you knew that before, this is just another swing component", javax.swing.SwingConstants.CENTER));

		f.getContentPane().add(java.awt.BorderLayout.WEST, new javax.swing.JLabel(" and another swing component "));

		f.getContentPane().add(java.awt.BorderLayout.EAST, new javax.swing.JLabel(" yet another swing component "));

		f.pack();

		f.setVisible(true);

		/**
		Don't do this at home. This demo relies on dsj closing and disposing off filtergraphs when the JVM exits. This is
		OK for a "open graph, do something & exit" style demo, but real world applications should take care of calling
		dispose() on filtergraphs they're done with themselves.
		**/

		f.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

	}

	public javax.swing.JPanel createMoviePlayerPanel(String fileName) {

		movie = new DSMovie(fileName, DSFiltergraph.J2D, this);

		movie.setLoop(true);

		javax.swing.JPanel panel = new javax.swing.JPanel(new java.awt.BorderLayout());

		panel.add(java.awt.BorderLayout.CENTER, movie.asComponent());

		panel.add(java.awt.BorderLayout.SOUTH, new SwingMovieController(movie));

		return panel;
	}

	public void propertyChange(java.beans.PropertyChangeEvent pe) {

	}

	public static void main(String[] args){

		new Lightweight().createGraph();

	}

}