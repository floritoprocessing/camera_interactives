/**
dsj demo code.
You may use, modify and redistribute this code under the terms laid out in Disclaimer.txt and in the header of the DSJDemo application.
copyright 2008
N.Peters
humatic GmbH
Berlin, Germany
**/

import de.humatic.dsj.*;
import de.humatic.dsj.src.*;

/**
Shows the use of the de.humatic.dsj.src.AsyncSource class to play movies from inside a jar. DSMovie can not do this, because
the native DirectShow filters that it uses to open the file have no idea of jars and don't know how to interpret a URL
like: "jar:file:/home/duke/duke.jar!/";
If you have java on the path you can use the "buildMovieFromJar.BAT" script to compile, jar and run this sample (alternatively
perform the build steps listed in the batch script manually or from within your IDE).
"MovieFromJar.jar" is the built output of the above. You should be able to doubleclick it to try the demo.
The movies the code refers to can be extracted from said jar.
**/

public class MovieFromJar extends javax.swing.JFrame {

	public MovieFromJar() {

		super("dsj - MovieFromJar");

	}

	public void createGraph() {

		Object[] options = {"AVI", "MPG"};

		int subType = javax.swing.JOptionPane.showOptionDialog(this, "         select stream type ",
															" dsj async source",
															javax.swing.JOptionPane.YES_NO_OPTION,
															javax.swing.JOptionPane.QUESTION_MESSAGE,
															null,
															options,
															options[0]
												  );
		java.net.URL jarURL = null;

		if (subType == 0) jarURL = getClass().getResource("/jarMov/Avi.avi");

		else if (subType == 1) jarURL = getClass().getResource("/jarMov/Mpeg.mpg");

		try{

			AsyncSource as = new AsyncSource(jarURL, null);

			DSGraph graph = as.createGraph(DSFiltergraph.DD7);

			add(graph.asComponent());

		} catch (Exception e){ System.out.println("Exception occured: "+e.toString()); }

		setTitle("playing: "+jarURL);

		pack();

		setVisible(true);

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

	}

	public static void main(String[] args) {

		new MovieFromJar().createGraph();

	}

}