/**
dsj demo code.
You may use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
copyright 2009
N.Peters
humatic GmbH
Berlin, Germany
**/

import de.humatic.dsj.*;

import javax.swing.*;

/**
Updated for 0_8_6

Demonstrates changing formats delivered by a capture device before and after the filtergraph is built.
Configuration before building the graph is done via a ready made dialog, whose source-code can be found in the
main dsj demo.

dsj 0_8_51 adds some missing functionality to address single output pins on capture devices with separate capture
and preview pins. This makes things a little more complex, but finally also takes hardware reality into account
after the graph is built.
0_8_6 intoduces the setCropSize method, which enables fine grain control of output sizes with devices, that
otherwise only offer limited choice of dimensions with their supported formats. This mostly applies to the more
professional capture boards and cameras. For example Viewcast Osprey boards or IDS Ueye cameras will only have one
dimension to select from, but then can shrink and / or crop from that. There is no clear standard, so the
implementation is driver specific.
Also new in 0_8_6 are api methods to configure analog video digitizers.
**/

public class CaptureFormats implements java.beans.PropertyChangeListener {

	private DSCapture graph;

	private DSCapture.CaptureDevice vDev;

	private DSFilter.DSPin activeOut,
						   previewOut,
						   captureOut;

	private DSMediaType [] mf;

	private JFrame f,
				   imageFrame;

	private JComboBox formatSelector;

	private JSpinner fpsSpinner;

	int FORMAT_INDEX = 1;

	boolean changingFormat,
			flip;

	private String[] tvFormatStr = new String[]{"ATV_None",
												"ATV_NTSC_M",
												"ATV_NTSC_M_J",
												"ATV_NTSC_433",
												"",
												"ATV_PAL_B",
												"ATV_PAL_D",
												"",
												"ATV_PAL_H",
												"ATV_PAL_I",
												"ATV_PAL_M",
												"ATV_PAL_N",
												"ATV_PAL_60",
												"ATV_SECAM_B",
												"ATV_SECAM_D",
												"ATV_SECAM_G",
												"ATV_SECAM_H",
												"ATV_SECAM_K ",
												"ATV_SECAM_K1",
												"ATV_SECAM_L",
												"ATV_SECAM_L1",
											"ATV_PAL_N_COMBO"};

	public CaptureFormats() {}

	public void createGraph() {

		f = new JFrame("dsj - CaptureFormats, 0_8_6");

		graph = DSCapture.fromUserDialog(f, DSFiltergraph.DD7, this);

		f.add(java.awt.BorderLayout.CENTER, graph.asComponent());

		vDev = graph.getActiveVideoDevice();

		previewOut = vDev.getDeviceOutput(DSCapture.CaptureDevice.PIN_CATEGORY_PREVIEW);

		captureOut = vDev.getDeviceOutput(DSCapture.CaptureDevice.PIN_CATEGORY_CAPTURE);

		/**
		We're only interested in the preview output for this demo, but a lot of devices (webcams amongst others)
		do not have a separate preview pin (preview is then built via a Tee filter)
		**/

		activeOut = previewOut != null ? previewOut : captureOut;

		int pinIndex = activeOut.getIndex();

		DSFilterInfo.DSPinInfo usedPinInfo = vDev.getFilterInfo().getDownstreamPins()[pinIndex];

		formatSelector = new JComboBox();

		formatSelector.setLightWeightPopupEnabled(false);

		mf = usedPinInfo.getFormats();

		for (int i = 0; i < mf.length; i++) {

			formatSelector.addItem(mf[i].getDisplayString());

		}

		int currentFormat = vDev.getSelectedFormat(activeOut);

		try{ formatSelector.setSelectedIndex(currentFormat); } catch (Exception iae){  }

		formatSelector.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent e) {

				if (changingFormat) return;

				try{

					changingFormat = true;

					vDev.setOutputFormat(activeOut, formatSelector.getSelectedIndex());

				} catch (Exception ex){ex.printStackTrace();}

			}

		});

		try{

			DSFilterInfo.DSMediaFormat dsmf = (DSFilterInfo.DSMediaFormat)(mf[currentFormat]);

			fpsSpinner = new JSpinner(new SpinnerNumberModel(
											  (int)(previewOut != null ? vDev.getFrameRate(previewOut) : vDev.getFrameRate(captureOut)),
											  (int)(dsmf.getFrameRateRange()[0])-5,
											  (int)(dsmf.getFrameRateRange()[1])+5,
											  5));
		} catch (Exception e){

			fpsSpinner = new JSpinner(new SpinnerNumberModel(15, 0, 30, 5));

		}

		fpsSpinner.addChangeListener(new javax.swing.event.ChangeListener() {

			public void stateChanged(javax.swing.event.ChangeEvent ce){

				if (changingFormat) return;

				try{

					vDev.setFrameRate(activeOut, Float.parseFloat(fpsSpinner.getValue().toString()));

					System.out.println((previewOut != null ? "preview" : "capture")+" fps: "+vDev.getFrameRate(activeOut));

				} catch (Exception e){ System.out.println(e.toString()); }

			}
		});

		/**
		You can take the short way home and change things with a user dialog. See the SwingPropPage sample for a
		way to make this more java-like.
		**/

		final JButton fd = new JButton("WDM dialog");

		fd.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent e) {

				changingFormat = true;

				int result = graph.getActiveVideoDevice().showDialog(DSCapture.CaptureDevice.WDM_PREVIEW);

				if (result < 0) {

					System.out.println("cant show preview dialog: "+result);

					result = graph.getActiveVideoDevice().showDialog(DSCapture.CaptureDevice.WDM_CAPTURE);

				}

				if (result < 0) {

					System.out.println("no capture dialog either: "+result);

					result = graph.getActiveVideoDevice().showDialog(DSCapture.CaptureDevice.WDM_DEVICE);

				}

				if (result < 0) changingFormat = false;

			}

		});

		JPanel ctrls = new JPanel();

		ctrls.add(new JLabel("Setting format on pin: "+(previewOut != null ? previewOut.getName() : captureOut.getName())+"  "));
		ctrls.add(formatSelector);
		ctrls.add(new JLabel(" fps: "));
		ctrls.add(fpsSpinner);
		ctrls.add(fd);
		f.add(java.awt.BorderLayout.NORTH, ctrls);

		final JCheckBox continous = new JCheckBox("grab continously");

		final JButton gi = new JButton("grab frame");

		gi.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent e) {

				/**
				The grabbed frame will always be 24bit BGR unless the graph is built with the YUV flag set.
				In that case getImage() will return null, but YUV data is available via getData(). Requires
				a YUV source of course.
				**/

				java.awt.image.BufferedImage bi = graph.getImage();

				final JLabel imgLabel = new JLabel(new ImageIcon(bi));

				final java.awt.image.WritableRaster raster = bi.getWritableTile(bi.getWidth(), bi.getHeight());

				byte[] imgData = ((java.awt.image.DataBufferByte)raster.getDataBuffer()).getData();

				try{ imageFrame.dispose(); } catch (NullPointerException ne){}

				imageFrame = new JFrame("dsj captured frame");

				imageFrame.add(java.awt.BorderLayout.CENTER, imgLabel);

				JTextArea ta = new JTextArea(bi.toString(), 5, 1);

				ta.setLineWrap(true);

				imageFrame.add(java.awt.BorderLayout.SOUTH, ta);

				imageFrame.setPreferredSize(new java.awt.Dimension(bi.getWidth()+100, bi.getHeight()+200));

				imageFrame.pack();

				imageFrame.setLocation(600, 200);

				imageFrame.setVisible(true);

				if (!continous.isSelected()) return;

				Runnable r = new Runnable() {

					public void run(){

						while(imageFrame.isVisible()) {

							try{

								graph.getImage();

								imgLabel.updateUI();

								Thread.sleep(40);

							} catch (Exception e){}

						}

					}

				};

				new Thread(r).start();

			}

		});

		final JPanel lowerCtrls = new JPanel(new java.awt.GridLayout(2,3));

		lowerCtrls.setBorder(new javax.swing.border.EmptyBorder(5,5,5,5));

		((java.awt.GridLayout)(lowerCtrls.getLayout())).setVgap(5);

		((java.awt.GridLayout)(lowerCtrls.getLayout())).setHgap(5);

		final JPanel cropCoords = new JPanel(new java.awt.GridLayout(1,4));

		final int[] coords = new int[]{0, 0, (int)(graph.getMediaDimension().getWidth()), (int)(graph.getMediaDimension().getHeight())};

		for (int i = 0; i < 4; i++) {

			final JTextField ct = new JTextField(String.valueOf(coords[i]));

			ct.setPreferredSize(new java.awt.Dimension(30, 22));

			cropCoords.add(ct);

		}

		final JButton crop = new JButton("crop/size image");

		crop.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent e) {

				int[] caps = activeOut.getFormatCaps();

				for (int i = 0; i < 4; i++) coords[i] = Integer.valueOf(((JTextField)cropCoords.getComponent(i)).getText().trim()).intValue();

				changingFormat = true;

				graph.getActiveVideoDevice().setCropSize(activeOut, coords[0], coords[1], coords[2], coords[3]);

			}

		});

		JPanel fl = new JPanel(new java.awt.GridLayout(1,3));

		final JCheckBox hor = new JCheckBox("hor.");

		final JCheckBox ver = new JCheckBox("ver.");

		final JButton flip = new JButton("flip");

		flip.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent e) {

				boolean flipGrabbedImage = true;

				int fli = (hor.isSelected() ? 2 : 0) | (ver.isSelected() ? 1 : 0) | (flipGrabbedImage ? 4 : 0);

				graph.flipImage(fli);

			}

		});

		fl.add(hor);

		fl.add(ver);

		fl.add(flip);

		final javax.swing.JComboBox tvf = new javax.swing.JComboBox(tvFormatStr);

		try{

			final int[] vdi = ((DSCapture)graph).getActiveVideoDevice().getVDigSettings();

			int fIdx = 0;

			while ((vdi[0] >>= 1) != 0) { fIdx++; }

			tvf.setSelectedIndex(fIdx+1);

			tvf.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent e) {

					if ((vdi[2] & (1 << (tvf.getSelectedIndex() - 1))) != 0) graph.getActiveVideoDevice().configureVDig((1 << (tvf.getSelectedIndex() - 1)), 3);

					else System.out.println("not supported");

				}

			});

		} catch (Exception e){

			System.out.println("Can't get / set TVFormat with this device: "+e.toString());

			tvf.setEnabled(false);

		}

		lowerCtrls.add(cropCoords);

		lowerCtrls.add(fl);

		lowerCtrls.add(gi);

		lowerCtrls.add(crop);

		lowerCtrls.add(tvf);

		lowerCtrls.add(continous);

		f.add(java.awt.BorderLayout.SOUTH, lowerCtrls);

		f.pack();

		f.setVisible(true);

		/**
		Don't do this at home. This demo relies on dsj closing and disposing off filtergraphs when the JVM exits. This is
		OK for a "open graph, do something & exit" style demo, but real world applications should take care of calling
		dispose() on filtergraphs they're done with themselves.
		**/

		f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

	}

	public void propertyChange(java.beans.PropertyChangeEvent pe) {

		if (DSJUtils.getEventType(pe) == DSFiltergraph.FORMAT_CHANGED) {

			f.add("Center", graph.asComponent());

			f.pack();

			int sf = vDev.getSelectedFormat(previewOut != null ? previewOut : captureOut);

			formatSelector.setSelectedIndex(sf);

			DSFilterInfo.DSMediaFormat dsmf = (DSFilterInfo.DSMediaFormat)(mf[sf]);

			SpinnerNumberModel model = (SpinnerNumberModel)(fpsSpinner.getModel());

			model.setMinimum((int)(dsmf.getFrameRateRange()[0])-5);

			model.setMaximum((int)(dsmf.getFrameRateRange()[1])+5);

			fpsSpinner.setModel(model);

			try{

				float currentRate = previewOut != null ? vDev.getFrameRate(previewOut) : vDev.getFrameRate(captureOut);

				fpsSpinner.setValue(new Integer((int)currentRate));

			} catch (Exception e){e.printStackTrace();}

			changingFormat = false;

		}

	}

	public static void main(String[] args){

		new CaptureFormats().createGraph();

	}


}