 /**
dsj samplecode, simplified version of the streamclient demo at www.humatic.de/htools/dsj/jaws/dsj_stc.htm
You are free to use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
**/

import java.io.*;
import java.awt.*;
import java.net.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

import de.humatic.dsj.*;

public class SimpleStreamClient implements ActionListener, java.beans.PropertyChangeListener {

	private de.humatic.dsj.DSGraph graph;

	private de.humatic.dsj.src.Source netSrc;

	private javax.swing.JFrame  f;

	private JPanel screen;

	private String path;

	private boolean buffered;


	public SimpleStreamClient() {}

	void createGraph(int type, String path) {

		try{

			switch (type) {

				case 0:

					netSrc = new de.humatic.dsj.src.RTSPSource(path, this);

					break;

				case 1:

					/**
					for rtmp separate the actual stream to play by a "?" in the demo's dialog.
					For example to play an FMS 3 sample:
					"rtmp://127.0.0.1/vod?Sample.flv"
					**/

					String res = null;

					if (path.indexOf("?") > 0) {

						res = path.substring(path.indexOf("?")+1);

						path = path.substring(0, path.indexOf("?"));

					}

					netSrc = new de.humatic.dsj.src.RTMPSource(path, res, 0, this); //

					break;

				case 2:

					if (path.indexOf("http") != -1) netSrc = new de.humatic.dsj.src.TSNetworkSource(new java.net.URL(path), 0, this);

					else netSrc = new de.humatic.dsj.src.TSNetworkSource(path.substring(0, path.indexOf(":")), Integer.parseInt(path.substring(path.indexOf(":")+1)), 0, this);

					break;

				case 3:

					netSrc = new de.humatic.dsj.src.TSNetworkSource(new java.net.URL(path), 0, this);

					break;

				case 4:

					de.humatic.dsj.src.MJPGNetworkSource.setConnectionTimeout(20000);

					netSrc = new de.humatic.dsj.src.MJPGNetworkSource(path, this);

					break;

			}

			graph = netSrc.createGraph(de.humatic.dsj.DSFiltergraph.RENDER_NATIVE);

		} catch (Exception e){

			e.printStackTrace();

			if (graph != null) graph.dispose();

			graph = null;

			return;

		}

		setStream(graph);

	}

	void buildFrame() {

		f = new javax.swing.JFrame("dsj streamclient");

		f.setPreferredSize(new Dimension(500, 300));

		screen = new JPanel();

		f.getContentPane().add(screen);

		JButton b = new JButton("open stream");
		b.getAccessibleContext().setAccessibleName("open");
		b.addActionListener(this);

		screen.add(b);

		f.pack();

		f.setVisible(true);

		f.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

	}

	private void setStream(final DSFiltergraph graph) {

		try{

			screen.removeAll();

			screen.add(graph.asComponent());

			f.pack();

			graph.setVolume(1f);

			graph.play();

		} catch (Exception ne){ne.printStackTrace();}

	}

	public void actionPerformed(ActionEvent ae){

		String actionName = (((JButton)ae.getSource()).getAccessibleContext().getAccessibleName());

		if (actionName.equalsIgnoreCase("open")) {

			JTextField url = new JTextField();

			JComboBox type = new JComboBox(new String[]{"RTSP", "RTMP", "TS", "PS", "MJPEG"});

			Object[] array = {"sourcetype:", type, "url:", url};

			JOptionPane.showConfirmDialog(f, array, "open stream", JOptionPane.OK_CANCEL_OPTION);

			createGraph(type.getSelectedIndex(), url.getText());


		}

	}


	private void printInfo() {

		if (graph == null) return;

		StringBuffer sb = new StringBuffer();

		sb.append("Server: "+((de.humatic.dsj.src.NetworkSource)netSrc).getServerInfo()+"\n");

		try{

			for (int i = 0; i < netSrc.getSourceFilters().length; i++) {

				sb.append(netSrc.getSourceFilters()[i].getMediaType()+"\n");
			}

			if (netSrc instanceof de.humatic.dsj.src.MPEGSource) {

				int[] pids = ((de.humatic.dsj.src.MPEGSource)netSrc).getProgramPIDs();
				int[] st = ((de.humatic.dsj.src.MPEGSource)netSrc).getStreamTypes();
				sb.append("PMT: "+DSJUtils.decHex(((de.humatic.dsj.src.MPEGSource)netSrc).getPMT_PID())+"\n");
				sb.append("video PID: "+DSJUtils.decHex(pids[0])+", streamtype: "+DSJUtils.decHex(st[0])+"\n");
				sb.append("audio PID: "+DSJUtils.decHex(pids[1])+", streamtype: "+DSJUtils.decHex(st[1])+"\n");

			}

			final DSFilter[] filtersInGraph = graph.listFilters();

			for (int i = 0; i < filtersInGraph.length; i++) {

				sb.append(filtersInGraph[i].getName()+"\n");
			}

		} catch (Exception e) {

			sb.append(graph.getInfo());

		}

		System.out.println(sb.toString());

	}

	public void propertyChange(java.beans.PropertyChangeEvent pe) {

		try{

			switch (DSJUtils.getEventType(pe)) {

				case DSFiltergraph.SOURCE_STATE_CHANGED:

					int srcState = DSJUtils.getEventValue_int(pe);

					System.out.println("Source State changed: "+srcState);

					if (srcState == de.humatic.dsj.src.Source.SS_RUNNING) System.out.println("playing: "+path);

					break;

				case DSFiltergraph.RTMP_EVENT:

					System.out.println("\nRTMP event: ");

					try{

						String[] kv = (String[])(pe.getOldValue());

						for (int i = 0; i < kv.length; i+=2) System.out.println(kv[i]+": "+kv[i+1]);

					} catch (Exception e){}

					System.out.println("_______________________________");

					break;

				case DSFiltergraph.BUFFERING:

					if (buffered) System.out.println("buffer underrun");

					else {

						System.out.println("buffering %: "+DSJUtils.getEventValue_int(pe));

					}

					buffered = false;

					break;

				case DSFiltergraph.BUFFER_COMPLETE:

					System.out.println("buffer complete");

					break;

				case DSFiltergraph.DONE:

					try { graph.dispose(); } catch (Exception e){}

					JButton b = new JButton("open stream");
					b.getAccessibleContext().setAccessibleName("open");
					b.addActionListener(this);

					screen.add(b);

					f.pack();

					break;

				case DSFiltergraph.GRAPH_ERROR:

					System.out.println("Graph error: "+String.valueOf(pe.getNewValue()));

					break;

			}

		 } catch (Exception e){}

	}

	public static void main(String[] args) {

		new SimpleStreamClient().buildFrame();

	}

}




