/**
dsj demo code.
You may use, modify and redistribute this code under the terms laid out in the header of the DSJDemo application.
copyright 2005-8
N.Peters
humatic GmbH
Berlin, Germany
**/

import de.humatic.dsj.*;
import javax.sound.sampled.*;
import java.io.*;

/**
This sample demonstrates the use of the SampleAccessFilter to "in-memory" decode mp3 files in shortest possible time.
**/

public class Fast_MP3_Decoder implements java.beans.PropertyChangeListener {

	private DSMovie graph;

	private java.nio.ByteBuffer bb;

	private byte[] data;

	private int nullSamples,
				sampleCount,
				offset,
				decodedDuration;

	private long startTime;

	private SwingMovieController smc;

	private String AUDIOFILE = "media/audio.mp3";

	private ByteArrayOutputStream bos;

	public Fast_MP3_Decoder() {

		javax.swing.JFrame f = new javax.swing.JFrame("dsj - Fast_MP3_Decoder");

		smc = new SwingMovieController();

		f.getContentPane().add(smc);

		f.pack();

		f.setVisible(true);

		/**
		Don't do this at home. This demo relies on dsj closing and disposing off filtergraphs when the JVM exits. This is
		OK for a "open graph, do something & exit" style demo, but real world applications should take care of calling
		dispose() on filtergraphs they're done with themselves.
		**/

		f.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		bos = new ByteArrayOutputStream();

	}

	public void createGraph() {

		startTime = System.currentTimeMillis();

		/**
		This uses the NO_SYNC setup flag to switch off the graph clock. In turn the native filtergraph will run as fast
		as it possibly can. We also set the DSFiltergraph.DELIVER_AUDIO flag. This will instruct dsj to
		remove the Audio Renderer (and prepare for delivering a DSAudioStream which is ignored here) in order to prevent
		the AudioRenderer from eventually disallowing us to turn off the clck.
		**/

		graph = new DSMovie(AUDIOFILE,  DSFiltergraph.DELIVER_AUDIO |
										DSFiltergraph.INIT_PAUSED |
										DSFiltergraph.INIT_MUTED |
										DSFiltergraph.NO_SYNC,
										this);

		decodedDuration = graph.getDuration();

		/**
		This is to prevent OutOfMemory errors.
		A real world application would need to supply a clever ringbuffering strategy or a lot of RAM to work on longer files.
		**/

		if (decodedDuration > 40000) {

			graph.setSelection(0, 40000);

			graph.setPlaySelection(true);

			decodedDuration = 40000;

		}

		DSFilter[] filters = graph.listFilters();

		/**
		Insert the SampleAccessFilter behind the MP3Decoder (We're not further checking that here. A real world app should
		take better care of making sure the filter gets placed in the correct position) and make it deliver samples
		on the native thread.
		**/

		graph.insertSampleAccessFilter(filters[2], null, filters[3], DSSampleBuffer.THREAD_DS);

		smc.setFiltergraph(graph);

		bos = new ByteArrayOutputStream();

		graph.play();

	}

	public void propertyChange(java.beans.PropertyChangeEvent pe) {

		if (Integer.valueOf(pe.getNewValue().toString()).intValue() == DSFiltergraph.SAMPLE_BUFFER_FILLED) {

			sampleCount++;

			final DSSampleBuffer buffer = (DSSampleBuffer)(pe.getOldValue());

			byte[] data = buffer.getSample();

			bos.write(data, 0, buffer.getSampleLength());

			offset+= data.length;

		} else if (Integer.valueOf(pe.getNewValue().toString()).intValue() == DSFiltergraph.DONE) {

			System.out.println("total nr of samples: "+ sampleCount+"\nbytes written: "+offset+"\ndecoded duration: "+decodedDuration+" msec\ndecoded in: "+(System.currentTimeMillis()-startTime)+" msec");

			PlaybackThread pbt = new PlaybackThread();

			pbt.start();

		}

	}

	public static void main(String[] args){

		new Fast_MP3_Decoder().createGraph();

	}

	/**
	The following is bogus code. Naturally you will not want to decode an mp3 file like shown above to then play it back in
	realtime using javasound, but possibly to analyse or process it.
	This playback thread is used here, as it is simply the easiest way to demonstrate, that the decoded data is valid PCM audio.
	**/

	private class PlaybackThread extends Thread {

		private byte[] audioBytes = new byte[16384];

		private int time;

		private PlaybackThread() {

		}

		public void run() {

			int[] ap = graph.getAudioProperties();

			AudioFormat format = new javax.sound.sampled.AudioFormat(javax.sound.sampled.AudioFormat.Encoding.PCM_SIGNED,
														   ap[0]*1f,
														   ap[1],
														   ap[2],
														   ap[2] * ap[1] / 8,
														   ap[0]/(ap[1] / 8),
														   false);

			DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);

			SourceDataLine line = null;;

			try {

				line = (SourceDataLine) AudioSystem.getLine(info);

				line.open(format, audioBytes.length);

				line.start();


			} catch (LineUnavailableException ex) {

				System.out.println("Unable to open the line: " + ex);

				return;

			}

			ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());

			try{

				while (bis.read(audioBytes, 0, audioBytes.length) != -1) {

					try{

						line.write(audioBytes, 0, audioBytes.length);

					} catch (Exception ex) { }

				}

			} catch (Exception ioe){}

		}

	}

}