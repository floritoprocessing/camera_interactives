class WebCam {
  int wid=1, hei=1;
  PImage camImage;
  boolean camIsFake=false;
  
  WebCam(int _w, int _h) {
    wid=_w;
    hei=_h;
  }
  
  WebCam(String filename) {
    camImage=loadImage(filename);
    wid=camImage.width;
    hei=camImage.height;
    camIsFake=true;
  }
  
  void showInput() {
    if (camIsFake) {
      image(camImage,0,0,width,height);
    } else {
      
    }
  }
  
  color getColorAt(float xp, float yp) {
    int x=int(xp*wid);
    int y=int(yp*hei);
    return (camImage.get(x,y));
  }
  
  float getBrightnessAt(float xp, float yp) {
    int x=int(xp*wid);
    int y=int(yp*hei);
    return (brightness(camImage.get(x,y)));
  }
  
}
