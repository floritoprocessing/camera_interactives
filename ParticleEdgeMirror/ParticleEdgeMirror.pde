import java.util.Vector;

int SCREEN_WIDTH=640;
int SCREEN_HEIGHT=480;

int WEBCAM_WIDTH=200;
int WEBCAM_HEIGHT=150;
String WEBCAM_FAKE_IMAGE="face3.jpg";

int PARTICLE_AMOUNT_MIN=1000;
int PARTICLE_AMOUNT_MAX=10000;

WebCam webcam;
Vector particles=new Vector();
Particle particle;


void setup() {
  size(640,480);
  webcam=new WebCam(WEBCAM_FAKE_IMAGE);
  for (int i=0;i<PARTICLE_AMOUNT_MIN;i++) particles.add(new Particle(SCREEN_WIDTH,SCREEN_HEIGHT));
}

void draw() {
  background(0);
  
  tint(255,50);
  webcam.showInput();
  
  for (int i=0;i<particles.size();i++) ((Particle)(particles.elementAt(i))).noiseDir();
  for (int i=0;i<particles.size();i++) ((Particle)(particles.elementAt(i))).updateFromCam(webcam,particles,PARTICLE_AMOUNT_MAX);
  while (particles.size()<PARTICLE_AMOUNT_MIN) particles.add(new Particle(SCREEN_WIDTH,SCREEN_HEIGHT));
  println(particles.size());
}
