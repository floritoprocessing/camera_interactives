class Particle {
  float x=0, y=0;
  float xmov=0, ymov=0;
  float dir=random(TWO_PI);
  float speed=1.0;
  float xlim=1, ylim=1;
  float drawStrength=0.0;
  color col=color(255);
  float ovBright=0.0;
  float avBright=0.0;
  
  float BRIGHT_DIFFERENCE=50.0;
  
  Particle(float _xl, float _yl) {
    xlim=_xl;
    ylim=_yl;
    x=random(xlim);
    y=random(ylim);
    setMovFromDirAndSpeed();
  }
  
  Particle(float _xl, float _yl, float _x, float _y, float _speed, float _dir) {
    xlim=_xl;
    ylim=_yl;
    x=_x;
    y=_y;
    speed=_speed;
    dir=_dir;
  }
  
  void noiseDir() {
    dir+=(-0.1+0.2*noise(10*x/xlim,10*y/ylim,millis()/10000.0));
    setMovFromDirAndSpeed();
  }
  
  void updateFromCam(WebCam wc, Vector p, int maxPart) {
  
    ovBright=wc.getBrightnessAt(x/xlim,y/ylim);
    if (abs(ovBright-avBright)>BRIGHT_DIFFERENCE) {
      drawStrength=1.0;
      if (random(1.0)<0.1&&p.size()<maxPart) p.add(new Particle(xlim,ylim,x,y,speed,random(TWO_PI)));
    } else {
      drawStrength=(3*drawStrength+0.0)/4.0;
    }
    avBright=(2*avBright+ovBright)/3.0;
    
    x+=xmov;
    y+=ymov;
    if (x<0) x+=xlim;
    if (x>=xlim) x-=xlim;
    if (y<0) y+=ylim;
    if (y>=ylim) y-=ylim;
    
    if (drawStrength>0.1) softSet((int)x,(int)y,col,drawStrength);
    if (drawStrength<0.1) p.remove(this);
  }

  
  void setMovFromDirAndSpeed() {
    xmov=speed*cos(dir);
    ymov=speed*sin(dir);
  }  
}

void softSet(int sx, int sy, color c1, float p1) {
  color c2=get(sx,sy);
  float p2=1.0-p1;
  float r1=c1>>16&255;
  float r2=c2>>16&255;
  float g1=c1>>8&255;
  float g2=c2>>8&255;
  float b1=c1&255;
  float b2=c2&255;
  int rr=int(p1*r1+p2*r2);
  int gg=int(p1*g1+p2*g2);
  int bb=int(p1*b1+p2*b2);
  set(sx,sy,color(rr,gg,bb));
}
