import processing.video.*;

// VALUES FOR CAMERA INTERACTING
color[] imgbuffer, bgbuffer, difbuffer, edgebuffer; // imgbuffer holds camera, bgbuffer holds background, difbuffer holds difference
color[][] tempbgbuffer;      // buffers used for averaging background buffer
color nullcolor;             // color for filling difference buffer if there is no data
int nrOfBgBuf, bgBufToTake;
boolean getBgBuf, gotBgBuf;
int cameraW, cameraH, cameraPix, cameraFPS;
float camToScreen, focusX, focusY, diffFac;
boolean newFrame, mirrorCamera; int showMode;
int nrOfActivePixels, activePixelThreshhold; Pixel[] activePixel; int maxActivePixels;
int[] aPixelsInCol; // nr of active pixels per column. Index is x values
int[] highPixInCol; // 'highest' pixel (y value) in this column
int n2 = 5/2; int m2 = 5/2; 
float[][] kernel = { {-5, -5, -5, -5, -5}, 
                     {-5, -1, -1, -1, -5},
                     {-5, -1, 15, -1, -5},
                     {-5, -1, -1, -1, -5},                                        
                     {-5, -5, -5, -5, -5} };
float val=1.0/25.0;
float[][] kernel2 = { {val, val, val, val, val}, 
                      {val, val, val, val, val},
                      {val, val, val, val, val},
                      {val, val, val, val, val},                                        
                      {val, val, val, val, val} };    
     
Capture cap;

void setup() {
  // setup screen
  size(800, 600); 
  ellipseMode(CENTER); 
  rectMode(CENTER); 
  noStroke(); 
  //framerate(25); 
  background(255);
  // setup camera
  cameraW=160; cameraH=120; cameraPix=cameraW*cameraH; cameraFPS=25;
  //beginVideo(cameraW,cameraH,cameraFPS);
  cap = new Capture(this,cameraW,cameraH,cameraFPS);
  cap.start();
  
  // setup camera buffer
  imgbuffer=new color[cameraPix]; 
  // setup background buffer
  nrOfBgBuf=10; getBgBuf=false; gotBgBuf=false;
  bgbuffer=new color[cameraPix]; tempbgbuffer=new color[nrOfBgBuf][cameraPix];
  // setup difference buffer
  difbuffer=new color[cameraPix]; nullcolor=color(0,0,0);
  // setup edge buffer
  edgebuffer=new color[cameraPix]; for (int i=0;i<cameraPix;i++) {edgebuffer[i]=color(0,0,0);}
  // setup active pixels;
  diffFac=0.4;  // factor for comparing camera and bgbuffer (0..1)
  nrOfActivePixels=0; activePixel=new Pixel[cameraPix]; activePixelThreshhold=100; 
  aPixelsInCol=new int[cameraW]; highPixInCol=new int[cameraW];
  // setup values
  newFrame=false; 
  mirrorCamera=true;
  showMode=1;  // 0: nothing, 1: camera, 2: buffer, 3: difference camera-buffer, 4: edge-buffer
  camToScreen=width/(float)cameraW;
}





void draw() {
  background(0,0,0);
  // get new background buffer if requested
  
  if (cap.available()) {
    videoEvent();
    cap.loadPixels();
  }
  
  if (getBgBuf&&newFrame) {
    getNewBackgroundBuffer();
  }
  // get nrOfActivePixels, focusX, focusY, activePixel:
  getDifferenceImage();
  // create edges
  edgebuffer=difbuffer;
  edgebuffer=createEdgeImage(edgebuffer,kernel2);
  edgebuffer=createEdgeImage(edgebuffer,kernel);
  // show image buffer!
  showBuffer(showMode);
}



color[] createEdgeImage(color[] inbuf, float[][] krn) {
  color[] outbuf=inbuf;
  for(int y=0; y<cameraH; y++) { 
    for(int x=0; x<cameraW; x++) { 
      float sum = 0.0;
      for(int k=-n2; k<n2; k++) { 
        for(int j=-m2; j<m2; j++) { 
          // Reflect x-j to not exceed array boundary 
          int xp = x-j; 
          int yp = y-k; 
          if (xp < 0) { 
            xp = xp + cameraW; 
          } else if (x-j >= cameraW) { 
            xp = xp - cameraW; 
          } 
          // Reflect y-k to not exceed array boundary 
          if (yp < 0) { 
            yp = yp + cameraH; 
          } else if (yp >= cameraH) { 
            yp = yp - cameraH; 
          } 
          sum = sum + krn[j+m2][k+n2] * (float)brightness(inbuf[xyToIndex(xp,yp)]); 
          if (sum<0) {sum=0;}
        } 
      }
      outbuf[xyToIndex(x,y)]=color(sum,sum,sum);
    } 
  }
  return outbuf;
}

void getDifferenceImage() {
  nrOfActivePixels=0; focusX=0; focusY=0;
  for (int x=0;x<cameraW;x++) {
    for (int y=0;y<cameraH;y++) {
      int i=xyToIndex(x,y);
      if (colorDifference(imgbuffer[i],bgbuffer[i])>diffFac) {
        difbuffer[i]=imgbuffer[i];
        activePixel[nrOfActivePixels]=new Pixel(x,y,imgbuffer[i]);
        focusX+=x; focusY+=y; nrOfActivePixels++;
      } else {
        difbuffer[i]=nullcolor;
      }
    }
  }
  if (nrOfActivePixels!=0) {
    focusX/=nrOfActivePixels;
    focusY/=nrOfActivePixels;
  }
}

void showBuffer(int m) {
  if (showMode!=0) {
  noStroke();
    for (int x=0;x<cameraW;x++) {
      for (int y=0;y<cameraH;y++) {
        int i=xyToIndex(x,y);
        if (m==1) {
          fill(imgbuffer[i]);
        } else if (m==2) {
          fill(bgbuffer[i]);
        } else if (m==3) {
          fill(difbuffer[i]);
        } else if (m==4) {
          fill(edgebuffer[i]);
        }
        rect((x+.5)*camToScreen,(y+.5)*camToScreen,camToScreen,camToScreen);
      }
    }
  }
}

void getNewBackgroundBuffer() {
  if (bgBufToTake<nrOfBgBuf) {
    // get another bg into a buffer each frame
    for (int i=0;i<cameraPix;i++) {
      tempbgbuffer[bgBufToTake][i]=imgbuffer[i];
    }
    bgBufToTake++;
  } else {
    // create an average bg buffer from prevoously taken bg buffers
    for (int i=0;i<cameraPix;i++) {
      float rr=0,gg=0,bb=0;
      for (int j=0;j<nrOfBgBuf;j++) {
        color cc=tempbgbuffer[j][i];
        rr+=red(cc); gg+=green(cc); bb+=blue(cc);
      }
      rr/=(float)nrOfBgBuf; gg/=(float)nrOfBgBuf; bb/=(float)nrOfBgBuf;
      bgbuffer[i]=color(rr,gg,bb);
    }
    getBgBuf=false; gotBgBuf=true;
  }
}

void videoEvent() { 
  // refresh camera buffer
  //cap.loadPixels();
  cap.read();
  for (int x=0;x<cameraW;x++) {
    for (int y=0;y<cameraH;y++) {
      if (mirrorCamera) {
        imgbuffer[xyToIndex(x,y)]=cap.pixels[xyToIndex(cameraW-1-x,y)];
      } else {
        imgbuffer[xyToIndex(x,y)]=cap.pixels[xyToIndex(x,y)];
      }
    }
  }
  newFrame=true;
}

void keyPressed() {
  if (key==' ') { setupBackground(); }
  if (key=='0') { showMode=0; }
  if (key=='1') { showMode=1; }
  if (key=='2') { showMode=2; }
  if (key=='3') { showMode=3; }
  if (key=='4') { showMode=4; }
}

void setupBackground() {
  bgBufToTake=0; getBgBuf=true;
}


// classes
// -------

class Pixel {
  int x,y;
  color c;
  Pixel(int ix, int iy, color ic) {
    x=ix; y=iy; c=ic;
  }
}

// functions
// ---------

int xyToIndex(int x, int y) {
  return (y*cap.width+x);
}

float colorDifference(color a, color b) {
  float r1=red(a), g1=green(a), b1=blue(a);
  float r2=red(b), g2=green(b), b2=blue(b);
  float rd=abs(r1-r2)/255.0, gd=abs(g1-g2)/255.0, bd=abs(b1-b2)/255.0;
  return ((rd+gd+bd)/1.0);
}

color colorMul(float n,color a) {
  float r1=n*red(a), g1=n*green(a), b1=n*blue(a);
  return color(r1,g1,b1);
}

color colorAdd(color a, color b) {
  float r1=red(a), g1=green(a), b1=blue(a);
  float r2=red(b), g2=green(b), b2=blue(b);
  float ro=r1+r2, go=g1+g2, bo=b1+b2;
  return color(ro,go,bo);
}

color colorMix(float percA, color a, color b) {
  float percB=1.0-percA;
  float r1=red(a), g1=green(a), b1=blue(a);
  float r2=red(b), g2=green(b), b2=blue(b);
  float ro=percA*r1+percB*r2, go=percA*g1+percB*g2, bo=percA*b1+percB*b2;
  return color(ro,go,bo);
}

// VECTOR CLASS AND MATHEMATICS --------------------------------
// -------------------------------------------------------------
class Vec {
  float x,y;
  Vec(float x,float y) {
    this.x=x; this.y=y;
  }
}

Vec VecAdd(Vec a, Vec b) {
  Vec z=new Vec(0,0);
  z.x=a.x+b.x; z.y=a.y+b.y;
  return z;
}

Vec VecSub(Vec a, Vec b) {
  Vec z=new Vec(0,0);
  z.x=a.x-b.x; z.y=a.y-b.y;
  return z;
}

Vec VecMul(float a, Vec b) {
  Vec z=new Vec(0,0);
  z.x=a*b.x; z.y=a*b.y;
  return z;
}

float VecSkalar(Vec a, Vec b) { // (len(a)*len(b)*cos(angle between a and b)
  float z;
  z=a.x*b.x+a.y*b.y;
  return z;
}

float VecLen(Vec a) {
  float z=sqrt(a.x*a.x+a.y*a.y);
  return z;
}

Vec VecInScreen(Vec a) {
  Vec z=a;
  while (z.x>width) {z.x-=width;} while (z.x<1) {z.x+=width;}
  return z;
}
// -------------------------------------------------------------
// (end vector class and mathematics) --------------------------
