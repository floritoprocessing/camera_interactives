import processing.opengl.*;
import processing.video.*;

Capture cap;
PImage slitScan;
int slitScanX;
int column;

void setup() {
  size(800,240,P3D);
  cap = new Capture(this,320,240);
  cap.start();
  //cap.settings();
  
  slitScan = new PImage(width-320,height);
  slitScan.loadPixels();
  slitScanX = cap.width - slitScan.width + 1;
  column = slitScan.width - 1;
}

void captureEvent(Capture c) {
  
  c.read();
  c.loadPixels();
  
  // copy one column into the slitscan image
  for (int y=0;y<cap.height;y++) {
    slitScan.set(column, y, cap.get(cap.width-1,y));
  }
  slitScan.updatePixels();
  
  //move image to the right
  slitScanX++;
  
  // move column to the left
  column--;
  
  // check for boundaries
  if (column==-1) {
    slitScanX -= slitScan.width;
    column = slitScan.width-1;
  }
  
}

void draw() {
  
  image(slitScan,slitScanX,0);
  image(slitScan,slitScanX+slitScan.width,0);
  image(cap,0,0);
  
}
