class KeybindCollection extends Vector {
  
  KeybindCollection() {
  }
  
  boolean knows(int index) {
    if (key==CODED) {
      if (keyCode==((Keybind)this.elementAt(index)).getKeyCode()) return true;
    } else {
      if (key==((Keybind)this.elementAt(index)).getKey()) return true;
    }
    return false;
  }
  
}
