class Point {

  float DRAG = 0.8;

  float x=0, y=0, sx=0, sy=0;
  float xm=-0.5+random(1.0), ym=-0.5+random(1.0);


  Point() {
  }

  Point(float x, float y) {
    this.x=x;
    this.y=y;
    this.sx=x;
    this.sy=y;
  }
  
  void addMovement(FlowField ff) {
    double dm[] = ff.getVelocityAtScaled(x,y);
    xm += (float)dm[0]*0.35;
    ym += (float)dm[1]*0.35;
  }

  void move() {
    float edge = 500;
    float dx = sx-x;
    float dy = sy-y;
    float distanceToOrigin = sqrt(dx*dx+dy*dy);
    //float distanceFromEdge = edge - distanceToOrigin;
    //if (distanceFromEdge<0) distanceFromEdge=0;
    if (distanceToOrigin>0) {
      float normX = dx/distanceToOrigin;
      float normY = dy/distanceToOrigin;
      float force = 3*abs((distanceToOrigin>edge?edge:distanceToOrigin)/distanceToOrigin);//1000/sq(distanceFromEdge);
      float xa = force*normX;
      float ya = force*normY;
      xm+=xa;
      ym+=ya;
    }
    ym*=DRAG;
    xm*=DRAG;
    x+=xm;
    y+=ym;
    
  }

}
