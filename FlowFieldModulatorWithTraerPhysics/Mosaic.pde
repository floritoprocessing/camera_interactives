

class Mosaic {

  float PARTICLE_MASS = 0.1; // 0.1 0.01 is a killer!
  float PARTICLE_DRAG = 0.05; // 0.05
  float SPRING_STRENGTH = 8.0; //8
  float SPRING_DAMPING = 0.5; // 0.5

    float FLOWFIELD_INFLUENCE = 1.5;//1.5;

  float CURTAIN_GRAVITY = 5;

  public static final int PRESET_CURTAIN = 0;
  public static final int PRESET_WAVE_FIELD = 1;

  PApplet pa;
  Webcam wc;
  FlowField ff;
  Point[][] polyVertex, texVertex;

  ParticleSystem physics;
  Particle[][] particle;

  int width,height;


  Mosaic(PApplet pa, Webcam wc, FlowField ff) {
    this.pa=pa;
    this.wc=wc;
    this.ff=ff;
    width=ff.width;
    height=ff.height;
    polyVertex = new Point[width+1][height+1];
    texVertex = new Point[width+1][height+1];
    particle = new Particle[width+1][height+1];

    physics = new ParticleSystem(CURTAIN_GRAVITY, PARTICLE_DRAG); // gravity,drag
    for (int x=0;x<=width;x++) for (int y=0;y<=height;y++) {
      //float xx = (float)pa.width*(-0.1 + 1.2*x/width); 
      //float yy = (float)pa.height*(-0.1 + 1.2*y/height);
      particle[x][y] = physics.makeParticle(PARTICLE_MASS,particleIndexToPosX(x),particleIndexToPosY(y),0.0f);
      //if (x==0||x==width||y==0||y==height) particle[x][y].makeFixed(); // FRAME
      if (y==0) particle[x][y].makeFixed(); // CURTAIN
      //polyVertex[x][y] = new Point((float)pa.width*x/width,(float)pa.height*y/height);
      texVertex[x][y] = new Point((float)x/width,(float)y/height);
    }

    float gridSize = (float)pa.width/width;

    for (int x=0;x<=width;x++) for (int y=0;y<=height;y++) {
      if (x<width) physics.makeSpring(particle[x][y],particle[x+1][y],SPRING_STRENGTH,SPRING_DAMPING,gridSize);
      if (y<height) physics.makeSpring(particle[x][y],particle[x][y+1],SPRING_STRENGTH,SPRING_DAMPING,gridSize);
      //physics.makeSpring(particles[i][j - 1], particles[i][j], 8.0, 0.5, gridStepX);
      //Spring makeSpring( Particle a, Particle b, float strength, float damping, float restLength )
    }
  }

  float particleIndexToPosX(int x) {
    return (float)pa.width*(-0.1 + 1.2*x/width); 
  }
  float particleIndexToPosY(int y) {
    return (float)pa.height*(-0.1 + 1.2*y/height);
  }

  public void switchPreset(int preset) {
    if (preset==PRESET_CURTAIN) {
      println("making curtain");
      for (int x=0;x<=width;x++) for (int y=0;y<=height;y++) {
        if (y==0) particle[x][y].makeFixed(); 
        else particle[x][y].makeFree();
      } 
      physics.setGravity(CURTAIN_GRAVITY);
    }
    
    else if (preset==PRESET_WAVE_FIELD) {
      println("making wave field");
      for (int x=0;x<=width;x++) for (int y=0;y<=height;y++) {
        if (x==0||x==width||y==0||y==height) {
          particle[x][y].position().set(particleIndexToPosX(x),particleIndexToPosY(y),0);
          particle[x][y].makeFixed(); 
        }
        else particle[x][y].makeFree();
      }
      physics.setGravity(0);
    }
    
  }

  // modulate all except border
  void modulate() {
    physics.tick(.1);
    //physics.tick(.1);
    for (int x=1;x<width;x++) for (int y=1;y<height;y++) {
      double dm[] = ff.getVelocityAtScaled(pa.width*(float)x/width,pa.height*(float)y/height);
      particle[x][y].velocity().add(FLOWFIELD_INFLUENCE*(float)dm[0],FLOWFIELD_INFLUENCE*(float)dm[1],0);
      //polyVertex[x][y].addMovement(ff); // version1 (without traer physics)
      //polyVertex[x][y].move();          // version1 (without traer physics)
    }
  }

  void draw(boolean showGrid, boolean showImage) {
    fill(255,255,255);
    if (showGrid) stroke(128,128,255,64); 
    else noStroke();
    textureMode(NORMAL);
    beginShape(TRIANGLES);
    //beginShape(QUADS);
    if (showImage) texture(wc.camImage);
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
      /*
      //version1:
       vertex(polyVertex[x  ][y  ].x,polyVertex[x  ][y  ].y,texVertex[x  ][y  ].x,texVertex[x  ][y  ].y);
       vertex(polyVertex[x+1][y  ].x,polyVertex[x+1][y  ].y,texVertex[x+1][y  ].x,texVertex[x+1][y  ].y);
       vertex(polyVertex[x+1][y+1].x,polyVertex[x+1][y+1].y,texVertex[x+1][y+1].x,texVertex[x+1][y+1].y);
       vertex(polyVertex[x+1][y+1].x,polyVertex[x+1][y+1].y,texVertex[x+1][y+1].x,texVertex[x+1][y+1].y);
       vertex(polyVertex[x  ][y+1].x,polyVertex[x  ][y+1].y,texVertex[x  ][y+1].x,texVertex[x  ][y+1].y);
       vertex(polyVertex[x  ][y  ].x,polyVertex[x  ][y  ].y,texVertex[x  ][y  ].x,texVertex[x  ][y  ].y);
       */

      // TRIANGLES: (version 2)
      if (showImage) {
        vertex(particle[x  ][y  ].position().x()-1.0,particle[x  ][y  ].position().y()-0.5,texVertex[x  ][y  ].x,texVertex[x  ][y  ].y);
        vertex(particle[x+1][y  ].position().x()+0.5,particle[x+1][y  ].position().y()-0.5,texVertex[x+1][y  ].x,texVertex[x+1][y  ].y);
        vertex(particle[x+1][y+1].position().x()+0.5,particle[x+1][y+1].position().y()+1.0,texVertex[x+1][y+1].x,texVertex[x+1][y+1].y);
        vertex(particle[x+1][y+1].position().x()+1.0,particle[x+1][y+1].position().y()+0.5,texVertex[x+1][y+1].x,texVertex[x+1][y+1].y);
        vertex(particle[x  ][y+1].position().x()-0.5,particle[x  ][y+1].position().y()+0.5,texVertex[x  ][y+1].x,texVertex[x  ][y+1].y);
        vertex(particle[x  ][y  ].position().x()-0.5,particle[x  ][y  ].position().y()-1.0,texVertex[x  ][y  ].x,texVertex[x  ][y  ].y);
      } 
      else {
        vertex(particle[x  ][y  ].position().x()-1.0,particle[x  ][y  ].position().y()-0.5);
        vertex(particle[x+1][y  ].position().x()+0.5,particle[x+1][y  ].position().y()-0.5);
        vertex(particle[x+1][y+1].position().x()+0.5,particle[x+1][y+1].position().y()+1.0);
        vertex(particle[x+1][y+1].position().x()+1.0,particle[x+1][y+1].position().y()+0.5);
        vertex(particle[x  ][y+1].position().x()-0.5,particle[x  ][y+1].position().y()+0.5);
        vertex(particle[x  ][y  ].position().x()-0.5,particle[x  ][y  ].position().y()-1.0);
      }

      /*
      // QUADS: (version 2)
       vertex(particle[x  ][y  ].position().x()-.5,particle[x  ][y  ].position().y()-.5,texVertex[x  ][y  ].x,texVertex[x  ][y  ].y);
       vertex(particle[x+1][y  ].position().x()+.5,particle[x+1][y  ].position().y()-.5,texVertex[x+1][y  ].x,texVertex[x+1][y  ].y);
       vertex(particle[x+1][y+1].position().x()+.5,particle[x+1][y+1].position().y()+.5,texVertex[x+1][y+1].x,texVertex[x+1][y+1].y);
       vertex(particle[x  ][y+1].position().x()-.5,particle[x  ][y+1].position().y()+.5,texVertex[x  ][y+1].x,texVertex[x  ][y+1].y);     
       */
    }
    endShape();
  }

}
