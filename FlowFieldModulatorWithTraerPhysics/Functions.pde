class Functions {
  
  public static final int RED_MASK = 0x00FF0000;
  public static final int GREEN_MASK = 0x0000FF00;
  public static final int BLUE_MASK = 0x000000FF;

  private int mix(int a, int b, int f) {
    return a + (((b - a) * f) >> 8);
  }

  public int red(int c) {
    return c>>16&0xFF;
  }
  public int green(int c) {
    return c>>8&0xFF;
  }
  public int blue(int c) {
    return c&0xFF;
  }

  public int col(int r, int g, int b) {
    return r<<16|g<<8|b;
  }

  public int colorMix(int c0, int c1, int f0) {
    int rr = mix(c0&RED_MASK,c1&RED_MASK,f0);
    int gg = mix(c0&GREEN_MASK,c1&GREEN_MASK,f0);
    int bb = mix(c0&BLUE_MASK,c1&BLUE_MASK,f0);
    return rr&RED_MASK|gg&GREEN_MASK|bb&BLUE_MASK;
  }

  public int grayscaleDifference(int g0,int g1) {
    int gd = g0-g1;
    return (gd<0?-gd:gd);
  }

  public int colorDifference(int c0, int c1) {
    int r0 = c0>>16&0xFF, r1 = c1>>16&0xFF;
    int g0 = c0>>8&0xFF, g1 = c1>>8&0xFF;
    int b0 = c0&0xFF, b1 = c1&0xFF;
    int rd = r0-r1;
    int gd = g0-g1;
    int bd = b0-b1;
    return (rd<0?-rd:rd)<<16|(gd<0?-gd:gd)<<8|(bd<0?-bd:bd);
  }

  public int brightness(int c) {
    return ((c>>16&0xFF)+(c>>8&0xFF)+(c&0xFF))/3;
  }
}
