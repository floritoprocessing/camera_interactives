import processing.video.*;

import java.util.Vector;

Webcam webcam;
FlowField flowField;
Mosaic mosaic;
KeybindCollection kc = new KeybindCollection();

boolean SHOW_FLOWFIELD = false;
boolean SHOW_GRID = true;
boolean SHOW_IMAGE = true;
boolean SHOW_STATS = true;
int FLOWFIELD_AVERAGE_PASSES = 4;//5;
float FLOWFIELD_THRESHOLD = 4.0;//5;

float avWebcamGetTime = 0;
float avFlowFieldUpdateTime = 0;
float avFlowFieldAveragingTime = 0;
float avFlowFieldThresholdTime = 0;
float avFlowFieldDrawTime = 0;

float avFac = 0.95, avFac1=1.0-avFac;

float avFrameRate = 25;

void setup() {
  size(800, 600, P3D);
  noSmooth();
  frameRate(100);

  kc.add(new Keybind('f', "show/hide flowfield"));
  kc.add(new Keybind(RIGHT, "increase flowField-averaging passes"));
  kc.add(new Keybind(LEFT, "decrease flowField-averaging passes"));
  kc.add(new Keybind(UP, "increase flowField noise threshold"));
  kc.add(new Keybind(DOWN, "decrease flowField noise threshold"));
  kc.add(new Keybind('g', "show/hide grid"));
  kc.add(new Keybind('i', "show/hide webcam image"));
  kc.add(new Keybind('s', "show/hide stats"));

  kc.add(new Keybind('1', "Preset: Curtain"));
  kc.add(new Keybind('2', "Preset: Wave Field"));

  webcam = new Webcam(this);
  flowField = new FlowField(this, webcam, 8);
  mosaic = new Mosaic(this, webcam, flowField);
}


void keyPressed() {
  //if (frameCount>100) {
  if (kc.knows(0)) { 
    SHOW_FLOWFIELD = !SHOW_FLOWFIELD; 
    avFlowFieldDrawTime=0;
  } else if (kc.knows(1)) FLOWFIELD_AVERAGE_PASSES++;
  else if (kc.knows(2)&&FLOWFIELD_AVERAGE_PASSES>=1) FLOWFIELD_AVERAGE_PASSES--;
  else if (kc.knows(3)) FLOWFIELD_THRESHOLD+=0.1;
  else if (kc.knows(4)&&FLOWFIELD_THRESHOLD>=0.1) FLOWFIELD_THRESHOLD-=0.1;
  else if (kc.knows(5)) SHOW_GRID = !SHOW_GRID;
  else if (kc.knows(6)) SHOW_IMAGE = !SHOW_IMAGE;
  else if (key=='1') mosaic.switchPreset(Mosaic.PRESET_CURTAIN);
  else if (key=='2') mosaic.switchPreset(Mosaic.PRESET_WAVE_FIELD);
  //}
}

void draw() {
  background(0);



  smooth();
  frameRate(25);

  avFrameRate = 0.9*avFrameRate + 0.1*frameRate;
  if (avFrameRate<1.0) noLoop();





  // GET WEBCAM
  int getCamTime = webcam.updateCameraImage();
  if (getCamTime==-1) {
    return;
  }
  avWebcamGetTime = avFac*avWebcamGetTime + avFac1*getCamTime;

  // UPDATE FLOWFIELD
  avFlowFieldUpdateTime = avFac*avFlowFieldUpdateTime + avFac1*flowField.update();

  // AVERAGE FLOWFIELD
  float tmp=0;
  for (int i=0; i<FLOWFIELD_AVERAGE_PASSES; i++) 
    tmp += flowField.average();
  avFlowFieldAveragingTime = avFac*avFlowFieldAveragingTime + avFac1*tmp;

  // THRESHOLD FLOWFIELD
  avFlowFieldThresholdTime = avFac*avFlowFieldThresholdTime + avFac1*flowField.threshold(FLOWFIELD_THRESHOLD);

  // MODULATE MOSAIC BY FLOWFIELD
  mosaic.modulate();

  // SHOW WEBCAM OR BACKGROUND
  mosaic.draw(SHOW_GRID, SHOW_IMAGE);

  // SHOW FLOWFIELD
  if (SHOW_FLOWFIELD) {
    PGraphicsOpenGL pgl = (PGraphicsOpenGL) g;
    //GL gl = pgl.beginGL();
    avFlowFieldDrawTime = avFac*avFlowFieldDrawTime + avFac1*flowField.draw(0, 0, 800/320.0, 800/320.0, 1.0);
    //pgl.endGL();
  } else
    avFlowFieldDrawTime = avFac*avFlowFieldDrawTime;




  // SHOW STATS

  if (SHOW_STATS) {
    print("frame");
    print("\twebcamGet");
    print("\tflowFieldMake");
    print("\tflowFieldAverage["+FLOWFIELD_AVERAGE_PASSES+"]");
    print("\tflowFieldThreshold["+nf(FLOWFIELD_THRESHOLD, 1, 2)+"]");
    println("\tflowFieldDraw["+(SHOW_FLOWFIELD?"on":"off")+"]");
    print(nf(frameCount, 5));
    print("\t"+nf(avWebcamGetTime, 1, 2)+" ms");
    print("\t"+nf(avFlowFieldUpdateTime, 1, 2)+" ms");
    print("\t\t"+nf(avFlowFieldAveragingTime, 1, 2)+" ms");
    print("\t\t"+nf(avFlowFieldThresholdTime, 1, 2)+" ms");//);
    println("\t\t\t"+nf(avFlowFieldDrawTime, 1, 2)+" ms");
  }
}
