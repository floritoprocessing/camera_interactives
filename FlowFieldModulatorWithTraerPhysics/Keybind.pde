class Keybind {
  
  char c;
  int code;
  String name;
  
  Keybind(char c, String name) {
    this.c=c;
    this.name=name;
    println("binding char ["+c+"] to "+name);
  }
  
  Keybind(int code, String name) {
    this.code = code;
    this.name = name;
    println("binding keyCode "+code+" to "+name);
  }
  
  char getKey() {
    return c;
  }
  
  int getKeyCode() {
    return code;
  }
  
}
