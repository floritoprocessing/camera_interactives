class FlowField extends Functions {

  private Webcam webcam;
  private int divider;
  private int dividerScreenToField;
  private int width, height;
  private int[][] lowResCamera, lowResLast;
  private double[][] velX, velY;
  private double[][] newVelX, newVelY;

  private double LAMBDA = 100;	// noise reduction

  /**
   * 	 * Creates a new instance of a FlowField based on a Webcam object with the resolution of the webcam divided by divider
   * 	 * @param wc the Webcam object
   * 	 * @param _divider divider determining the resoluton of the FlowField
   	 */
  public FlowField(PApplet pa, Webcam wc, int _divider) {

    webcam = wc;
    divider = _divider;    
    width = webcam.width/divider;
    height = webcam.height/divider;
    print("initializing FlowField with "+width+" "+height+"... ");
    dividerScreenToField = pa.width/width;
    velX = new double[width][height];
    velY = new double[width][height];
    newVelX = new double[width][height];
    newVelY = new double[width][height];
    lowResCamera = new int[width][height];
    lowResLast = new int[width][height];
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
      velX[x][y] = 0;
      velY[x][y] = 0;
      lowResCamera[x][y] = 0x000000;
      lowResLast[x][y] = 0x000000;
    }
    println("done");
  }

  private void makeLowResImages() {
    for (int x=0;x<width;x++) for (int y=0;y<height;y++)
      lowResLast[x][y] = lowResCamera[x][y];

    int d2 = divider*divider;
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
      int sx = x*divider, sy = y*divider;
      int rgb=0;
      for (int xo=0;xo<divider;xo++) for (int yo=0;yo<divider;yo++) {
        int wx = sx+xo, wy = sy+yo;
        int wc = brightness(webcam.getCamera(wx, wy));
        rgb += wc;

      }
      rgb/=d2;
      lowResCamera[x][y] = rgb;
    }


    /*
    // erode
    int[][] temp = new int[width][height];
    int nx=0, ny=0;
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
      int min=255;
      for (int xo=-1;xo<=1;xo++) for (int yo=-1;yo<=1;yo++) {
        nx=x+xo;
        ny=y+yo;
        if (nx>=0&&nx<width&&ny>=0&&ny<height)
          min = lowResCamera[nx][ny]<min?lowResCamera[nx][ny]:min;
      }
      temp[x][y]=min;
    }
    for (int x=0;x<width;x++) for (int y=0;y<height;y++)
      lowResCamera[x][y]=temp[x][y];
    */
  }

  /**
   * 	 * Updates the flow field
   	 */
  public int update() {
    int ti=millis();
    makeLowResImages();

    for (int x=1;x<width-1;x++) for (int y=1;y<height-1;y++) {
      int cDif = lowResCamera[x][y]-lowResLast[x][y];
      int gradX1 = lowResCamera[x+1][y]-lowResCamera[x-1][y];
      int gradX2 = lowResLast[x+1][y]-lowResLast[x-1][y];
      double gradX = (gradX1+gradX2)/2.0;
      int gradY1 = lowResCamera[x][y+1]-lowResCamera[x][y-1];
      int gradY2 = lowResLast[x][y+1]-lowResLast[x][y-1];
      double gradY = (gradY1+gradY2)/2.0;
      double gradMag = Math.sqrt(gradX*gradX+gradY*gradY+LAMBDA);
      velX[x][y] = -cDif*gradX/gradMag;
      velY[x][y] = -cDif*gradY/gradMag;
    }

    return (millis()-ti);

    /*
		 *
     		 * http://petewarden.com/notes/archives/2005/05/gpu_optical_flo.html
     		 * 
     		 * (1) CurrentDifference = (Frame1[x][y]-Frame0[x][y]);
     		 * 
     		 * (2) GradientX = ((Frame1[x+1][y]-Frame1[x-1][y]) + (Frame0[x+1][y]-Frame0[x-1][y])) / 2.0;		 * 
     		 * (3) GradientY = ((Frame1[x][y+1]-Frame1[x][y-1]) + (Frame0[x][y+1]-Frame0[x][y-1])) / 2.0; 
     		 * 
     		 * (4) GradientMagnitude = sqrt((GradientX*GradientX)+(GradientY*GradientY)); 
     		 * 
     		 * (5) VelocityX = CurrentDifference*(GradientX/GradientMagnitude);
     		 * (6) VelocityY = CurrentDifference*(GradientY/GradientMagnitude); 
     		 * 
     		 */


  }
  
  public int threshold(float thresh) {
    int ti=millis();
    for (int x=0;x<width;x++) for (int y=0;y<height;y++)
      if (Math.sqrt(velX[x][y]*velX[x][y]+velY[x][y]*velY[x][y])<thresh) {
        velX[x][y]=0;
        velY[x][y]=0;
      }
    return (millis()-ti);
  }


  public int average() {
    int ti=millis();
    for (int x=1;x<width-1;x++) for (int y=1;y<height-1;y++) {

      double avX=0, avY=0;
      for (int xo=-1;xo<=1;xo++) for (int yo=-1;yo<=1;yo++) {
        avX += velX[x+xo][y+yo];
        avY += velY[x+xo][y+yo];
      }
      avX/=9.0;
      avY/=9.0;

      newVelX[x][y]=avX;
      newVelY[x][y]=avY;

    }

    for (int x=1;x<width-1;x++) for (int y=1;y<height-1;y++) {
      velX[x][y]=newVelX[x][y];
      velY[x][y]=newVelY[x][y];
    }
    return (millis()-ti);
  }

  public double[] getVelocityAtScaled(double scrnX, double scrnY) {

    int fieldX = (int)(scrnX/dividerScreenToField);
    int fieldY = (int)(scrnY/dividerScreenToField);
    double[] out = new double[] {
      0,0    };
    if (fieldX>=1&&fieldX<width-1&&fieldY>=1&&fieldY<height-1) {
      out[0] = velX[fieldX][fieldY];
      out[1] = velY[fieldX][fieldY];
    }
    return out;
  }

  /**
   * 	 * Returns the lower resolution PImage representation of what FlowField uses as input
   * 	 * @see #FlowField(Webcam, int)
   * 	 * @return PImage
   	 */
  public PImage getLowResImage() {
    PImage out = new PImage(width,height);
    int i=0;
    for (int y=0;y<height;y++) for (int x=0;x<width;x++) {
      out.pixels[i] = lowResCamera[x][y];
      i++;
    }
    out.updatePixels();
    return out;
  }

  /**
   * 	 * Draws the vector representation of the FlowField on the PApplet pa at screen position xo/yo
   * 	 * @param pa the parent Processing Applet
   * 	 * @param xo the top left position x where to start drawing
   * 	 * @param yo the top left position y where to start drawing
   	 */
  /*
  public int draw(PApplet pa, float xo, float yo, float xs, float ys) {
   int ti=millis();
   pa.stroke(255,0,0);//,192);
   float d2=divider/2.0f;
   xo += d2;
   yo += d2;
   for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
   float x0 = xo+x*divider, y0 = yo+y*divider;
   pa.line(x0*xs,y0*ys,(x0+(float)(velX[x][y]))*xs,(y0+(float)(velY[x][y]))*ys);
   }
   return(millis()-ti);
   }
   */

  public int draw(float xo, float yo, float xs, float ys, float exag) {
    int ti=millis();
    //pa.stroke(255,0,0,192);
    strokeWeight(1);
    stroke(255,0,0);
    float d2=divider/2.0f;
    xo += d2;
    yo += d2;
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
      float x0 = xo+x*divider, y0 = yo+y*divider;
      beginShape(LINE_STRIP);
      //pa.line(x0*xs,y0*ys,(x0+(float)(velX[x][y]))*xs,(y0+(float)(velY[x][y]))*ys);
      vertex(x0*xs,y0*ys);
      vertex((float)((x0+exag*velX[x][y])*xs),(float)((y0+exag*velY[x][y])*ys));
      endShape();
    }
    return(millis()-ti);
  }

}
