/**
 * 
 */
package florito.camsculpturer;

import processing.core.PApplet;
import processing.core.PImage;
import processing.video.Capture;
//import JMyron.JMyron;

/**
 * @author Marcus
 *
 */
public class Webcam extends Capture {
	
	public static final int CAMERA_IMAGE = 1;

	public static boolean DEBUG;

	public int width, height;
	private PImage image;
	
	public Webcam(PApplet pa, int w, int h) {
		super(pa,w,h);
		this.width=w;
		this.height=h;
		image = new PImage(w,h);
		image.loadPixels();
	}
	
	public PImage getCameraImage() {
		return image;
	}
	
	/*
	 * (non-Javadoc)
	 * @see JMyron.JMyron#start(int, int)
	 */
	public void start() {//int width, int height) {
		super.start();//width, height);
//		this.width = width;//super.getForcedWidth();
//		this.height = height;//super.getForcedHeight();
		image = new PImage(this.width,this.height);
		System.out.println("webcam started. Requested "+width+"x"+height+", forced "+this.width+"x"+this.height);
	}
	
	/*
	 * (non-Javadoc)
	 * @see JMyron.JMyron#update()
	 */
	public void update(int target) {
//		super.update();
		if (available()) {
			read();
			loadPixels();
			if ((target&0x01)==1) {
				image.pixels = pixels;//cameraImage();
				image.updatePixels();
			}
		}
	}

}
