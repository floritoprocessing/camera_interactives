/**
 * 
 */
package florito.camsculpturer;

import java.util.ArrayList;

//import JMyron.JMyron;
import processing.core.PApplet;
import processing.core.PImage;

/**
 * @author Marcus
 *
 */
public class CamSculpturer extends PApplet {
	
	public static boolean DEBUG;


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"florito.camsculpturer.CamSculpturer"});
	}
	
	Webcam webcam;
	int nrOfContours = 20;
	int colors[] = new int[nrOfContours];
	
	int lowScale = 4;
	PImage small;
	
	int layerAmount = 1;
	int firstLayer = 0;
	LineCollection[] lineLayer = new LineCollection[layerAmount];
	
	float layerTotalDepth = -800;
	
	public void settings() {

		size(1024,768,P3D);
	}
	public void setup() {
		webcam = new Webcam(this,320,240);
		webcam.start();//320, 240);
//		webcam.adaptivity(0);
		small = new PImage(webcam.width/lowScale, webcam.height/lowScale);
		small.loadPixels();
		//webcam.settings();
		
		//colorMode(HSB,255);
		for (int i=0;i<colors.length;i++) {
			colors[i] = color(0,0,0);//color(i*255/colors.length,255,128);
		}
		
		smooth();
		//colorMode(RGB,255);
	}
	
	public void draw() {
		if (frameCount<100) {
			return;
		}
		background(255);
		webcam.update(Webcam.CAMERA_IMAGE);
		
		small.copy(webcam.getCameraImage(), 0, 0, webcam.width, webcam.height, 0, 0, small.width, small.height);
		small.updatePixels();
		small.filter(GRAY);
		small.filter(BLUR,1);
		small.updatePixels();
		image(small,0,0);
		
		float dolly = 0.55f;
		//camera(width/2,height*2*dolly,400*dolly, width/2,height/2,0, 0,1,0);
		
		
		
		//strokeWeight(3);
		int[] px = small.pixels;
		int[] bright = new int[px.length];
		for (int i=0;i<px.length;i++) {
			bright[i] = 0xff&px[i];
		}

		LineCollection lines = Contour.createContour(bright, small.width, small.height, lowScale*width/webcam.width, nrOfContours);
		lineLayer[firstLayer] = lines;//.toArray(new Line[0]);
		
		
		int layerIndex, layer, j;
		float z, depthPercentage, opacity;
		for (layer = firstLayer; layer<firstLayer+layerAmount; layer++) {
			layerIndex = layer%layerAmount;
			//z = (layer-firstLayer)*-100;
			depthPercentage = ((float)layer-firstLayer)/layerAmount;
			if (depthPercentage==0) {
				opacity = 1;
			} else {
				opacity = 0.4f-0.4f*depthPercentage;
			}
			//opacity = 1-pow(depthPercentage,0.1f);
			z = depthPercentage*layerTotalDepth;
			
			stroke(0,0,0,opacity*255);
			if (lineLayer[layerIndex]!=null) {
				for (j=0;j<lineLayer[layerIndex].size();j++) {
					lineLayer[layerIndex].get(j).set3d((int)z, (int)z);
				}
				lineLayer[layerIndex].draw(this);
			}
			
		}
		
		//translate(0,0,layerTotalDepth);
		//image(webcam.getCameraImage(),0,0,width,height);
		//filter(BLUR,1);
		
		firstLayer--;
		if (firstLayer==-1) {
			firstLayer=layerAmount-1;
		}
	}
}
