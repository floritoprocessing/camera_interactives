package florito.camsculpturer;

public class Array2d {
	
	public final int width;
	public final int height;
	public final int[] pixels;
	
	public Array2d(int width, int height) {
		this.width = width;
		this.height = height;
		pixels = new int[width*height];
	}
	
	public void set(int x, int y, int v) {
		pixels[y*width+x]=v;
	}
	
	public int get(int x, int y) {
		return pixels[y*width+x];
	}
	

}
