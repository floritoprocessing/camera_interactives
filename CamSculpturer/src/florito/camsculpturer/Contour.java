package florito.camsculpturer;

import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PImage;

public class Contour {

	/**
	  * 
	  * @param valMin
	  * @param sMin
	  * @param valMax
	  * @param sMax
	  * @param s
	  * @return
	  */
	 public static float interpolate(float valMin, float sMin, float valMax, float sMax, float s, boolean restrain) {
		 if((sMax - sMin) != 0.0F) {
			 if (restrain)
				 return Math.max(Math.min(valMin, valMax), Math.min(Math.max(valMin, valMax), valMin + (s - sMin) * (valMax - valMin) / (sMax - sMin)));
			 else
				 return valMin + (s - sMin) * (valMax - valMin) / (sMax - sMin);
		 } else {
			 return 0.0F;
		 }
	 }

	public static int[] getSmallMediumLarge(float[] value, int i, int j, int k) {
		int temp;
		int large = i;
		int medium = j;
		int small = k;
		if(value[small] > value[medium]) {
			temp = medium;
			medium = small;
			small = temp;
		}
		if(value[medium] > value[large]) {
			temp = large;
			large = medium;
			medium = temp;
		}
		if(value[small] > value[medium]) {
			temp = medium;
			medium = small;
			small = temp;
		}
		return new int[] {small,medium,large};
	}
	
	public static LineCollection createContour(int[] vals, int width, int height, int scl, int nrOfContours) {
		
		LineCollection out = new LineCollection();
		
		//int scl = noiseToCanvas;//canvas.width/noise.width;
		float[] contour = new float[nrOfContours+1];
		for (int i=0;i<contour.length;i++) {
			contour[i] = (float)i/nrOfContours*255;
		}
		float[] X = new float[5];
		float[] Y = new float[5];
		float[] value = new float[5];
		
		int v1,v2,v3;
		float target;
		float x1, x2, y1, y2;
		
		//int i=0;
		int pixelIndex = 0;
	    for (int j=0;j<height-1;j++) {
	    	for(int i=0;i<width-1;i++)	{
	    		X[0] = i;
	    		Y[0] = j;	
	    		value[0] = vals[pixelIndex];//get(i,j);//&0xff;
	    		X[1] = i+1;	
	    		Y[1] = j;	
	    		value[1] = vals[pixelIndex+1];//img.get(i+1,j);//&0xff;
				X[2] = i+1;	
				Y[2] = j+1;	
				value[2] = vals[pixelIndex+width+1];//img.get(i+1,j+1);//&0xff;
				X[3] = i;	
				Y[3] = j+1;	
				value[3] = vals[pixelIndex+width];//img.get(i,j+1);//&0xff;
				X[4] = 0.5F*(X[0]+X[1]);
				Y[4] = 0.5F*(Y[1]+Y[2]);
				value[4] = 0.25F*(value[0]+value[1]+value[2]+value[3]);
	
				v3 = 4;
				for(v1=0; v1<4; v1++) {
					v2 = v1 + 1;
					if (v1 == 3) {
					v2 = 0;
					}
					
					int[] sml = getSmallMediumLarge(value, v1, v2, v3);
					int small = sml[0];
					int medium = sml[1];
					int large = sml[2];
							
					for(int lines=0; lines<=nrOfContours; lines++){
						target = contour[lines];
						
						if (value[small] < target && target < value[large]) {
							x1 = interpolate(X[small], value[small], X[large], value[large], target, false);
							y1 = interpolate(Y[small], value[small], Y[large], value[large], target, false);
							if (target > value[medium]) {
								x2 = interpolate(X[medium], value[medium], X[large], value[large], target, false);
								y2 = interpolate(Y[medium], value[medium], Y[large], value[large], target, false);
							} else {
								x2 = interpolate(X[small], value[small], X[medium], value[medium], target, false);
								y2 = interpolate(Y[small], value[small], Y[medium], value[medium], target, false);
							}
							float xx0 = (x1+0.5f)*scl;
							float xx1 = (x2+0.5f)*scl;
							float yy0 = (y1+0.5f)*scl;
							float yy1 = (y2+0.5f)*scl;
							out.add(new IntLine((int)xx0,(int)yy0,(int)xx1,(int)yy1));
						}
					}
				}
				pixelIndex++;
			}
	    	pixelIndex++;
		}
	    //canvas.endDraw();
	    
	    return out;
	}

	/*public static ArrayList<Line> createContour(Array2d noise, int noiseToCanvas, int nrOfContours, int alpha, int[] colors) {
		
		ArrayList<Line> out = new ArrayList<Line>();
		
		int scl = noiseToCanvas;//canvas.width/noise.width;
		float[] contour = new float[nrOfContours+1];
		for (int i=0;i<contour.length;i++) {
			contour[i] = (float)i/nrOfContours*255;
		}
		float[] X = new float[5];
		float[] Y = new float[5];
		float[] value = new float[5];
		
		int v1,v2,v3;
		float target;
		float x1, x2, y1, y2;
		
	    for (int j=0;j<noise.height-1;j++) {
	    	for(int i=0;i<noise.width-1;i++)	{
	    		X[0] = i;
	    		Y[0] = j;	
	    		value[0] = noise.get(i,j);//&0xff;
	    		X[1] = i+1;	
	    		Y[1] = j;	
	    		value[1] = noise.get(i+1,j);//&0xff;
				X[2] = i+1;	
				Y[2] = j+1;	
				value[2] = noise.get(i+1,j+1);//&0xff;
				X[3] = i;	
				Y[3] = j+1;	
				value[3] = noise.get(i,j+1);//&0xff;
				X[4] = 0.5F*(X[0]+X[1]);
				Y[4] = 0.5F*(Y[1]+Y[2]);
				value[4] = 0.25F*(value[0]+value[1]+value[2]+value[3]);
	
				v3 = 4;
				for(v1=0; v1<4; v1++) {
					v2 = v1 + 1;
					if (v1 == 3) {
					v2 = 0;
					}
					
					int[] sml = getSmallMediumLarge(value, v1, v2, v3);
					int small = sml[0];
					int medium = sml[1];
					int large = sml[2];
							
					for(int lines=0; lines<=nrOfContours; lines++){
						target = contour[lines];
						
						if (value[small] < target && target < value[large]) {
							x1 = interpolate(X[small], value[small], X[large], value[large], target, false);
							y1 = interpolate(Y[small], value[small], Y[large], value[large], target, false);
							if (target > value[medium]) {
								x2 = interpolate(X[medium], value[medium], X[large], value[large], target, false);
								y2 = interpolate(Y[medium], value[medium], Y[large], value[large], target, false);
							} else {
								x2 = interpolate(X[small], value[small], X[medium], value[medium], target, false);
								y2 = interpolate(Y[small], value[small], Y[medium], value[medium], target, false);
							}
							int col = alpha<<24 | colors[lines];
							float xx0 = (x1+0.5f)*scl;
							float xx1 = (x2+0.5f)*scl;
							float yy0 = (y1+0.5f)*scl;
							float yy1 = (y2+0.5f)*scl;
							out.add(new Line(xx0,yy0,xx1,yy1,col));
						}
					}
				}
			}
		}
	    //canvas.endDraw();
	    
	    return out;
	}*/

}
