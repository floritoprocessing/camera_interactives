package florito.camsculpturer;

import processing.core.PApplet;
import processing.core.PGraphics;

public class Line {
	
	private static final int DIM2 = 0;
	private static final int DIM3 = 1;
	
	private float x0, y0, z0;
	private float x1, y1, z1;
	private int type = DIM2;

	public Line(float x0, float y0, float x1, float y1) {
		this.x0=x0;
		this.y0=y0;
		this.x1=x1;
		this.y1=y1;
		type = DIM2;
	}
	
	public Line(float x0, float y0, float z0, float x1, float y1, float z1) {
		this(x0,y0,x1,y1);
		set3d(z0,z1);
	}
	
	public void set3d(float z0, float z1) {
		this.z0=z0;
		this.z1=z1;
		type = DIM3;
	}
	
	public void draw(PGraphics pg) {
		if (type==DIM2) {
			pg.line(x0, y0, x1, y1);
		} else if (type==DIM3) {
			pg.line(x0, y0, z0, x1, y1, z1);
		}
	}
	
	public void draw(PApplet pa) {
		if (type==DIM2) {
			pa.line(x0, y0, x1, y1);
		} else if (type==DIM3) {
			pa.line(x0, y0, z0, x1, y1, z1);
		}
	}

	/**
	 * @param line
	 * @param line2
	 * @return
	 */
	public static boolean connected(Line A, Line B) {
		if (A.x0==B.x0 && A.y0==B.y0) 
			return true;
		else if (A.x0==B.x1 && A.y0==B.y1)
			return true;
		else if (A.x1==B.x1 && A.y1==B.y1)
			return true;
		else if (A.x1==B.x0 && A.y1==B.y0) 
			return true;
		else
			return false;
	}

}
