/**
 * 
 */
package florito.camsculpturer;

import java.util.ArrayList;

import processing.core.PApplet;

/**
 * @author Marcus
 *
 */
public class LineCollection extends ArrayList<IntLine> {

	public LineCollection() {
		super();
	}

	/**
	 * @param camSculpturer
	 */
	public void draw(PApplet pa) {
		for (int i=0;i<size();i++) {
			get(i).draw(pa);
		}
	}
	
}
