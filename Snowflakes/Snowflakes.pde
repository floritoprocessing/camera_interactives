import processing.video.*;

Capture cam;

void setup() {
  size(320,240);
  initCamera();
  initBgBuf();
  createSnowflakes();
}

void draw() {
  if (cam.available()) {
    cam.read();
    cam.loadPixels();
  }
  background(128);
  showCamera();
  getNewBgBuf();
  showBgBuf();
  showDifBuf();
  updateSnowflakes();
}



////////////////////////////  Camera PARAMETERS, OBJECT and ROUTINES ////////////////////////////

// preset values:
int camW=320, camH=240, camPix=320*240, fps=50;

// global" variables/objects:
boolean newFrame=false;

void initCamera() {
  imageMode(CORNER);
  //beginVideo(camW,camH,fps);
  cam = new Capture(this,camW,camH,fps);
  cam.start();
}

void showCamera() {
  image(cam,0,0,width,height);
}

////////////////////////////  bgBuf PARAMETERS, OBJECT and ROUTINES ////////////////////////////

// preset values:
int nr_buf_frames=10;
int threshhold=220;  // 0..255*3

// global" variables/objects:
int[][] bgbuf; int[] difbuf;
boolean getBuf;
int bufToGet;

void initBgBuf() {
  getBuf=false; bufToGet=0;
  bgbuf=new int[nr_buf_frames+1][camPix];
  difbuf=new int[camPix];
}

void showBgBuf() {
  if (keyPressed) { if (key=='b'|key=='B') {
  for (int x=0;x<camW;x++) {for (int y=0;y<camH;y++) {
    int i=y*camW+x;
    set(x,y,bgbuf[nr_buf_frames][i]);
  } }
  } }
}

void showDifBuf() {
  for (int i=0;i<camPix;i++) {
    int c1=bgbuf[nr_buf_frames][i], c2=cam.pixels[i];
    int dif=abs(c1>>16&255-c2>>16&255)+abs(c1>>8&255-c2>>8&255)+abs(c1&255-c2&255);
    if (dif>threshhold) {
      difbuf[i]=65535;
    } else {
      difbuf[i]=0;
    }
  }
  if (keyPressed) { if (key=='d'|key=='D') {
    for (int x=0;x<camW;x++) {for (int y=0;y<camH;y++) {
      int i=y*camW+x;
      set(x,y,difbuf[i]);
    } }
  } }
}

void getNewBgBuf() {
  if (getBuf) {
    println("get buffer nr: "+bufToGet);
    for (int i=0;i<camPix;i++) {
      bgbuf[bufToGet][i]=cam.pixels[i];
    }
    bufToGet++;
    float part=1/(float)nr_buf_frames;
    if (bufToGet==nr_buf_frames) {
      println("creating average image");
      for (int i=0;i<camPix;i++) {
        float rr=0,gg=0,bb=0;
        for (int j=0;j<nr_buf_frames;j++) {
          int c=bgbuf[j][i];
          rr+=part*(c>>16&255); gg+=part*(c>>8&255); bb+=part*(c&255);
        }
        bgbuf[nr_buf_frames][i]=int(rr)<<16|int(gg)<<8|int(bb);
      }
      bufToGet=0; getBuf=false;
    }
  }
}

void keyPressed() {
  if (key==' ') { bufToGet=0; getBuf=true; }
}


////////////////////////////  Snowflake PARAMETERS, OBJECT and ROUTINES ////////////////////////////

// preset values:
int nr_flakes=500;

// "global" variables/objects:
Snowflake[] snowflake; 

void createSnowflakes() {
  snowflake=new Snowflake[nr_flakes];
  for (int i=0;i<nr_flakes;i++) {snowflake[i]=new Snowflake();}
}

void updateSnowflakes() {
  for (int i=0;i<nr_flakes;i++) {snowflake[i].update();snowflake[i].toScreen();}
}

class Snowflake {
  int[] flake; int flake_size, maxpix;
  float speedOffset;
  float sx,sy,ym=0;
  Snowflake() {
    speedOffset=random(0.8,1.2);
    flake_size=(int)random(2,5); maxpix=flake_size*flake_size;
    flake=new int[maxpix];
    for (int i=0;i<maxpix;i++) {flake[i]=0;}
    for (float r=0;r<flake_size/2.0;r+=0.5) {
      for (float rd=0;rd<TWO_PI;rd+=TWO_PI/360.0) {
        if (random(flake_size/2.0)<flake_size/(20*r)) {
          int x=int(flake_size/2.0+r*cos(rd));
          int y=int(flake_size/2.0+r*sin(rd));
          int i=y*flake_size+x;
          flake[i]=1<<17|255<<16|255<<8|255;
        }
      }
    }
    sx=random(width); sy=-(int)random(height);
  }
  
  void update() {
    ym=(3*ym+flake_size*speedOffset*0.5)/4.0;
    
    // check for difference between bgbuf and camera
    int i=int(constrain(sy,0,camH-1))*camW+int(constrain(sx,0,camW-1));
    int c1=bgbuf[nr_buf_frames][i], c2=cam.pixels[i];
    float dif=abs(c1>>16&255-c2>>16&255)+abs(c1>>8&255-c2>>8&255)+abs(c1&255-c2&255);

    if (dif>threshhold) { 
      
    } else {
      sy+=ym;
    }
    
    if (sy>height+flake_size) {
      sx=random(width);
      sy=-(int)random(height)-flake_size;
    }
    
  }
  
  void toScreen() {
    for (int x=0;x<flake_size;x++) {for (int y=0;y<flake_size;y++) {
      int i=y*flake_size+x;
      if (flake[i]<<17!=0) {
        set (int(sx)+x,int(sy)+y,flake[i]);
      }
    } }
  }
}
