import java.util.Arrays;

//import JMyron.JMyron;
import processing.core.PApplet;
import processing.video.Capture;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class CamEdgeDetector extends PApplet {
	
	public static boolean DEBUG;

	public static void main(String[] args) {
		PApplet.main(new String[] {"CamEdgeDetector"});
	}
	
	Capture cap;
//	JMyron jm;
	int[] tmp0;
	int[] fade;
	
	public void settings() {
		size(320,240,P3D);
	}
	public void setup() {
		
		
//		jm = new JMyron();
//		jm.start(320, 240);
		cap = new Capture(this,320,240);
		cap.start();
//		println(jm.getForcedWidth()+"x"+jm.getForcedHeight());
//		tmp0 = new int[jm.getForcedWidth()*jm.getForcedHeight()];
//		fade = new int[jm.getForcedWidth()*jm.getForcedHeight()];
		tmp0 = new int[320*240];
		fade = new int[320*240];
	}
	
	
	public void draw() {
		//background(0);
		if (cap.available()) {
			cap.read();
			cap.loadPixels();
		}
//		jm.update();
		loadPixels();
		
//		int[] camSource = jm.cameraImage();
//		int[] tmp1 = jm.cameraImage();//new int[camSource.length];
		int[] camSource = cap.pixels;
		int[] tmp1 = cap.pixels;//new int[camSource.length];
		
		//System.arraycopy(camSource, 0, campixels, 0, camSource.length);
		
		
		gray(tmp1,tmp0); // 0..765
		createEdges(tmp0, tmp1, 320, 240); // 0..6120
		divide(tmp1, tmp0, 8, 255);
		fade(tmp0, fade, 0.1f);
		toRGB(fade, tmp1);
		addRGB(camSource,tmp1,pixels);
		//medianFilter(tmp, pixels, 320, 240);
		updatePixels();
		
		//System.arraycopy(tmp, 0, pixels, 0, tmp.length);
	}

	/**
	 * @param tmp2
	 * @param pixels
	 * @param f
	 */
	private static void fade(int[] in, int[] inAndOut, float inRatio) {
		float outRatio = 1-inRatio;
		for (int i=0;i<in.length;i++) {
			inAndOut[i] = 0xff<<24 
			| (int)Math.min(255, inRatio*(in[i]>>16&0xff) + outRatio*(inAndOut[i]>>16&0xff)) <<16
			| (int)Math.min(255, inRatio*(in[i]>>8&0xff)  + outRatio*(inAndOut[i]>>8&0xff)) <<8
			| (int)Math.min(255, inRatio*(in[i]&0xff)     + outRatio*(inAndOut[i]&0xff));
		}
	}


	/**
	 * @param in1
	 * @param in2
	 * @param out
	 */
	private static void addRGB(int[] in1, int[] in2, int[] out) {
		for (int i=0;i<in1.length;i++) {
			out[i] = 0xff<<24 
			| Math.min(255, (in1[i]>>16&0xff)+(in2[i]>>16&0xff))<<16
			| Math.min(255, (in1[i]>>8&0xff)+(in2[i]>>8&0xff))<<8
			| Math.min(255, (in1[i]&0xff)+(in2[i]&0xff));
		}
	}


	/**
	 * @param in
	 * @param out
	 * @param divider
	 */
	private static void divide(int[] in, int[] out, int divider, int max) {
		for (int i=0;i<in.length;i++) {
			out[i] = (in[i]/divider);
			if (out[i]>max) out[i]=max;
		}
	}
	
	private static void toRGB(int[] in, int[] out) {
		for (int i=0;i<in.length;i++) {
			out[i] = in[i]<<16 | in[i]<<8 | in[i];
		}
	}


	/**
	 * Takes a 24-bit color info and converts it to brightness info
	 * @param in input array (0x000000..0xffffff)
	 * @param out output array (0..765)
	 */
	private static void gray(int[] in, int[] out) {
		for (int i=0;i<in.length;i++) {
			out[i] = (in[i]>>16&0xff) + (in[i]>>8&0xff) + in[i]&0xff;
		}
	}


	

	

	/**
	 * Create edges
	 * @param in input array
	 * @param out output array (these values can be up to 8x larger than the input array)
	 * @param width
	 * @param height
	 */
	private static void createEdges(int[] in, int[] out, int width, int height) {
		int i = 1+width;
		int i00 = - width - 1;
		int i10 = - width;
		int i20 = - width + 1;
		int i01 = -1;
		int i21 = 1;
		int i02 = + width - 1;
		int i12 = + width;
		int i22 = + width + 1;
		int gradX, gradY, grad;
		for (int y=1;y<height-1;y++) {
			for (int x=1;x<width-1;x++) {
				gradX = -in[i+i00] + in[i+i20] - 2*in[i+i01] + 2*in[i+i21] - in[i+i02] + in[i+i22];
				gradY = in[i+i00] + 2*in[i+i10] + in[i+i20] - in[i+i02] - 2*in[i+i12] - in[i+i22];
				grad = Math.abs(gradX)+Math.abs(gradY); // 0..2048
				out[i] = grad;
				i++;
			}
			i+=2;
		}
	}
	
	
	/**
	 * @param campixels
	 * @param i
	 * @param j
	 */
	/*private static void medianFilter(int[] src, int[] out, int width, int height) {
		for (int i=0;i<src.length;i++) {
			src[i] = 0x000000ff & (((src[i]>>16&0xff) + (src[i]>>8&0xff) + (src[i]&0xff))/3);
		}
		int i = 1+width;
		int i00 = - width - 1;
		int i10 = - width;
		int i20 = - width + 1;
		int i01 = -1;
		int i21 = 1;
		int i02 = + width - 1;
		int i12 = + width;
		int i22 = + width + 1;
		for (int y=1;y<height-1;y++) {
			for (int x=1;x<width-1;x++) {
				out[i] = median(src[i+i00],src[i+i10],src[i+i20],src[i+i01], src[i], src[i+i21], src[i+i02], src[i+i12], src[i+i22]);
				i++;
			}
			i+=2;
		}
	}*/
	
	/**
	 * @param i
	 * @param j
	 * @param k
	 * @param l
	 * @param m
	 * @param n
	 * @param o
	 * @param p
	 * @param q
	 * @return
	 */
	/*private static int median(int i, int j, int k, int l, int m, int n, int o,
			int p, int q) {
		int[] list = new int[] {i,j,k,l,m,n,o,p,q};
		Arrays.sort(list);
		return list[4];
	}*/
	
}
