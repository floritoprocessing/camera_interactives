import JMyron.*;
JMyron cam;
PImage camImage;
PImage outImage;
PFont font;
int y = 0;

void setup() {
  size(320,600);
  cam = new JMyron();
  cam.start(320,240);
  camImage = new PImage(320,240);
  cam.findGlobs(0);
  
  outImage = new PImage(width,height);
  
  font = loadFont("Arial-Black-24.vlw");
  textFont(font);
  //java.awt.Toolkit.getDefaultToolkit().beep();     
}

void draw() {
  cam.update();
  camImage.pixels = cam.cameraImage();
  camImage.updatePixels();
  
  background(0);
  
  float newHeight = height;
  float newWidth = camImage.width*newHeight/camImage.height;
  imageMode(CENTER);
  image(camImage,width/2,height/2,newWidth,newHeight);
  loadPixels();
  
  if (y<height) {
    int index = y*width;
    arrayCopy(pixels,index,outImage.pixels,index,width);
    outImage.updatePixels();
  }
  
  imageMode(CORNER);
  image(outImage,0,0);
  
  
  
  
  y++;
  //println(newWidth+", "+newHeight);
  
  //image(camImage,0,0);
}


