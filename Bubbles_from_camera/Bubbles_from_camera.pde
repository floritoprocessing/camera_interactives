import processing.video.*;
Capture video;

// VALUES FOR CAMERA INTERACTING
color[] imgbuffer, bgbuffer; // imgbuffer holds camera, bgbuffer holds background
color[][] tempbgbuffer;      // buffers used for averaging background buffer
int nrOfBgBuf, bgBufToTake;
boolean getBgBuf, gotBgBuf;
int cameraW, cameraH, cameraPix, cameraFPS;
float camToScreen, focusX, focusY;
boolean newFrame, showCamera, mirrorCamera, showNoBuffer;
int nrOfActivePixels, activePixelThreshhold; Pixel[] activePixel; int maxActivePixels;
int[] aPixelsInCol; // nr of active pixels per column. Index is x values
int[] highPixInCol; // 'highest' pixel (y value) in this column

// VALUES FOR BUBBLES AND PHASESETS:
int actBubble, nrOfBubbles;
color navyBlue=#23238e, lightskyblue1=#b0e2ff, black=#000000;
Bubble[] bubble;
PhaseSet[] phaseset; int actphaseset, nrOfPhaseSets;


void setup() {
  // setup screen
  size(800, 600,P3D); 
  ellipseMode(DIAMETER);
  rectMode(DIAMETER); 
  noStroke(); 
  //framerate(50); 
  background(255);
  // setup camera
  cameraW=80; cameraH=60; cameraPix=cameraW*cameraH; cameraFPS=50;
  //beginVideo(cameraW,cameraH,cameraFPS);
  video = new Capture(this,cameraW,cameraH,cameraFPS);
  video.start();
  // setup camera buffer
  imgbuffer=new color[cameraPix]; 
  // setup background buffer
  nrOfBgBuf=5; getBgBuf=false; gotBgBuf=false;
  bgbuffer=new color[cameraPix]; tempbgbuffer=new color[nrOfBgBuf][cameraPix];
  // setup active pixels;
  nrOfActivePixels=0; activePixel=new Pixel[cameraPix]; activePixelThreshhold=200; 
  aPixelsInCol=new int[cameraW]; highPixInCol=new int[cameraW];
  // setup values
  newFrame=false; 
  showCamera=true;  // if false-> show background buffer
  mirrorCamera=true;
  showNoBuffer=false;
  camToScreen=width/(float)cameraW;
    
  // INIT PHASESET: (used by Bubbles)
  actphaseset=0; nrOfPhaseSets=width*height;
  phaseset=new PhaseSet[nrOfPhaseSets];
  for (int i=0;i<nrOfPhaseSets;i++) {
    phaseset[i]=new PhaseSet();
  }
  // INIT BUBBLES
  actBubble=0; nrOfBubbles=2000;
  bubble=new Bubble[nrOfBubbles];
  for (int i=0;i<nrOfBubbles;i++) {
    bubble[i]=new Bubble();
    bubble[i].active=false;
  }
  actBubble=0;
}





void draw() {
  if (video.available()) videoEvent();
  video.loadPixels();
  
  background(navyBlue);
  // get new background buffer if requested
  if (getBgBuf&&newFrame) {
    getNewBackgroundBuffer();
  }
  // show image buffer!
  if (!showNoBuffer) {showImageBuffer();}
  
  // get nrOfActivePixels, focusX, focusY, activePixel:
  getDifferenceImage();
  
  
  if (nrOfActivePixels>activePixelThreshhold&&gotBgBuf) {
    // go through all active pixels and count the number of active pixels per column
    // clear column buffer
    for (int i=0;i<cameraW;i++) {
      aPixelsInCol[i]=0;
      highPixInCol[i]=cameraH;
    }
    // create column buffer
    for (int i=0;i<nrOfActivePixels;i++) {
      int x=activePixel[i].x, y=activePixel[i].y;
      aPixelsInCol[x]+=1;
      if (y<highPixInCol[x]) {highPixInCol[x]=y;}
    }
    
    for (int x=0;x<cameraW;x++) {
      if (aPixelsInCol[x]>5) {
        int y=highPixInCol[x];
        float sx=x*camToScreen, sy=y*camToScreen;
        actphaseset=int(sx)+width*int(sy);
        bubble[actBubble].init(actBubble,phaseset[actphaseset],phaseset[actphaseset].s,int(sx),int(sy));
        actBubble++; if (actBubble==nrOfBubbles) {actBubble=0;}
        println(actBubble);
      }
    }
      
    //// CREATE BUBBLES WHERE Focus IS
    //actphaseset=focX+width*focY;
    //bubble[actBubble].init(actBubble,phaseset[actphaseset],phaseset[actphaseset].s,focX,focY);
    //// increase activeBubbleNr
    //actBubble++; if (actBubble==nrOfBubbles) {actBubble=0;}
  }
  
  // UPDATE ALL BUBBLES
  for (int i=0;i<nrOfBubbles;i++) {
    bubble[i].update();
  }
}





void getDifferenceImage() {
  nrOfActivePixels=0; focusX=0; focusY=0;
  for (int x=0;x<cameraW;x++) {
    for (int y=0;y<cameraH;y++) {
      int i=xyToIndex(x,y);
      if (colorDifference(imgbuffer[i],bgbuffer[i])>0.3) {
        activePixel[nrOfActivePixels]=new Pixel(x,y,imgbuffer[i]);
        focusX+=x; focusY+=y; nrOfActivePixels++;
      }
    }
  }
  if (nrOfActivePixels!=0) {
    focusX/=nrOfActivePixels;
    focusY/=nrOfActivePixels;
  }
}

void showImageBuffer() {
  noStroke();
  for (int x=0;x<cameraW;x++) {
    for (int y=0;y<cameraH;y++) {
      int i=xyToIndex(x,y);
      if (showCamera) {
        fill(imgbuffer[i]);
      } else {
        fill(bgbuffer[i]);
      }
      rect((x+.5)*camToScreen,(y+.5)*camToScreen,camToScreen,camToScreen);
    }
  }
}

void getNewBackgroundBuffer() {
  if (bgBufToTake<nrOfBgBuf) {
    // get another bg into a buffer each frame
    for (int i=0;i<cameraPix;i++) {
      tempbgbuffer[bgBufToTake][i]=imgbuffer[i];
    }
    bgBufToTake++;
  } else {
    // create an average bg buffer from prevoously taken bg buffers
    for (int i=0;i<cameraPix;i++) {
      float rr=0,gg=0,bb=0;
      for (int j=0;j<nrOfBgBuf;j++) {
        color cc=tempbgbuffer[j][i];
        rr+=red(cc); gg+=green(cc); bb+=blue(cc);
      }
      rr/=(float)nrOfBgBuf; gg/=(float)nrOfBgBuf; bb/=(float)nrOfBgBuf;
      bgbuffer[i]=color(rr,gg,bb);
    }
    getBgBuf=false; gotBgBuf=true;
  }
}

void videoEvent() { 
  video.read();
  // refresh camera buffer
  for (int x=0;x<cameraW;x++) {
    for (int y=0;y<cameraH;y++) {
      if (mirrorCamera) {
        imgbuffer[xyToIndex(x,y)]=video.pixels[xyToIndex(cameraW-1-x,y)];
      } else {
        imgbuffer[xyToIndex(x,y)]=video.pixels[xyToIndex(x,y)];
      }
    }
  }
  newFrame=true;
}

void keyPressed() {
  if (key==' ') { setupBackground(); }
  if (key=='n'||key=='N') { showNoBuffer=!showNoBuffer; }
  if (key=='c'||key=='C') { showCamera=true; }
  if (key=='b'||key=='B') { showCamera=false; }
}

void setupBackground() {
  bgBufToTake=0; getBgBuf=true;
}


// classes
// -------

class Pixel {
  int x,y;
  color c;
  Pixel(int ix, int iy, color ic) {
    x=ix; y=iy; c=ic;
  }
}





// class Bubble
// ------------------------------------------------------------------
// bubble moves dependent from its size
// bubble needs to be initialized through init(indexNr, Phaseset, size, xpos, ypos)
// buuble updates through update()
class Bubble {
  float x,y,z,xs,ys,zs;  // position and startposition
  float s;               // size in mm (2..4)
  float sms;             // time in milliseconds at creation
  float ymov=300.0/50.0; // 300 mm/sec y-movement
  float wavlen, xfreq, zfreq; // wavelength -> x- and z-frequency (dependent from s);
  float critHeight;           // height at wich sin-movement is fully extended (dependent from s);
  int nrOfTrajectories;       // nr of trajectories that a bubble could follow (2..5) -> see '5' in phaseset (dependent from s);
  float amp;                  // amplitude of sin-movement (dependent from s);
  float xph=0.0, zph=0.0;     // phase of sin-movement (dependent from pixel where bubble is launched)
  float mainScale=.8;         // scale for translating mm to pixel
  boolean active;
  int nr;

  Bubble() {
    active=false;
  }
  
  void init(int n, PhaseSet phset, float si, float xi, float yi) {
    nr=n; 
    s=si;                                       // in mm                 | 4..2
    wavlen=50+(4.0-s)*50.0;                     // in mm                 | 50 .. 100
    xfreq=(ymov*50.0)/wavlen; zfreq=xfreq-0.01; // wavelength of sin-movement
    critHeight=35.0+5.0*(s-2.0);                // in mm                 | 45  .. 35 
    nrOfTrajectories=2+int(1.5*(4.0-s));        //                       | 2   .. 5
    amp=4.5+2.5*(4.0-s);                        // in mm                 | 4.5 .. 9.5                
    xph=phset.x[nr%nrOfTrajectories]; zph=phset.z[nr%nrOfTrajectories];
    x=xi; xs=xi; y=yi; ys=yi; z=0.0; zs=0.0; 
    active=true; sms=millis();
  }
  
  void update() {
    if (active) {
      float sintrav=(ys-y)/critHeight; if (sintrav>1.0) {sintrav=1.0;};
      sintrav=pow(sintrav,2);
      x=xs+mainScale*( sintrav*(amp*sin(xfreq*TWO_PI*(millis()-sms)/1000.0+xph) + amp*cos(zfreq*TWO_PI*(millis()-sms)/1000.0+zph)) );
      z=zs+mainScale*( sintrav*(amp*cos(xfreq*TWO_PI*(millis()-sms)/1000.0+xph) + amp*sin(zfreq*TWO_PI*(millis()-sms)/1000.0+zph)) );
      y-=mainScale*ymov;
      if (y<1) {
        active=false;
      } else {
        stroke(lightskyblue1);
        point(x,y,z);
        for (float rd=0;rd<TWO_PI;rd+=TWO_PI/12.0) {
          point(x+mainScale*s*cos(rd),y+mainScale*s*sin(rd),z);
        }
      }
    }
  }
}
// ------------------------------------------------------------------
// end class Bubble



// class PhaseSet
// ------------------------------------------------------------------
// holds 5 x- and y-phases (0..TWO_PI) 
// holds one s value (2..4)
// used for storing phases and bubble sizes for each pixel on screen
class PhaseSet {
  float[] x,z;
  float s;
  PhaseSet() {
    x=new float[5]; z=new float[5];
    for (int i=0;i<5;i++) {
      x[i]=random(TWO_PI); z[i]=random(TWO_PI);
    }
    s=pow(random(1),4)*2.0+2.0;
  }
}
// ------------------------------------------------------------------
// end class PhaseSet



// functions
// ---------

int xyToIndex(int x, int y) {
  return (y*video.width+x);
}

float colorDifference(color a, color b) {
  float r1=red(a), g1=green(a), b1=blue(a);
  float r2=red(b), g2=green(b), b2=blue(b);
  float rd=abs(r1-r2)/255.0, gd=abs(g1-g2)/255.0, bd=abs(b1-b2)/255.0;
  return ((rd+gd+bd)/1.0);
}

color colorMul(float n,color a) {
  float r1=n*red(a), g1=n*green(a), b1=n*blue(a);
  return color(r1,g1,b1);
}

color colorAdd(color a, color b) {
  float r1=red(a), g1=green(a), b1=blue(a);
  float r2=red(b), g2=green(b), b2=blue(b);
  float ro=r1+r2, go=g1+g2, bo=b1+b2;
  return color(ro,go,bo);
}

color colorMix(float percA, color a, color b) {
  float percB=1.0-percA;
  float r1=red(a), g1=green(a), b1=blue(a);
  float r2=red(b), g2=green(b), b2=blue(b);
  float ro=percA*r1+percB*r2, go=percA*g1+percB*g2, bo=percA*b1+percB*b2;
  return color(ro,go,bo);
}



// VECTOR CLASS AND MATHEMATICS --------------------------------
// -------------------------------------------------------------
class Vec {
  float x,y;
  Vec(float x,float y) {
    this.x=x; this.y=y;
  }
}

Vec VecAdd(Vec a, Vec b) {
  Vec z=new Vec(0,0);
  z.x=a.x+b.x; z.y=a.y+b.y;
  return z;
}

Vec VecSub(Vec a, Vec b) {
  Vec z=new Vec(0,0);
  z.x=a.x-b.x; z.y=a.y-b.y;
  return z;
}

Vec VecMul(float a, Vec b) {
  Vec z=new Vec(0,0);
  z.x=a*b.x; z.y=a*b.y;
  return z;
}

float VecSkalar(Vec a, Vec b) { // (len(a)*len(b)*cos(angle between a and b)
  float z;
  z=a.x*b.x+a.y*b.y;
  return z;
}

float VecLen(Vec a) {
  float z=sqrt(a.x*a.x+a.y*a.y);
  return z;
}

Vec VecInScreen(Vec a) {
  Vec z=a;
  while (z.x>width) {z.x-=width;} while (z.x<1) {z.x+=width;}
  return z;
}
// -------------------------------------------------------------
// (end vector class and mathematics) --------------------------
