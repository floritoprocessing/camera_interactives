boolean SHOW_CAM_INPUT=false;
boolean SHOW_ASCII_IMAGE=true;
boolean ASCII_IMAGE_IN_COLOR=true;

import processing.video.*; 
Capture myCapture; 

PFont font;
CustomFont cFont;
ASCIIImage ascImage;


void setup() {
  // SCREEN SETUP
  size(640, 480);//,P3D);
  colorMode(RGB, 255);
  background(0, 0, 0);

  cFont=new CustomFont(10);
  cFont.createCharacters(10);
  cFont.sortCharacters();
  //cFont.printSortedCharacters();
  //cFont.drawAllCharacters(10);

  //ascImage=new ASCIIImage("img2.jpg",10);
  ascImage=new ASCIIImage(640, 480, 10);  

  myCapture=new Capture(this, 640, 480);
  myCapture.start();
  //println(myCapture.list());
}

void draw() {
  background(0, 0, 0);
  myCapture.read();
  if (SHOW_CAM_INPUT) image(myCapture, 0, 0);
  ascImage.init(myCapture, 10);
  ascImage.divideImage();
  if (SHOW_ASCII_IMAGE) ascImage.drawASCIIImage(cFont, ASCII_IMAGE_IN_COLOR);

  int nfrm = 1;
  String bspth = "V:\\Processing visual index\\";
  String className = getClass().getName();
  //if (frameCount%nfrm==0) {
  //	saveFrame(bspth+className+"\\"+className+"_######.bmp");
  //}
  //ascImage.drawDividedImage();
}

void keyPressed() {
  if (key=='s') SHOW_CAM_INPUT=!SHOW_CAM_INPUT;
  if (key=='a') SHOW_ASCII_IMAGE=!SHOW_ASCII_IMAGE;
  if (key=='c') ASCII_IMAGE_IN_COLOR=!ASCII_IMAGE_IN_COLOR;
}
