class CustomFont {
  CustomChar[] cChar;
  int POINTSIZE;

  CustomFont(int p) {
    // LOAD AND SET FONT
    font = loadFont("LucidaConsole-10.vlw"); 
    textFont(font, p); 
    POINTSIZE=p;
  }

  void createCharacters(int p) {
    cChar=new CustomChar[223];
    for (int c=32;c<255;c++) {
      cChar[c-32]=new CustomChar(p,p);
      cChar[c-32].makeChar(c,0,0,color(255,255,255));
      double tmp=cChar[c-32].getBright();
    }
  }

  void sortCharacters() {
    for (int i=223;--i>=0;) {
      boolean flipped = false;
      for (int j=0; j<i; j++) {
        double a=cChar[j].getBright();
        double b=cChar[j+1].getBright();
        if (a>b) {
          CustomChar tmp=cChar[j];
          cChar[j]=cChar[j+1];
          cChar[j+1]=tmp;
          flipped = true;
        }
      }
      if (!flipped) return;
    }
  }

  void printSortedCharacters() {
    for (int c=0;c<223;c++) {
      println(c+":\t"+char(cChar[c].getCharNr())+" "+cChar[c].getBright());
    }
  }


  void drawAllCharacters(int p) {
    int i=0;
    int x=0;
    int y=p;
    while (i<223) {
      cChar[i].drawTo(x,y,color(255,255,255));
      x+=p;
      if (x>width-p) { 
        x=0;
        y+=p;
      }
      i++;
    }
  }
  
  void drawCharacterFromBrightness(int x,int y, double br, color c) {
    int ix=(int)(br*222);
    cChar[ix].drawTo(x,y,c);
  }

}
