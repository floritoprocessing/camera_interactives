class ASCIIImage {
  PImage img;
  double[][] divImg;
  color[][] divImgColor;
  int WID, HEI;
  int DIV;
  
  ASCIIImage(int w, int h, int d) {
    DIV=d;
    WID=w;
    HEI=h;
    divImg=new double[WID][HEI];
    divImgColor=new color[WID][HEI];
  }
  
  ASCIIImage(String fileName, int d) {
    DIV=d;
    img=loadImage(fileName);
    WID=img.width;
    HEI=img.height;
    divImg=new double[WID][HEI];
    divImgColor=new color[WID][HEI];
  }
  
  void init(PImage _img, int d) {
    img=_img;
    DIV=d;
  }
  
  void divideImage() {
    //img.loadPixels();
    for (int u=0;u<WID/DIV;u++) {
      for (int v=0;v<HEI/DIV;v++) {
        int baseX=u*DIV;
        int baseY=v*DIV;
        double bright=0;
        float rr=0, gg=0, bb=0;
        for (int xo=0;xo<DIV;xo++) {
          for (int yo=0;yo<DIV;yo++) {
            int scX=baseX+xo;
            int scY=baseY+yo;
            color ic=img.get(scX,scY);
            bright+=brightness(ic);
            rr+=red(ic);
            gg+=green(ic);
            bb+=blue(ic);
            //bright+=red(img.get(scX,scY));
          }
        }
        float d=DIV*DIV;
        bright/=(255.0*d);
        rr/=d;
        gg/=d;
        bb/=d;
        divImg[u][v]=bright;
        divImgColor[u][v]=color(rr,gg,bb);
      }
    }
  }
  
  void drawDividedImage() {
    rectMode(CORNER);
    noStroke();
    for (int u=0;u<WID/DIV;u++) {
      for (int v=0;v<HEI/DIV;v++) {
        float br=(float)(255*divImg[u][v]);
        fill(br);
        rect(u*DIV,v*DIV,DIV,DIV);
      }
    }
  }
  
  void drawASCIIImage(CustomFont cf,boolean inColor) {
    for (int u=0;u<WID/DIV;u++) {
      for (int v=0;v<HEI/DIV;v++) {
        double br=divImg[u][v];
        color c;
        if (inColor) {
          c=divImgColor[u][v];
        } else {
          c=color(255,255,255);
        }
        cf.drawCharacterFromBrightness(u*DIV,v*DIV,br,c);
      }
    }
  }
  
}
