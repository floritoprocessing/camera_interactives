class CustomChar {
  int wid=1, hei=1;
  int CHRNR;
  double brightIndex;
  
  CustomChar() {
  }
  
  CustomChar(int w, int h) {
    wid=w;
    hei=h;
  }
  
  void init(CustomChar c) {
    wid=c.wid;
    hei=c.hei;
    CHRNR=c.CHRNR;
    brightIndex=c.brightIndex;
  }
  
  void makeChar(int chr, int xo, int yo, color c) {
    CHRNR=chr;
    int nrOfPixels=0;
    background(0,0,0);
    stroke(255,255,255);
    text(char(chr),xo,yo+hei);
    for (int x=xo;x<xo+wid;x++) {
      for (int y=yo;y<yo+hei;y++) {
        color sc=get(x,y);
        if (sc!=color(0,0,0)) {
          nrOfPixels++;
        } else {
        }
      }
    }
    brightIndex=nrOfPixels/100.0;
  }
  
  double getBright() {
    return brightIndex;
  }
  
  int getCharNr() {
    return CHRNR;
  }
  
  void drawTo(int baseX, int baseY, color c) {
    fill(c);
    text(char(CHRNR),baseX,baseY);
  }
}
