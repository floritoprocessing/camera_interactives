import processing.opengl.*;

import processing.video.*;
Capture cap;

// camera variables:
float camToScreen, ctsX, ctsY; boolean newFrame; PImage fakeCam;
// camera buf:
color[] videoBuf;

// grid variables:
int gridW, gridH; 
float gridSpace; 
float gridToCam;
MyVertex[][] corner;
//MyVertex[] corner;

float rotX=0, rotY=0;
float brightToDepth = 0.3;

void setup() {
  // setup screen:
  size(800,600,P3D);
  // setup camera:
  fakeCam=loadImage("80x60.jpg");
  //beginVideo(80,60,25);
  cap = new Capture(this,80,60,25);
  cap.start();
  camToScreen=width/(float)cap.width; ctsX=cap.width*camToScreen; ctsY=cap.height*camToScreen;
  newFrame=false;
  // setup buffer;
  videoBuf=new color[cap.width*cap.height];
  // setup grid:
  gridW=160; gridH=120; gridSpace=5;
  corner=new MyVertex[gridW][gridH];
  //corner=new MyVertex[gridW*gridH];
  int i=0;
  for (int x=0;x<gridW;x++) {
  for (int y=0;y<gridH;y++) {
    
      corner[x][y]=new MyVertex((x+.5)*gridSpace,(y+.5)*gridSpace,0);
      //corner[i++]=new MyVertex((x+.5)*gridSpace,(y+.5)*gridSpace,0);
    }
  }
  gridToCam=cap.width/(float)gridW;
}

void draw() {
  background(128);
  // draw camera:
  //  image(video,0,0,ctsX,ctsY);
  
  if (cap.available()) {
    newFrame=true;
    cap.read();
    cap.loadPixels();
  }
  if (newFrame) {
    // get new video buffer
    for (int i=0;i<cap.width*cap.height;i++) {
      videoBuf[i]=cap.pixels[i];
//      videoBuf[i]=fakeCam.pixels[i];
    }
  }
  
  // change grid
  //int i=0;
  for (int x=0;x<gridW;x++) {
    for (int y=0;y<gridH;y++) {  
      int cx=int(x*gridToCam);
      int cy=int(y*gridToCam);
      color c=videoBuf[cy*cap.width+cx];
      float tz=brightToDepth*brightness(c);
      corner[x][y].z = (corner[x][y].z+tz)/2.0;
      corner[x][y].c=c;
      //corner[i].z = (corner[i].z+tz)/2.0;
      //corner[i++].c=c;
    }
  }
  
  MyVertex[] p=new MyVertex[4];
  
  if (mousePressed) {
    if (mouseButton==LEFT) {
      rotX += (pmouseY-mouseY)*1/180.0;
      rotY -= (pmouseX-mouseX)*1/180.0;
    }
    if (mouseButton==RIGHT) {
      brightToDepth *= (1 + (pmouseY-mouseY)*0.01f);
    }
  }
  
  // draw grid RED
  background(255); 
  
  translate(width/2,height/2);
  rotateY(rotY);
  rotateX(rotX);
  translate(-width/2,-height/2);
  
  beginShape(QUADS); 
  stroke(128);
  
  
//  i=0;
  for (int x=0;x<gridW;x++) {  
    for (int y=0;y<gridH;y++) {
    
      if (x<gridW-1&&y<gridH-1) {
        p[0]=corner[x][y]; 
        p[1]=corner[x+1][y]; 
        p[2]=corner[x+1][y+1]; 
        p[3]=corner[x][y+1];
        //p[0]=corner[i]; 
        //p[1]=corner[i+1]; 
        //p[2]=corner[i+1+gridW]; 
        //p[3]=corner[i+gridW];
        float br=0.0;
        for (int j=0;j<4;j++) {
          br+=brightness(p[j].c)/4.0;
        }
        fill(br);
        for (int j=0;j<4;j++) {vertex(p[j].x,p[j].y,p[j].z);}
        
      }
//      i++;
    }
  }
  endShape();

  newFrame=false;
}

class MyVertex {
  float x,y,z;
  color c;
  MyVertex(float inx, float iny, float inz) {
    c=color(0,0,0);
    x=inx; y=iny; z=inz;
  }
}

void videoEvent() {
  newFrame=true;
}
