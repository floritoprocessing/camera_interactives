class GameController {
  
  public static final int MODE_SET_BACKGROUND=0;
  public static final int MODE_RUNNING=1;
  int mode = 0;
  int lastMode = 0;
  
  AverageImage bgAverage;
  PImage lastImage, currentImage, diffImage;
  
  GameController() {
    bgAverage = new AverageImage();
  }
  
  void setMode(int m) {
    mode = m;
  }
  
  void update(CameraInput ci) {
    
    if (mode==MODE_SET_BACKGROUND) {
      // INIT:
      if (lastMode!=mode) {
        bgAverage.setMode(AverageImage.MODE_WAIT_FOR_KEY);
      }
      // LOOP:
      PImage img=ci.getCam();
      bgAverage.update(img);
      if (bgAverage.mode==AverageImage.MODE_IDLE) mode=MODE_RUNNING;
      
    } 
    
    
    
    if (mode==MODE_RUNNING) {
      // INIT:
      if (lastMode!=mode) {
        println("Into Run Mode!");
        PImage img = ci.getCam();
        currentImage = new PImage(img.width,img.height);
        currentImage.copy(img,0,0,img.width,img.height,0,0,img.width,img.height);
        lastImage = new PImage(img.width,img.height);
        lastImage.copy(img,0,0,img.width,img.height,0,0,img.width,img.height);
        diffImage = new PImage(img.width,img.height);
      }
      
      
      
      // LOOP:
      PImage img=ci.getCam();
      
      
      for (int i=0;i<img.pixels.length;i++) diffImage.pixels[i]=img.pixels[i];
      diffImage.updatePixels();
      diffImage.blend(lastImage,0,0,img.width,img.height,0,0,img.width,img.height,SUBTRACT);
      //diffImage.copy(img,0,0,img.width,img.height,0,0,img.width,img.height);
      //diffImage.blend(lastImage,0,0,0,0,ADD);
      
      image(img,0,0);
      image(lastImage,0,height/2);
      image(diffImage,width/2,0);
      //currentImage.copy(img,0,0,img.width,img.height,0,0,img.width,img.height);
      
      for (int i=0;i<img.pixels.length;i++) lastImage.pixels[i]=img.pixels[i];
      lastImage.updatePixels();
      //lastImage.copy(img,0,0,img.width,img.height,0,0,img.width,img.height);
    }
    
    lastMode = mode;
  }
  
  
}
