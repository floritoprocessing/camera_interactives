import processing.video.*;

class CameraInput {
  
  int wid=320;
  int hei=240;
  int fps=25;
  int device_index=0;
  
  Capture myCapture;
  
  CameraInput(PApplet pa, int w, int h, int f, int di) {
    wid=w;
    hei=h;
    fps=f;
    device_index=di;
    myCapture = new Capture(pa,wid,hei);
    myCapture.start();
  }
  
  PImage getCam() {
    if (myCapture.available()) myCapture.read();
    myCapture.loadPixels();
    return myCapture;
  }
}
