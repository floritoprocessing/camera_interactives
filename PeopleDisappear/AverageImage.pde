class AverageImage {

  int NR_OF_IMAGES = 10;

  public static final int MODE_WAIT_FOR_KEY = 0;
  public static final int MODE_RECORDING = 1;
  public static final int MODE_MAKE_AVERAGE_IMAGE = 2;
  public static final int MODE_SHOW_AVERAGE_IMAGE = 3;
  public static final int MODE_IDLE = 4;
  int mode = 0;

  PImage[] bufImage;
  PImage averageImage;
  int recFrame = 0;

  //int[][] channelPixelArray;
  

  AverageImage() {
    bufImage=new PImage[NR_OF_IMAGES];
    averageImage=new PImage();
  }

  void setMode(int m) {
    mode = m;
  }

  void update(PImage _img) {
    
    if (mode==MODE_WAIT_FOR_KEY) {

      println("Press a Key to start recording background image");
      image(_img,0,0);
      if (keyPressed) {
        mode=MODE_RECORDING;
        recFrame=0;
      }

    } 



    if (mode==MODE_RECORDING) {

      bufImage[recFrame] = new PImage(_img.width,_img.height);
      _img.loadPixels();
      for (int i=0;i<_img.pixels.length;i++) bufImage[recFrame].pixels[i]=_img.pixels[i];
      bufImage[recFrame].loadPixels();
      recFrame++;
      if (recFrame==NR_OF_IMAGES) mode=MODE_MAKE_AVERAGE_IMAGE;

    }



    if (mode==MODE_MAKE_AVERAGE_IMAGE) {

      println("Making average image");
      background(0);
      averageImage = new PImage(bufImage[0].width,bufImage[0].height);
      int[] pxval = new int[3];
      for (int p=0;p<bufImage[0].pixels.length;p++) {
        for (int c=0;c<3;c++) {
          pxval[c]=0;
          for (int i=0;i<NR_OF_IMAGES;i++) {
            int col=bufImage[i].pixels[p];
            pxval[c]+=(col>>(c*8)&0xFF);
          }
          pxval[c] = (int)( pxval[c] / (float)NR_OF_IMAGES);
        }
        averageImage.pixels[p]=color(pxval[2],pxval[1],pxval[0]);
        averageImage.updatePixels();
      }

      
      mode = MODE_SHOW_AVERAGE_IMAGE;

    }



    if (mode==MODE_SHOW_AVERAGE_IMAGE) {
      println("Showing Average Image - press key to continue");
      image(averageImage,0,0);
      if (keyPressed) mode = MODE_IDLE;
    }
    
    
    
    if (mode==MODE_IDLE) {
      // nothing
    }
  }

}
