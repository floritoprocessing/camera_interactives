CameraInput camInput;
GameController gc;

void setup() {
  size(640,480);
  background(0);
  camInput = new CameraInput(this,320,240,25,0);
  gc = new GameController();
}

void draw() {
  gc.update(camInput);
  //image(camInput.getCam(),0,0);
}
