import processing.video.*;
Capture video;

// camera variables:
float camToScreen, ctsX, ctsY; boolean newFrame;
// grid variables:
int gridW, gridH; float gridSpace; float gridToCam;
MyVertex[][] corner;
// 2buf var
PImage imgRed, imgGreen;

void setup() {
  // setup screen:
  size(800,600,P3D);
  // setup camera:
  //beginVideo(80,60,25);
  video = new Capture(this,80,60,25);
  video.start();
  
  camToScreen=width/(float)video.width; ctsX=video.width*camToScreen; ctsY=video.height*camToScreen;
  newFrame=false;
  // setup grid:
  gridW=160; gridH=120; gridSpace=5;
  corner=new MyVertex[gridW][gridH];
  for (int x=0;x<gridW;x++) {
    for (int y=0;y<gridH;y++) {
      corner[x][y]=new MyVertex((x+.5)*gridSpace,(y+.5)*gridSpace,0);
    }
  }
  gridToCam=video.width/(float)gridW;
  // setup 2buf
  imgRed=loadImage("800x600.jpg"); imgGreen=imgRed;
}

void draw() {
  background(128);
  // draw camera:
  //  image(video,0,0,ctsX,ctsY);
  
  if (video.available()) {
    video.read();
    newFrame = true;
    video.loadPixels();
  }
  
  if (newFrame) {
    // change grid
    for (int x=0;x<gridW;x++) {
      for (int y=0;y<gridH;y++) {
        int cx=int(x*gridToCam);
        int cy=int(y*gridToCam);
        color c=video.pixels[cy*video.width+cx];
        float tz=0.5*brightness(c);
        corner[x][y].z = tz;//(3.0*corner[x][y].z+tz)/4.0;
        corner[x][y].c=c;
      }
    }
  }
  
  MyVertex[] p=new MyVertex[4];
  // draw grid RED
  background(255); beginShape(QUADS); noStroke(); noFill();
  for (int x=0;x<gridW;x++) {
    for (int y=0;y<gridH;y++) {
      if (x<gridW-1&&y<gridH-1) {
        p[0]=corner[x][y]; p[1]=corner[x+1][y]; p[2]=corner[x+1][y+1]; p[3]=corner[x][y+1];
//        fill(brightness(corner[x][y].c),0,0,50); 
        stroke(255,0,0);
      for (int i=0;i<4;i++) {vertex(p[i].x-30,p[i].y,p[i].z);}
      }
    }
  }
  endShape();
  
  loadPixels();
  System.arraycopy(pixels,0,imgRed.pixels,0,pixels.length);
  //imgRed=copy(this,0,0,width,height,0,0,width,height);
  
  // draw grid GREEN
  background(255); beginShape(QUADS); noStroke(); noFill();
  for (int x=0;x<gridW;x++) {
    for (int y=0;y<gridH;y++) {
      if (x<gridW-1&&y<gridH-1) {
        p[0]=corner[x][y]; p[1]=corner[x+1][y]; p[2]=corner[x+1][y+1]; p[3]=corner[x][y+1];
//        fill(0,brightness(corner[x][y].c),0,50); 
        stroke(0,255,0);
      for (int i=0;i<4;i++) {vertex(p[i].x+30,p[i].y,p[i].z);}
      }
    }
  }
  endShape();
  
  loadPixels();
  System.arraycopy(pixels,0,imgGreen.pixels,0,pixels.length);
  //imgGreen=copy();
  
  background(255);
  
  tint(255,128);
  image(imgGreen,-30,0);
  image(imgRed,30,0);

  newFrame=false;
}

class MyVertex {
  float x,y,z;
  color c;
  MyVertex(float inx, float iny, float inz) {
    c=color(0,0,0);
    x=inx; y=iny; z=inz;
  }
}

void videoEvent() {
  newFrame=true;
}
